efinancial Installation
==========================
1. Download mysql and install it and start the mysql service
2. Create mysql schema by going to $(WORKSPACE)/schema and run "mysql<create_tables.sql"
3. Initialize mysql databa by running "mysql<data_inject.sql" in the same directory
4. mvn package
5. copy $(WORKSPACE)/target/efinancialpal.war and copy it over to $(JETTY)/webapps
6. Go to $(JETTY) directory and run "java -Dconfig.dir $(WORKSPACE)/src/main/resources -jar start.jar jetty.http.port=8888"
7. Open swagger UI by typing "http://localhost:8888/efinancialpal/apidocs" in your local browser
