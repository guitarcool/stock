package com.affirm;

/**
 * Created by haiyanluo on 12/2/17.
 */
public class Facility {
    private int bankId;
    private int facilityId;
    private float interestRate;
    private int amount;
    public Facility() {

    }
    public Facility(int bankId, int facilityId, float interestRate, int amount) {
        this.bankId = bankId;
        this.facilityId = facilityId;
        this.interestRate = interestRate;
        this.amount = amount;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public int getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(int facilityId) {
        this.facilityId = facilityId;
    }

    public float getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(float interestRate) {
        this.interestRate = interestRate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}
