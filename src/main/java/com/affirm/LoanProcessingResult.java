package com.affirm;

import scala.tools.cmd.gen.AnyVals;

/**
 * Created by haiyanluo on 12/2/17.
 */
public class LoanProcessingResult {
    private int loanId;
    private int facilityId;
    private int expectedYield;

    public LoanProcessingResult() {

    }

    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    public int getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(int facilityId) {
        this.facilityId = facilityId;
    }

    public int getExpectedYield() {
        return expectedYield;
    }

    public void setExpectedYield(int expectedYield) {
        this.expectedYield = expectedYield;
    }
}
