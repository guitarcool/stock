package com.affirm;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

/**
 * Created by haiyanluo on 12/2/17.
 */
public class AffirmMain {
    private static final Logger logger = LoggerFactory.getLogger(AffirmMain.class);
    private static final String LOANS_FILE = "/Volumes/Transcend/Downloads/affirm-take-home-interview/large/loans.csv";
    private static final String ASSIGNMENT_FILE = "/tmp/assignment.csv";
    private static final String YIELDS_FILE = "/tmp/yields.csv";

    public static void main(String[] args) {
        List<Facility> facilities = LoanProcessor.loadFacilities();
        Map<Integer, Set<Covenant>> facility2CovenantsMap = LoanProcessor.loadCovenants();
        processLoanInStreaming(facilities, facility2CovenantsMap);
    }

    public static void processLoanInStreaming(List<Facility> facilities, Map<Integer, Set<Covenant>> facility2CovenantsMap) {
        FileWriter fileWriter = null;
        CSVPrinter csvPrinter = null;

        Map<Integer, Integer> expectedYieldMap = new HashMap<>();
        try (final Reader reader = new BufferedReader(new FileReader(LOANS_FILE))) {
            try (CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader())) {
                CSVFormat csvFileFormat = CSVFormat.EXCEL.withIgnoreEmptyLines();
                fileWriter = new FileWriter(ASSIGNMENT_FILE);
                csvPrinter = new CSVPrinter(fileWriter, csvFileFormat);
                csvPrinter.printRecord("loan_id", "facility_id");

                for (final CSVRecord record : parser) {
                    Loan loan = new Loan();
                    loan.setId(Integer.valueOf(record.get("id")));
                    loan.setAmount(Integer.valueOf(record.get("amount")));
                    loan.setInterestRate(Float.valueOf(record.get("interest_rate")));
                    loan.setDefaultLikelihood(Float.valueOf(record.get("default_likelihood")));
                    loan.setState(String.valueOf(record.get("state")));
                    LoanProcessingResult loanProcessingResult = LoanProcessor.loanMatch(facilities, facility2CovenantsMap, loan);
                    expectedYieldMap.put(loanProcessingResult.getFacilityId(), expectedYieldMap.getOrDefault(loanProcessingResult.getFacilityId(), 0) + loanProcessingResult.getExpectedYield());
                    if (loanProcessingResult != null) {
                        logger.info("loan_id: {}, facility_id: {}, expected_yield: {}", loanProcessingResult.getLoanId(), loanProcessingResult.getFacilityId(), expectedYieldMap.get(loanProcessingResult.getFacilityId()));
                    } else {
                        logger.info("Can't assign facility to loan {}", loan.getId());
                    }
                    printOutAssignment(loanProcessingResult, csvPrinter);
                }
            } catch (Exception e) {
                logger.error("Failed to parse {}", LOANS_FILE);
            }

        } catch (Exception e) {
            logger.error("Failed to open facilities file {}", LOANS_FILE);
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                fileWriter.close();
            } catch (Exception e1) {
                logger.error("Error while flushing/closing fileWriter/csvPrinter:", e1);
            }
        }
        try {
            printOutYield(expectedYieldMap);
        } catch (Exception e) {
            logger.error("Failed to output expectedYieldMap: ", e);
        }
    }

    public static void printOutYield(Map<Integer, Integer> yieldMap) throws Exception {
        int minFacilityId = Integer.MAX_VALUE;
        int maxFacilityId = Integer.MIN_VALUE;
        for (int facilityId : yieldMap.keySet()) {
            minFacilityId = Math.min(facilityId, minFacilityId);
            maxFacilityId = Math.max(facilityId, maxFacilityId);
        }

        FileWriter fileWriter = null;
        CSVPrinter csvPrinter = null;

        try {
            CSVFormat csvFileFormat = CSVFormat.EXCEL.withIgnoreEmptyLines();
            fileWriter = new FileWriter(YIELDS_FILE);
            csvPrinter = new CSVPrinter(fileWriter, csvFileFormat);
            csvPrinter.printRecord("facility_id", "expected_yield");

            for (int i = minFacilityId; i <= maxFacilityId; ++i) {
                if (yieldMap.containsKey(i)) {
                    List<String> record = new ArrayList<>();
                    record.add(String.valueOf(i));
                    record.add(String.valueOf(yieldMap.get(i)));
                    csvPrinter.printRecord(record);
                }
            }
        } catch (Exception e) {
            logger.error("Failed to write output for yield: ", e);
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                fileWriter.close();
            } catch (Exception e1) {
                logger.error("Error while flushing/closing fileWriter/csvPrinter:", e1);
            }
        }
    }
    public static void printOutAssignment(LoanProcessingResult loanProcessingResult, CSVPrinter csvPrinter) throws Exception {
        List<String> record = Arrays.asList(String.valueOf(loanProcessingResult.getLoanId()), String.valueOf(loanProcessingResult.getFacilityId()));
        csvPrinter.printRecord(record);
    }

}
