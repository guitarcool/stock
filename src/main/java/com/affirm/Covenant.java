package com.affirm;

/**
 * Created by haiyanluo on 12/2/17.
 */
public class Covenant {
    private int bankId;
    private int facilityId;
    private float maxDefaultLikelihood;
    private String bannedState;

    public Covenant() {

    }

    public Covenant(int bankId, int facilityId, float maxDefaultLikelihood, String bannedState) {
        this.bankId = bankId;
        this.facilityId = facilityId;
        this.maxDefaultLikelihood = maxDefaultLikelihood;
        this.bannedState = bannedState;
    }

    public Covenant clone(int facilityId) {
        Covenant covenant = new Covenant(this.bankId, this.facilityId, this.maxDefaultLikelihood, this.bannedState);
        covenant.setFacilityId(facilityId);
        return covenant;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public int getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(int facilityId) {
        this.facilityId = facilityId;
    }

    public float getMaxDefaultLikelihood() {
        return maxDefaultLikelihood;
    }

    public void setMaxDefaultLikelihood(float maxDefaultLikelihood) {
        this.maxDefaultLikelihood = maxDefaultLikelihood;
    }

    public String getBannedState() {
        return bannedState;
    }

    public void setBannedState(String bannedState) {
        this.bannedState = bannedState;
    }
}
