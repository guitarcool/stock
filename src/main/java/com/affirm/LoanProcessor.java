package com.affirm;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.util.*;

/**
 * Created by haiyanluo on 12/2/17.
 */
public class LoanProcessor {
    private static final Logger logger = LoggerFactory.getLogger(LoanProcessor.class);
    private static final String FACILITIES_FILE = "/Volumes/Transcend/Downloads/affirm-take-home-interview/large/facilities.csv";
    private static final String COVENANTS_FILE = "/Volumes/Transcend/Downloads/affirm-take-home-interview/large/covenants.csv";
    private static final String BANK_FILE = "/Volumes/Transcend/Downloads/affirm-take-home-interview/large/banks.csv";
    private static Map<Integer, Set<Integer>> bank2FacilitiesMap = new HashMap<>();

    public static List<Facility> loadFacilities() {
        List<Facility> facilities = new ArrayList<>();
        try (final Reader reader = new BufferedReader(new FileReader(FACILITIES_FILE))) {
            try (CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader())) {
                for (final CSVRecord record : parser) {
                    Facility facility = new Facility();
                    facility.setAmount(Math.round(Float.valueOf(record.get("amount"))));
                    facility.setInterestRate(Float.valueOf(record.get("interest_rate")));
                    int facilityId = Integer.valueOf(record.get("id"));
                    facility.setFacilityId(facilityId);
                    int bankId = Integer.valueOf(record.get("bank_id"));
                    facility.setBankId(bankId);
                    if (!bank2FacilitiesMap.containsKey(bankId)) {
                        bank2FacilitiesMap.put(bankId, new HashSet<>());
                    }
                    bank2FacilitiesMap.get(bankId).add(facilityId);
                    facilities.add(facility);
                }
            } catch (Exception e) {
                logger.error("Failed to parse {}", FACILITIES_FILE);
            }

        } catch (Exception e) {
            logger.error("Failed to open facilities file {}", FACILITIES_FILE);
        }
        Collections.sort(facilities, (a, b) -> Float.compare(a.getInterestRate(), b.getAmount()));
        return facilities;
    }

    public static Map<Integer, Set<Covenant>> loadCovenants() {
        Map<Integer, Set<Covenant>> facility2CovenantsMap = new HashMap<>();
        try (final Reader reader = new BufferedReader(new FileReader(COVENANTS_FILE))) {
            try (CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader())) {
                for (final CSVRecord record : parser) {
                    Covenant covenant = new Covenant();
                    String facilityIdStr = record.get("facility_id");
                    if (StringUtils.isNotBlank(record.get("max_default_likelihood"))) {
                        covenant.setMaxDefaultLikelihood(Float.valueOf(record.get("max_default_likelihood")));
                    } else {
                        covenant.setMaxDefaultLikelihood(Float.MAX_VALUE);
                    }
                    int bankId = Integer.valueOf(record.get("bank_id"));
                    covenant.setBankId(bankId);
                    if (facilityIdStr != null) {
                        int facilityId = Integer.valueOf(facilityIdStr);
                        covenant.setFacilityId(facilityId);
                        if (!facility2CovenantsMap.containsKey(facilityId)) {
                            facility2CovenantsMap.put(facilityId, new HashSet<>());
                        }
                        facility2CovenantsMap.get(facilityId).add(covenant);
                    } else {
                        if (bank2FacilitiesMap.containsKey(bankId)) {
                            for (int facilityId : bank2FacilitiesMap.get(bankId)) {
                                facility2CovenantsMap.get(facilityId).add(covenant.clone(facilityId));
                            }
                        }
                    }
                    if (record.get("banned_state") != null){
                        covenant.setBannedState(String.valueOf(record.get("banned_state")));
                    }
                }
            } catch (Exception e) {
                logger.error("Failed to parse {}", COVENANTS_FILE);
            }

        } catch (Exception e) {
            logger.error("Failed to open covenant file {}", COVENANTS_FILE);
        }
        return facility2CovenantsMap;
    }

    public static List<Bank> loadBanks() {
        List<Bank> banks = new ArrayList<>();
        try (final Reader reader = new BufferedReader(new FileReader(FACILITIES_FILE))) {
            try (CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader())) {
                for (final CSVRecord record : parser) {
                    Bank bank = new Bank();
                    bank.setBankId(Integer.valueOf(record.get("id")));
                    bank.setBankName(String.valueOf(record.get("name")));
                    banks.add(bank);
                }
            } catch (Exception e) {
                logger.error("Failed to parse {}", BANK_FILE);
            }

        } catch (Exception e) {
            logger.error("Failed to open bank file {}", BANK_FILE);
        }
        return banks;
    }

    public static LoanProcessingResult loanMatch(List<Facility> facilities, Map<Integer, Set<Covenant>> facilities2CovenantsMap, Loan loan) {
        for (int i = 0; i < facilities.size(); ++i) {
            Facility facility = facilities.get(i);
            if (!facilities2CovenantsMap.containsKey(facility.getFacilityId()) || (facility.getAmount() >= loan.getAmount() && canGiveLoan(facilities2CovenantsMap.get(facility.getFacilityId()), loan))) {
                facility.setAmount(facility.getAmount() - loan.getAmount());
                LoanProcessingResult loanProcessingResult = new LoanProcessingResult();
                loanProcessingResult.setLoanId(loan.getId());
                loanProcessingResult.setFacilityId(facility.getFacilityId());
                float expectedYield = ((1.0f - loan.getDefaultLikelihood()) * loan.getInterestRate() - loan.getDefaultLikelihood() - facility.getInterestRate()) * loan.getAmount();
                loanProcessingResult.setExpectedYield(Math.round(expectedYield));
                return loanProcessingResult;
            }
        }
        return null;
    }

    public static boolean canGiveLoan(Set<Covenant> covenants, Loan loan) {
        for (Covenant covenant : covenants) {
            if (covenant.getBannedState() != null && covenant.getBannedState().equals(loan.getState())) {
                return false;
            }
            if (loan.getDefaultLikelihood() > covenant.getMaxDefaultLikelihood()) {
                return false;
            }
        }
        return true;
    }


}
