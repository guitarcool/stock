package com.efinancialpal.ta;

import com.efinancialpal.swagger.rest.Constants;
import com.efinancialpal.utils.db.ActiveJDBCUtil;
import org.javalite.activejdbc.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by haiyan on 7/24/2016.
 */
public class TechnicalAnalyzer {
    private static final Logger logger = LoggerFactory.getLogger(TechnicalAnalyzer.class);
    private static TechnicalAnalyzer instance = new TechnicalAnalyzer();

    public static TechnicalAnalyzer getInstance() {
        return instance;
    }

    /** Check if there is 50week high over the past one month period
     *
     * @param ticker
     * @return
     */
    public boolean newHigh50Weeks(String ticker) {
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            // get the latest record of the ticker symbol
            String query = "select * from daily_transaction_data where ticker = '" + ticker + "' order by version_updated_at desc limit 1";
            logger.info("query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result == null || result.isEmpty()) {
                logger.info("ticker symbol {} doesn't have any transaction data in our system", ticker);
                return false;
            } else {
                BigDecimal maxClosePrice = (BigDecimal) result.get(0).get("close");
                Date maxClosePriceDate = (Date)(result.get(0).get("version_updated_at"));
                logger.info("Max close price for ticker symbol {} is {} at date {}", ticker, maxClosePrice, maxClosePriceDate);
                Calendar today = Calendar.getInstance();
                today.add(Calendar.DATE, -30);
                if (maxClosePriceDate.compareTo(today.getTime())  > 0) {
                    logger.info("Price has breakout within the last month for ticker {}", ticker);
                    return true;
                }
            }
            return false;
        } catch (Throwable t) {
            logger.error("Exception happened while calculating 50 weeks breakout: ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
}
