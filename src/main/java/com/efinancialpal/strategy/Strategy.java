package com.efinancialpal.strategy;

import com.efinancialpal.strategy.backtest.BackTestResult;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haiyanluo on 11/26/16.
 */
public abstract class Strategy {
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        return false;
    }
    public List<BackTestResult> backTest(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        return new ArrayList<BackTestResult>();
    }
    public abstract  String name();
    public abstract  String longOrShort();
}
