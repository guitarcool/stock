package com.efinancialpal.strategy.buy;

/**
 * Created by haiyanluo on 9/9/17.
 */

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.strategy.utils.StrategyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;


/**
 * pull back 3-7 days in a row with lower volume after making 40 day high
 */
public class Pullback123LongStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(Pullback123LongStrategy.class);

    private static final int TOTAL_DAYS_TO_CONSIDER = 40;
    private static final double LOW_PRICE_STOCK = 20;
    private static final int MIN_CONSOLIDATION_DAY = 3;
    private static final int MAX_CONSOLIDATION_DAY = 20; // after 7 days, it tends to lose the momentum
    private static final int ADX_LENGTH = 14;
    private static final double ADX_THRESHOLD = 30;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < TOTAL_DAYS_TO_CONSIDER + 1) {
            // should have more than one year data
            return false;
        }
        try {
            double highestHihgPrice = quotes.get(0).getHigh().doubleValue();
            double lowestConsolidationPrice = quotes.get(0).getLow().doubleValue();
            double currentClosePrice = quotes.get(0).getClose().doubleValue();
            /*
            if (currentClosePrice < LOW_PRICE_STOCK) {
                // low stock price is filtered out
                return false;
            }
            */

            int highestIndex = 0;
            for (int i = 1; i < TOTAL_DAYS_TO_CONSIDER; ++i) {
                if (quotes.get(i).getHigh().doubleValue() > highestHihgPrice) {
                    highestIndex = i;
                    highestHihgPrice = quotes.get(i).getHigh().doubleValue();
                }
            }
            if (highestIndex < MIN_CONSOLIDATION_DAY || highestIndex > MAX_CONSOLIDATION_DAY) {
                // only consolidated for 3-7 days
                return false;
            }

            // if today's price range is too large, filter it out, since it's hard to set stop loss.
            // However, if it's a hammer, we want to keep it, so we only look at close price
            double todayClose = quotes.get(0).getClose().doubleValue();
            double yesterdayClose = quotes.get(1).getClose().doubleValue();
            if (todayClose / yesterdayClose < 0.97) {
               return false;
            }

            for (int i = 1; i < highestIndex; ++i) {
                double currentHigh = quotes.get(i - 1).getHigh().doubleValue();
                double previousdayHigh = quotes.get(i).getHigh().doubleValue();
                if (currentHigh > previousdayHigh && (currentHigh - previousdayHigh)/ previousdayHigh > 0.005) {
                // if (currentHigh > previousdayHigh) {
                    return false;
                }
                if (quotes.get(i).getLow().doubleValue() < lowestConsolidationPrice) {
                    lowestConsolidationPrice = quotes.get(i).getLow().doubleValue() ;
                }
            }
            if (lowestConsolidationPrice / highestHihgPrice < 0.5) {
                // the retracement is too deep
                return false;
            }

            // has some volume
            double downVolume = 0;
            for (int i = 0; i <= highestIndex; ++i) {
                downVolume += quotes.get(i).getVolume();
            }
            downVolume /= (highestIndex + 1);

            // volume contraction
            double avgVolume = 0;
            for (int i = highestIndex; i < highestIndex + 20; ++i) {
                avgVolume += quotes.get(i).getVolume();
            }
            avgVolume /= 20;
            if (downVolume > 0.8 * avgVolume) {
                return false;
            }

            if (!StrategyUtils.isEmaSlopeUp(currentClosePrice, quotes, true /* long */)) {
                return false;
            }

            // TODO: check if ADX +DI is greater than 30
            /*
            AverageDirectionalMovementUpIndicator adx = new AverageDirectionalMovementUpIndicator(new TimeSeries(tickList), ADX_LENGTH);
            AverageDirectionalMovementDownIndicator adx1 = new AverageDirectionalMovementDownIndicator(new TimeSeries(tickList), ADX_LENGTH);
            double di = adx.getValue(0).toDouble();
            if (di < ADX_THRESHOLD) {
                return false;
            }
            */

        } catch (Exception e) {
            logger.error("error in processing {}, ", ticker, e);
            return false;
        }
        logger.info("Found one stock: {}", ticker);

        evaluationContext.populate(ticker, name(), longOrShort());
        return true;
    }

    @Override
    public String name() {
        return StrategyConstants.PULLBACK123_LONG;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}

