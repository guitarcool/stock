package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 11/14/16.
 *
 * 4X4 pullback strategy
 * http://www.marketgeeks.com/technical-analysis-strategies/
 *
 * I added the volume part
 */
public class FourByFourPullbackstrategy extends Strategy {
    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 25 ) {
            return false;
        }

        // for the past 4 days, we have lower low for every day
        if (quotes.get(0).getLow().doubleValue() > quotes.get(1).getLow().doubleValue()) {
            return false;
        }
        if (quotes.get(1).getLow().doubleValue() > quotes.get(2).getLow().doubleValue()) {
            return false;
        }
        if (quotes.get(2).getLow().doubleValue() > quotes.get(3).getLow().doubleValue()) {
            return false;
        }
        if (quotes.get(3).getLow().doubleValue() > quotes.get(4).getLow().doubleValue()) {
            return false;
        }

        double downVolume = (quotes.get(0).getVolume() + quotes.get(1).getVolume() + quotes.get(2).getVolume() + quotes.get(3).getVolume()) * 1.0 / 4;

        double previousVolume = 0;
        // 4 days ago, we have a 20-day high
        double high = quotes.get(4).getHigh().doubleValue();
        for (int i = 5; i < 24; ++i) {
            previousVolume += quotes.get(i).getVolume();
            if (high < quotes.get(i).getHigh().doubleValue()) {
                return false;
            }
        }

        previousVolume /= 20;
        if (downVolume > 0.8 * previousVolume) {
            return false;
        }

        evaluationContext.populate(ticker, name(), longOrShort());
        return true;
    }

    @Override
    public String name() {
        return StrategyConstants.FOUR_BY_FOUR_PULLBACK;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
