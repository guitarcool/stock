package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.strategy.utils.StrategyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 11/12/16.
 * <p>
 * find the sotcks with the following shape, indicating a bottom is forming
 *  |    |
 *  |    |
 *   ___
 *    |
 *    |
 * <p>
 * NOTICE: this also gives hanging man pattern
 */
public class HammerStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(HammerStrategy.class);
    private static final double threshold = 0.2;
    private static final int PAST_THREE_DAYS = 1; // only the current day

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 3) {
            return false;
        }

        double currentClosePrice = quotes.get(0).getClose().doubleValue();
        try {

            for (int i = 0; i < PAST_THREE_DAYS; ++i) {
                HistoricalQuote quote = quotes.get(i);
                double open = quote.getOpen().doubleValue();
                double close = quote.getClose().doubleValue();
                double high = quote.getHigh().doubleValue();
                double low = quote.getLow().doubleValue();
                double volume = quote.getVolume().doubleValue();

                double range = high - low;
                if (range / low < 0.02) {
                    // the stock doesn't move at all
                    continue;
                }

                double bodyLength = Math.abs(open - close);
                /*
                if (bodyLength / range > threshold) {
                    // the body is too large, the shadow needs to be at least twice as long as the body length
                    continue;
                } */

                double topShadow = high - (Math.max(open, close));
                if (topShadow / range > 0.2) {
                    // the top shadow is too long;
                    continue;
                }

                double bottomShadow = Math.min(open, close) - low;
                if (bottomShadow / range < 0.6) {
                    continue;
                }

                int j = i + 1;
                for (; j < i + 6; ++j) {
                    // 5 day low
                    if (low > quotes.get(j).getLow().doubleValue()) {
                        break;
                    }
                }
                if (j < i + 6) {
                    continue;
                }
                /*
                if (volume < 0.7 * StrategyUtils.getAverageVolume(quotes.subList(i + 1, quotes.size()), 20)) {
                    continue;
                }*/

                // The following functions till has a bug!!!
                // May 27 2019 comment out for now
                // if (!StrategyUtils.isEmaSlopeUp(currentClosePrice, quotes, true /* up */,  false /* relaxed uptrend */)) {
                //     continue;
                // }


                evaluationContext.populate(ticker, name(), longOrShort());
                return true;

            /*
            double closePosition = high - close;
            if (closePosition / range > threshold) {
                // close price not in the top
                continue;
            }

            double openPosition = high - open;
            if (openPosition / range > 0.2) {
                // open position is not in the top
                continue;
            }
            */
            }


            /*
            if (!StrategyUtils.isEmaSlopeUp(currentClosePrice, quotes, true, false)) {
                return false;
            }
            */

        } catch (Exception e) {
            logger.error("Failed to process {} ", ticker, e);
            return false;
        }

        return false;

    }

    @Override
    public String name() {
        return StrategyConstants.HAMMMER;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
