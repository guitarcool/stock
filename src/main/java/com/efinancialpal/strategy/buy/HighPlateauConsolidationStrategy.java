package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 12/7/16.
 * ??????
 */
public class HighPlateauConsolidationStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(HighPlateauConsolidationStrategy.class);
    private static final int TOTAL_CONSIDERED_DAYS = 20;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        try {
            if (quotes.size() < TOTAL_CONSIDERED_DAYS) {
                return false;
            }
            double highPrice = 0;
            int highIndex = 0;
            double highVolume = 0;
            for (int i = 0; i < TOTAL_CONSIDERED_DAYS; ++i) {
                if (highPrice < Math.max(quotes.get(i).getClose().doubleValue(), quotes.get(i).getOpen().doubleValue())) {
                    highPrice = Math.max(quotes.get(i).getClose().doubleValue(), quotes.get(i).getOpen().doubleValue());
                    highVolume = quotes.get(i).getVolume();
                    highIndex = i;
                }
            }
            if (highIndex == 0) {
                // can't be current day;
                return false;
            }
            if (quotes.get(highIndex).getClose().doubleValue() < quotes.get(highIndex).getOpen().doubleValue()) {
                return false;
            }
            if ((quotes.get(highIndex).getClose().doubleValue() - quotes.get(highIndex).getOpen().doubleValue()) / quotes.get(highIndex).getOpen().doubleValue() < 0.03) {
                // increase at least 3%
                return false;
            }

            for (int i = 0; i < highIndex; ++i) {
                double currentHighPrice = Math.max(quotes.get(i).getClose().doubleValue(), quotes.get(i).getOpen().doubleValue());
                if (currentHighPrice / highPrice > 0.97) { // max -3% consolidation
                    return false;
                }
                double currentVolume = quotes.get(i).getVolume();
                if (currentVolume / highVolume > 0.3) {
                    return false;
                }
            }
        } catch (Exception e) {
            logger.error("failed to process {} ", ticker, e);
            return false;
        }
        evaluationContext.populate(ticker, name(), longOrShort());
        return true;
    }

    @Override
    public String name() {
        return StrategyConstants.HIGH_PLATEAU_CONSOLIDATION;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
