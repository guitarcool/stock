package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 12/4/16.
 */
public class RectangleConsolidationBreakoutWithVolumeHikeStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(RectangleConsolidationBreakoutWithVolumeHikeStrategy.class);

    private static final int CONSOLIDATION_DAYS = 125; // half a year
    private static final double CONSOLIDATION_AMPTITUDE_PERCENTAGE = 0.3; // up or down no more than 15%
    private static final int TOTAL_DAYS_TO_CONSIDER = 180; // one and half a year
    private static final int RECENT_BREAKUP_CONSIDERED_DAYS = 20; // break out within 1 month
    private static final double BREAKUP_PERCENTAGE = 0.05;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < TOTAL_DAYS_TO_CONSIDER) {
            return false;
        }
        if (quotes.get(0).getVolume().doubleValue() * quotes.get(0).getClose().doubleValue() < 2000000) {
            // trading volume is too light
            return false;
        }
        double priceAfterBreakup = 0;
        int breakoutIndex = 0;
        double totVolumeAfterBreakout = 0;
        double totVolume = 0;
        for (int index = 0; index < RECENT_BREAKUP_CONSIDERED_DAYS; ++index) {
            double openPrice = quotes.get(index).getOpen().doubleValue();
            double closePrice = quotes.get(index).getAdjClose().doubleValue();
            priceAfterBreakup = Math.max(priceAfterBreakup, Math.max(openPrice, closePrice));
            totVolumeAfterBreakout += quotes.get(index).getVolume().doubleValue();
            totVolume += quotes.get(index).getVolume().doubleValue();
        }

        double highPrice = quotes.get(breakoutIndex + 1).getAdjClose().doubleValue();
        double lowPrice = quotes.get(breakoutIndex + 1).getAdjClose().doubleValue();
        double highPriceOverall = quotes.get(breakoutIndex + 1).getAdjClose().doubleValue();
        double lowPriceOverall = quotes.get(breakoutIndex + 1).getAdjClose().doubleValue();
        for (int i = breakoutIndex + 1; i < TOTAL_DAYS_TO_CONSIDER; ++i) {
            double openPrice = quotes.get(i).getOpen().doubleValue();
            double closePrice = quotes.get(i).getAdjClose().doubleValue();
            if (i < CONSOLIDATION_DAYS) {
                highPrice = Math.max(highPrice, Math.max(openPrice, closePrice));
                lowPrice = Math.min(lowPrice, Math.min(openPrice, closePrice));
            }
            highPriceOverall = Math.max(highPriceOverall, Math.max(openPrice, closePrice));
            lowPriceOverall = Math.min(lowPriceOverall, Math.min(openPrice, closePrice));
            totVolume += quotes.get(i).getVolume().doubleValue();
        }

        if (priceAfterBreakup / highPriceOverall < 1.03) {
            // less than 3% breakup, not a good breakout
            return false;
        }

        if ((totVolumeAfterBreakout / (breakoutIndex + 1)) / (totVolume / TOTAL_DAYS_TO_CONSIDER) < 1.2) {
            // no volume hike after breakout
            return false;
        }

        double amptitude = (highPrice - lowPrice) / 2;

        // consolidation price range should be relative small compared with the overall price change range over a longer term
        if (amptitude / (highPriceOverall - lowPriceOverall) > CONSOLIDATION_AMPTITUDE_PERCENTAGE) {
            return false;
        }

        // missing at least three times the rectangle has been touched

        evaluationContext.populate(ticker, name(), longOrShort());
        return true;
    }

    @Override
    public String name() {
        return StrategyConstants.RECTANGLE_CONSOLIDATION_BREAKOUT_WITH_VOLUME_HIKE;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
