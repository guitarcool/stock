package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 10/20/16.
 */
public class PriceIncreaseWithVolumeHikeStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(PriceIncreaseWithVolumeHikeStrategy.class);

    private static double VOLUME_INCREASE_RATIO_THRESHOLD = 2.0;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        for (int i = 0; i < quotes.size() - 1; ++i) {
            HistoricalQuote todayQuote = quotes.get(i);
            HistoricalQuote yesterdayQuote = quotes.get(i + 1);
            if (todayQuote.getAdjClose().compareTo(yesterdayQuote.getAdjClose()) > 0 && todayQuote.getVolume() > VOLUME_INCREASE_RATIO_THRESHOLD * yesterdayQuote.getVolume()) {
                evaluationContext.populate(ticker, name(), longOrShort());
                return true;
            }
        }
        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.PRICE_INCREASE_WITH_VOLUME_HIGH;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
