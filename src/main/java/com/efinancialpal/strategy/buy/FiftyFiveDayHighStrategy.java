package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 1/21/17.
 */
public class FiftyFiveDayHighStrategy extends Strategy {
    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 60) {
            return false;
        }

        double closePrice = quotes.get(0).getAdjClose().doubleValue();

        for (int i = 1; i < 55; ++i) {
            if (closePrice < quotes.get(i).getHigh().doubleValue()) {
                return false;
            }
        }

        evaluationContext.populate(ticker, name(), longOrShort());

        return true;


    }

    @Override
    public String name() {
        return StrategyConstants.FIFTY_FIVE_DAY_HIGH;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
