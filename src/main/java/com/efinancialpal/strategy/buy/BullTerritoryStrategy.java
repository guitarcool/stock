package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by haiyanluo on 10/22/16.
 *
 * check if the market is in bull territory, this strategy should only apply to weekly chart
 * if it's daily chart, we need to change the year line to 250
 */
public class BullTerritoryStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(BullTerritoryStrategy.class);

    private boolean isWeekly = true;

    private static int BULL_MARKET_THRESHOLD = 50; // 50-week the year line

    public BullTerritoryStrategy() {
        BULL_MARKET_THRESHOLD = 50; // by default it's the weekly chart
        isWeekly = true;
    }

    public BullTerritoryStrategy(boolean isWeekly) {
        if (isWeekly) {
            BULL_MARKET_THRESHOLD =  50; // 50 weeks is the year line
            isWeekly = true;
        }
        else {
            BULL_MARKET_THRESHOLD = 250; // 250 days is the year line
            isWeekly = false;
        }
    }

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.isEmpty()) {
            return false;
        }

        int numOfRecords = quotes.size();
        List<Tick> tickList = new ArrayList<>();

        HistoricalQuote latestQuote = quotes.get(0);

        for (int i = 0; i < numOfRecords; ++i) {
            HistoricalQuote quote = quotes.get(i);
            DateTime dateTime = new DateTime(quote.getDate().getTime());
            Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());
            tickList.add(tick) ;
        }
        Collections.reverse(tickList);

        TimeSeries timeSeries = new TimeSeries(tickList);

        EMAIndicator ema = new EMAIndicator(new ClosePriceIndicator(timeSeries), BULL_MARKET_THRESHOLD);

        double latestPrice = latestQuote.getAdjClose().doubleValue();
        double ema50 = ema.getValue(numOfRecords - 1).toDouble();

        evaluationContext.populate(ticker, name(), longOrShort());
        return latestPrice > ema50;
    }

    @Override
    public String name() {
        return StrategyConstants.BULL_TERRITORY;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
