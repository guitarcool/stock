package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.MyStochasticOscillatorKIndicator;
import com.efinancialpal.strategy.utils.StrategyConstants;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by haiyanluo on 1/26/17.
 */
public class StochasticOverSoldStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(StochasticOverSoldStrategy.class);
    private final static int DURATION = 12;

    private final static double RSI_OVERSOLD_THRESHOLD = 20;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 14) {
            return false;
        }
        List<Tick> tickList = new ArrayList<>();
        List<DateTime> dateTimes = new ArrayList<>();
        for (int i = 0; i < quotes.size(); ++i) {
            HistoricalQuote quote = quotes.get(i);
            DateTime dateTime = new DateTime(quote.getDate().getTime()); // notice by default it's using UTC, which is fine for now
            Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());
            tickList.add(tick);
            dateTimes.add(dateTime);
        }

        TimeSeries timeSeries = new TimeSeries(tickList);
        Collections.reverse(tickList); // bug ?? why we reverse tickList
        ClosePriceIndicator closePriceIndicator = new ClosePriceIndicator(timeSeries);
        MyStochasticOscillatorKIndicator kIndicator = new MyStochasticOscillatorKIndicator(timeSeries, DURATION);
        double k = kIndicator.getK(0).toDouble();
        if (k < RSI_OVERSOLD_THRESHOLD) {
            evaluationContext.populate(ticker, name(), longOrShort());
            return true;
        }
        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.STOCHASTIC_OVERSOLD;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
