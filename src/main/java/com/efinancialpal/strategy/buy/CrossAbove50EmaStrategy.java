package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by haiyanluo on 3/11/17.
 */
public class CrossAbove50EmaStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(CrossAbove50EmaStrategy.class);

    private static final int EMA_INTERVAL = 50;
    private static final int CONSIDERED_DAYS = 10;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 100) {
            return false;
        }

        try {

            List<Tick> tickList = new ArrayList<>();
            for (int i = 0; i < quotes.size(); ++i) {
                HistoricalQuote quote = quotes.get(i);
                DateTime dateTime = new DateTime(quote.getDate().getTime()); // notice by default it's using UTC, which is fine for now
                Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());
                tickList.add(tick);
            }
            Collections.reverse(tickList);
            TimeSeries timeSeries = new TimeSeries(tickList);
            EMAIndicator emaIndicator50 = new EMAIndicator(new ClosePriceIndicator(timeSeries), EMA_INTERVAL);

            double latestPrice = quotes.get(0).getAdjClose().doubleValue();
            double weekly30Price = emaIndicator50.getValue(0).toDouble();
            if (latestPrice < weekly30Price) {
                return false;
            }
            if (weekly30Price < emaIndicator50.getValue(1).toDouble()) { // ema-50 slope positive
                return false;
            }

            double priceTenDaysAgo = quotes.get(CONSIDERED_DAYS).getClose().doubleValue();
            if (priceTenDaysAgo > emaIndicator50.getValue(CONSIDERED_DAYS).toDouble()) {
                return false;
            }

            double lowestPrice = quotes.get(0).getLow().doubleValue();
            double highestPrice = quotes.get(0).getHigh().doubleValue();
            for (int i = 1; i < CONSIDERED_DAYS; ++i) {
                if (quotes.get(i).getLow().doubleValue() < lowestPrice) {
                    lowestPrice = quotes.get(i).getLow().doubleValue();
                }
                if (quotes.get(i).getHigh().doubleValue() > highestPrice) {
                    highestPrice = quotes.get(i).getHigh().doubleValue();
                }
            }
            if ((highestPrice - lowestPrice) / lowestPrice < 0.1) {
                return false;
            }
        } catch (Exception e) {
            logger.error("Failed to process {} ", ticker, e);
            return false;
        }

        evaluationContext.populate(ticker, name(), longOrShort());
        return true;

    }

    @Override
    public String name() {
        return StrategyConstants.CROSS_ABOVE_50EMA;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
