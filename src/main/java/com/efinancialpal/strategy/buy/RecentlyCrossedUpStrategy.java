package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by haiyanluo on 10/21/16.
 */
public class RecentlyCrossedUpStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(RecentlyCrossedUpStrategy.class);
    private static final int LONG_TERM = 50;
    private static final int SHORT_TERM = 20;
    private static final int RECENCY = 50;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        int numOfRecords = quotes.size();
        List<Tick> tickList = new ArrayList<>();

        if (quotes.size() < RECENCY) {
            logger.error("too less records for ticker {}", ticker);
            return false;
        }

        for (int i = 0; i < numOfRecords; ++i) {
            HistoricalQuote quote = quotes.get(i);
            DateTime dateTime = new DateTime(quote.getDate().getTime());
            Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());
            tickList.add(tick) ;
        }
        Collections.reverse(tickList);

        TimeSeries shortTimeSeries = new TimeSeries(tickList);
        TimeSeries longTimeSeries = new TimeSeries(tickList);

        EMAIndicator shortTermEma = new EMAIndicator(new ClosePriceIndicator(shortTimeSeries), SHORT_TERM);
        EMAIndicator longTermEma = new EMAIndicator(new ClosePriceIndicator(longTimeSeries), LONG_TERM);

        int startTime = numOfRecords - RECENCY + 1;
        if (shortTermEma.getValue(startTime).toDouble() > longTermEma.getValue(startTime).toDouble()) {
            logger.info("{} days/weeks ago, ticker {}'s short term price {} is already above long term price {} ", RECENCY, ticker, shortTermEma.getValue(startTime), longTermEma.getValue(startTime));
            return false;
        }

        for (int i = startTime + 2; i < numOfRecords; i ++) {
            if (shortTermEma.getValue(i).toDouble() > longTermEma.getValue(i).toDouble()) {
                evaluationContext.populate(ticker, name(), longOrShort());
                return true;
            }
        }
        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.RECENTLY_CROSSED_UP;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
