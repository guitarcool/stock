package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 11/16/16.
 *
 * last day is up with body completely embraces the day before - which was down
 */
public class EmbraceLongStrategy extends Strategy {
    private static final int CONSIDERED_DAYS = 2;
    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 10) {
            return false;
        }

        for (int i = 0; i < CONSIDERED_DAYS; ++i) {
            HistoricalQuote quote0 = quotes.get(0);
            double open0 = quote0.getOpen().doubleValue();
            double close0 = quote0.getClose().doubleValue();
            double low = quote0.getLow().doubleValue();
            double high = quote0.getHigh().doubleValue();
            if ((high - low) / low < 0.03) {
                // too short
               continue;
            }
            if (open0 >= close0) {
                // should be up day
                continue;
            }

            HistoricalQuote quote1 = quotes.get(1);
            double open1 = quote1.getOpen().doubleValue();
            double close1 = quote1.getClose().doubleValue();
            if (open1 <= close1) {
                // should be down day
                continue;
            }

            if (open0 >= close1) {
                continue;
            }

            if (open1 >= close0) {
                continue;
            }

            if (close0 < close1 + (open1 - close1) * 0.75) {
                continue;
            }

            int j = i + 1;
            for (; j < 10; ++j) {
                // at least 10 day low
                if (low > quotes.get(j).getLow().doubleValue()) {
                    break;
                }
            }
            if (j < 10) {
                continue;
            }
            evaluationContext.populate(ticker, name(), longOrShort());
            return true;
        }

        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.EMBRACE_LONG;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
