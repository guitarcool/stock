package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.swagger.service.MyMacdIndicator;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by haiyanluo on 10/16/16.
 */
// find stocks that have shown MACD bottom divergence on (weekly or daily chart)
public class BottomMacdDivergenceStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(BottomMacdDivergenceStrategy.class);
    private static final int MIN_W_BOTTOM_WIDTH = 15; // 3 weeks
    private static final int MAX_W_BOTTOM_WIDTH = 50; // 10 weeks
    private static final int LOWEST_PRICE_HAPPENED = 5; // within 1 week
    private static final int TOTAL_RECORDS_TO_CONSIDER = 200;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        try {
            if (quotes.size() < TOTAL_RECORDS_TO_CONSIDER) {
                return false;
            }
            HistoricalQuote lowestQuote = quotes.get(0);
            int lowestIndex = 0;
            List<Tick> tickList = new ArrayList<>();
            List<DateTime> dateTimes = new ArrayList<>();
            for (int i = 0; i < quotes.size(); ++i) {
                HistoricalQuote quote = quotes.get(i);
                if (i < LOWEST_PRICE_HAPPENED && quote.getLow().compareTo(lowestQuote.getLow()) < 0) {
                    lowestQuote = quote;
                    lowestIndex = i;
                }
                DateTime dateTime = new DateTime(quote.getDate().getTime()); // notice by default it's using UTC, which is fine for now
                Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());
                tickList.add(tick);
                dateTimes.add(dateTime);
            }

            if (lowestIndex >= LOWEST_PRICE_HAPPENED) {
                // lowest price happened over the last 10-day period
                return false;
            }

            TimeSeries timeSeries = new TimeSeries(tickList);
            Collections.reverse(tickList); // bug ?? why we reverse tickList
            ClosePriceIndicator closePriceIndicator = new ClosePriceIndicator(timeSeries);
            MyMacdIndicator macd = new MyMacdIndicator(closePriceIndicator, 12, 26, dateTimes);

            int startIndex = lowestIndex + MIN_W_BOTTOM_WIDTH; // move back some days/weeks to avoid the w-bottom being too narrow
            if (startIndex >= quotes.size()) {
                return false;
            }
            HistoricalQuote secondLowestQuote = quotes.get(startIndex);
            int secondLowestIndex = startIndex;
            for (int i = startIndex + 1; i < TOTAL_RECORDS_TO_CONSIDER && i < quotes.size(); ++i) {
                HistoricalQuote quote = quotes.get(i);
                if (quote.getLow().compareTo(secondLowestQuote.getLow()) < 0) {
                    secondLowestQuote = quote;
                    secondLowestIndex = i;
                }
            }

            // lowest price is lower than second lowest price
            double secondLowestPrice = secondLowestQuote.getLow().doubleValue();
            if (secondLowestPrice <= lowestQuote.getLow().doubleValue()) {
                return false;
            }

            // move back and forth for to check if there is additional low compared with second low
            // make sure the second low is the lowest among a region
            for (int i = secondLowestIndex - 10; i <= secondLowestIndex + 10; ++i) {
                if (quotes.get(i).getLow().doubleValue() < secondLowestPrice) {
                    return false;
                }
            }


            // check between the two lows if there is a price increase
            boolean priceHasRaisedInBetween = false;
            for (int i = lowestIndex; i < secondLowestIndex; ++i) {
                if (quotes.get(i).getClose().doubleValue() > quotes.get(secondLowestIndex).getClose().doubleValue()) {
                    priceHasRaisedInBetween = true;
                    break;
                }
            }
            if (!priceHasRaisedInBetween) {
                return false;
            }


            if (secondLowestIndex - lowestIndex < MIN_W_BOTTOM_WIDTH || secondLowestIndex - lowestIndex > MAX_W_BOTTOM_WIDTH) {
                // the W bottom is too wide
                return false;
            }


            // check CMF over the past 20 days
            if (!checkCMF(quotes, lowestIndex, secondLowestIndex)) {
                return false;
            }

            boolean hasPositiveMacd = false;

            lowestIndex = quotes.size() - 1 - lowestIndex;
            secondLowestIndex = quotes.size() - 1 - secondLowestIndex;

            for (int i = secondLowestIndex + 1; i < lowestIndex; ++i) {
                if (macd.getHistogram(i) > 0) {
                    hasPositiveMacd = true;
                    break;
                }
            }

            if (!hasPositiveMacd) {
                // macd needs to have positive value in between the two lows
                return false;
            }


            // convert the indexes since MACD was reversed above
            if (macd.getHistogram(lowestIndex) < 0 && macd.getHistogram(secondLowestIndex) < 0 && macd.getHistogram(lowestIndex) > macd.getHistogram(secondLowestIndex)) {
                evaluationContext.populate(ticker, name(), longOrShort());
                return true;
            }

            return false;
        } catch (Exception e) {
            logger.error("Failed to process {}", ticker, e);
            return false;
        }
    }

    public boolean checkCMF(List<HistoricalQuote> quotes, int lowestIndex, int secondLowestIndex) {
        double lowIndexCMF = getCMF(quotes, lowestIndex);
        double secondLowIndexCMF = getCMF(quotes, secondLowestIndex);
        return lowIndexCMF >= secondLowIndexCMF;
    }

    private double getCMF(List<HistoricalQuote> quotes, int index) {
        double sumMoneyFlowVolume = 0;
        double sumVolume = 0;
        for (int i = index; i < index + 20; ++i) {
            HistoricalQuote quote = quotes.get(i);
            double multiplier  = ((quote.getClose().doubleValue() - quote.getLow().doubleValue()) - ((quote.getHigh().doubleValue() - quote.getClose().doubleValue()))) / (quote.getHigh().doubleValue() - quote.getLow().doubleValue());
            double moneyFlowVolume = multiplier * quote.getVolume();
            sumMoneyFlowVolume += moneyFlowVolume;
            sumVolume += quote.getVolume().doubleValue();
        }
        double cmf = sumMoneyFlowVolume / sumVolume;
        return cmf ;
    }

    @Override
    public String name() {
        return StrategyConstants.BOTTOM_MACD_DIVERGENCE;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
