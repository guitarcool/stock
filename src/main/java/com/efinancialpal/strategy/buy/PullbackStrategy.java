package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.strategy.utils.StrategyUtils;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 11/14/16.
 *
 * pull back 4-20 days with lower volume consolidation, before that there was a 10-day high
 */
public class PullbackStrategy extends Strategy {
    private static final int TOTAL_DAYS_TO_CONSIDER = 20;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 200) {
            // should have more than 1 year data
            return false;
        }

        double high = quotes.get(0).getHigh().doubleValue();
        double low = quotes.get(0).getLow().doubleValue();
        double currentClosePrice = quotes.get(0).getClose().doubleValue();

        int highIndex = 0;
        int lowIndex = 0;
        for (int i = 1; i < TOTAL_DAYS_TO_CONSIDER; ++i) {
            if (quotes.get(i).getHigh().doubleValue() > high) {
                high = quotes.get(i).getHigh().doubleValue();
                highIndex = i;
            }
            if (quotes.get(i).getLow().doubleValue() < low) {
                low = quotes.get(i).getLow().doubleValue();
                lowIndex = i;
            }
        }
        // continue so that we can get the lowest price of the past 30 days
        for (int i = TOTAL_DAYS_TO_CONSIDER; i < 30; ++i) {
            if (quotes.get(i).getLow().doubleValue() < low) {
                low = quotes.get(i).getLow().doubleValue();
                lowIndex = i;
            }

            if (quotes.get(i).getHigh().doubleValue() > high) {
                // highest price was 20 days ago
                return false;
            }
        }

        if (lowIndex <= highIndex) {
            // consolidation to the lowest price
            return false;
        }

        // over the past 4-20 days, we have a highest price.
        // If it's not within the range, not a candidate.
        if (highIndex < 4 || highIndex > 20) {
            return false;
        }

        double previousAvgVolume = 0;
        for (int i = highIndex; i < highIndex + 5; ++i) { // 5 days leading up to the highest price
            previousAvgVolume += quotes.get(i).getVolume();
        }
        previousAvgVolume = previousAvgVolume * 1.0 / 5;

        if (previousAvgVolume * high < 2000000) {
            // average daily trading volume is less than $2m
            return false;
        }

        double downAvgVolume = 0;
        for (int i = 0; i < highIndex; ++i) {
            double currentDayVolume = quotes.get(i).getVolume();
            if (currentDayVolume > 1.1 * previousAvgVolume && quotes.get(i).getClose().doubleValue() < quotes.get(i + 1).getClose().doubleValue()) {
                // down price has one day with too much volume
                return false;
            }
            downAvgVolume += currentDayVolume;
        }
        downAvgVolume = downAvgVolume * 1.0 / highIndex;

        if (downAvgVolume > 0.6 * previousAvgVolume) {
            return false;
        }

        // check if the correction is too deep
        double lowClose = quotes.get(0).getClose().doubleValue();
        for (int i = 1; i < highIndex; ++i) {
            if (quotes.get(i).getClose().doubleValue() < lowClose) {
                lowClose = quotes.get(i).getClose().doubleValue();
            }
        }
        if ((high - lowClose) / (high - low) > 0.6) {
            // the golden ratio of maximum is 0.62 something
            // the correction is too deep
            return false;
        }

        /* buggy
        boolean priceAlmostNoChange = true;
        for (int i = 1; i < highIndex; ++i) {
            if (quotes.get(i).getClose().doubleValue() / quotes.get(i - 1).getClose().doubleValue() > 0.995) {
                priceAlmostNoChange = false;
                break;
            }
        }
        if (priceAlmostNoChange) {
            // this stock is probably acquired or something, no change of price at all after price high
            return false;
        }
        */


        // check the latest price is around or above 50-day average
        /*
        List<Tick> tickList = new ArrayList<>();
        for (int i = 0; i < quotes.size(); ++i) {
            HistoricalQuote quote = quotes.get(i);
            DateTime dateTime = new DateTime(quote.getDate().getTime());
            Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getClose().doubleValue(), quote.getVolume().doubleValue());
            tickList.add(tick) ;
        } */

        if (!StrategyUtils.isEmaSlopeUp(currentClosePrice, quotes, true /* long */)) {
            return false;
        }

        /*
        TimeSeries timeSeries = new TimeSeries(tickList);
        Collections.reverse(tickList);

        int daysToAverage50 = 50;
        EMAIndicator ema50 = new EMAIndicator(new ClosePriceIndicator(timeSeries), daysToAverage50);

        double latestPrice = quotes.get(0).getClose().doubleValue();
        double current50DayAveragePrice = ema50.getValue(quotes.size() - 1).toDouble();

        int daysToAverage250 = 250;
        EMAIndicator ema250 = new EMAIndicator(new ClosePriceIndicator(timeSeries), daysToAverage250);
        double current250DayAveragePrice = ema250.getValue(quotes.size() - 1).toDouble();

        // check if the 250 moving average is slope upward
        if (current250DayAveragePrice < ema250.getValue(quotes.size() - 2).toDouble()) {
            return false;
        }
        */

        /*
        if (latestPrice < current50DayAveragePrice && Math.abs(latestPrice - current50DayAveragePrice) / current50DayAveragePrice > 0.1) {
            // below 50-day (10-week line) price too much > 10%
            return false;
        }

        if (latestPrice < current250DayAveragePrice) {
            // in bear territory
            return false;
        }
        */

        // its around the one year high
        /*
        double oneYearHigh = 0;
        for (int i = 0; i < 200; ++i) {
            if (oneYearHigh < quotes.get(i).getHigh().doubleValue()) {
                oneYearHigh = quotes.get(i).getHigh().doubleValue();
            }
        }
        if (Math.abs(oneYearHigh - high) / oneYearHigh > 0.5) {
            return false;
        }
        */

        /*
        for (int i = 0; i < 50; ++i) {
            double todayPrice = quotes.get(i).getClose().doubleValue();
            double yesterdayPrice = quotes.get(i + 1).getClose().doubleValue();
            if (todayPrice / yesterdayPrice > 1.16 || todayPrice / yesterdayPrice < 0.95) {
                // single day huge up or down outlier
                return false;
            }
        }
        */

        evaluationContext.populate(ticker, name(), longOrShort());
        return true;
    }

    @Override
    public String name() {
        return StrategyConstants.PULLBACK_LONG;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
