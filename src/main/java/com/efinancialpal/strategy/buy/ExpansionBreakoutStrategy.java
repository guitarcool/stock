package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.strategy.utils.StrategyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 10/15/17.
 * Hit and Run: Expansion Breakout
 */
public class ExpansionBreakoutStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(ExpansionBreakoutStrategy.class);
    private static final int MAX_DAY_CONSIDERED = 60;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < MAX_DAY_CONSIDERED + 1) {
            return false;
        }

        try {
            double todayHigh = quotes.get(0).getHigh().doubleValue();
            double todayLow = quotes.get(0).getLow().doubleValue();
            double todayClose = quotes.get(0).getClose().doubleValue();
            double todayRange = todayHigh - todayLow;
            double yesterdayClose = quotes.get(1).getClose().doubleValue();

            // 9 day max range
            for (int i = 1; i < 9; ++i) {
                double curRange = quotes.get(i).getHigh().doubleValue() - quotes.get(i).getLow().doubleValue();
                if (todayRange < curRange) {
                    return false;
                }
            }

            // long candidate
            int i = 1;
            for (i = 1; i < MAX_DAY_CONSIDERED; ++i) {
                double curHigh = quotes.get(i).getHigh().doubleValue();
                if (curHigh > todayHigh) {
                    break;
                }
            }
            // check if there is too much top shadow
            double topShadow = quotes.get(0).getHigh().doubleValue() - Math.max(quotes.get(0).getClose().doubleValue(), quotes.get(0).getOpen().doubleValue());
            double range = quotes.get(0).getHigh().doubleValue() - quotes.get(0).getLow().doubleValue();
            boolean hasLongTopShadow = false;
            if (topShadow / range > 0.1) {
                hasLongTopShadow = true;
            }
            if (!hasLongTopShadow && quotes.get(0).getClose().doubleValue() > quotes.get(0).getOpen().doubleValue() && i == MAX_DAY_CONSIDERED && todayClose > yesterdayClose && StrategyUtils.isEmaSlopeUp(todayClose, quotes, true /* long */)) {
                evaluationContext.populate(ticker, name(), StrategyConstants.LONG);
                return true;
            }

            // short candidate
            i = 1;
            for (i = 1; i < MAX_DAY_CONSIDERED; ++i) {
                double curLow = quotes.get(i).getLow().doubleValue();
                if (curLow < todayLow) {
                    break;
                }
            }
            // check if there is too much bottom shadow
            double bottomShadow = Math.min(quotes.get(0).getClose().doubleValue(), quotes.get(0).getOpen().doubleValue()) - quotes.get(0).getLow().doubleValue();
            boolean hasLongBottomShadow = false;
            if (bottomShadow / range > 0.1) {
                hasLongBottomShadow = true;
            }
            if (!hasLongBottomShadow && quotes.get(0).getClose().doubleValue() < quotes.get(0).getOpen().doubleValue() && i == MAX_DAY_CONSIDERED && todayClose < yesterdayClose && StrategyUtils.isEmaSlopeUp(todayClose, quotes, false /* short */)) {
                evaluationContext.populate(ticker, name(), StrategyConstants.SHORT);
                return true;
            }
        } catch (Exception e) {
            logger.error("Unable to process {}", ticker, e);
            return false;
        }
        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.EXPANSION_BREAKOUT;
    }

    @Override
    public String longOrShort() {
        // could be both actually
        return StrategyConstants.LONG;
    }
}
