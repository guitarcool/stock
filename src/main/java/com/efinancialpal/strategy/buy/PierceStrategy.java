package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 11/28/16.
 */
public class PierceStrategy extends Strategy {
    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 2) {
            return false;
        }
        HistoricalQuote quote0 = quotes.get(0);
        HistoricalQuote quote1 = quotes.get(1);

        // current day is up
        if (quote0.getOpen().doubleValue() >= quote0.getClose().doubleValue()) {
            return false;
        }

        // previous day is down
        if (quote1.getOpen().doubleValue() <= quote1.getClose().doubleValue()) {
            return false;
        }

        // gap down
        if (quote1.getOpen().doubleValue() >= quote0.getClose().doubleValue()) {
            return false;
        }

        double yesterdayBodyLength = quote1.getOpen().doubleValue() - quote1.getClose().doubleValue();

        // pierce half of yesterday's body
        if (quote0.getClose().doubleValue() <= yesterdayBodyLength / 2 + quote1.getClose().doubleValue()) {
            return false;
        }

        // price is up >= 2%
        if ((quote0.getClose().doubleValue() - quote0.getOpen().doubleValue()) / quote0.getOpen().doubleValue() < 0.02) {
            return false;
        }


        evaluationContext.populate(ticker, name(), longOrShort());
        return true;
    }

    @Override
    public String name() {
        return StrategyConstants.PIERCE_LONG;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
