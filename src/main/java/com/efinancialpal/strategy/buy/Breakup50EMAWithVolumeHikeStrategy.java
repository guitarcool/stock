package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by haiyanluo on 12/4/16.
 *
 * recently break up 50-day EMA with increasing volume
 */
public class Breakup50EMAWithVolumeHikeStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(Breakup50EMAWithVolumeHikeStrategy.class);
    private static final int EMA_INTERVAL = 30;
    private static final int TIME_TO_CROSS_UP = 20;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        try {
            if (quotes.size() < 50) {
                return false;
            }

            List<Tick> tickList = new ArrayList<>();
            for (int i = 0; i < quotes.size(); ++i) {
                HistoricalQuote quote = quotes.get(i);
                DateTime dateTime = new DateTime(quote.getDate().getTime()); // notice by default it's using UTC, which is fine for now
                Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());
                tickList.add(tick);
            }
            Collections.reverse(tickList);
            TimeSeries timeSeries = new TimeSeries(tickList);
            EMAIndicator emaIndicator30 = new EMAIndicator(new ClosePriceIndicator(timeSeries), EMA_INTERVAL);

            double latestPrice = quotes.get(0).getAdjClose().doubleValue();
            if (latestPrice < emaIndicator30.getValue(0).toDouble()) {
                return false;
            }

            double previousPrice = quotes.get(TIME_TO_CROSS_UP - 1).getAdjClose().doubleValue();
            if (previousPrice > emaIndicator30.getValue(TIME_TO_CROSS_UP - 1).toDouble()) {
                // break up sometimes ago, not a recent breakup
                return false;
            }

            int index = TIME_TO_CROSS_UP;
            int highestVolumeIndex = index;
            double highestVolume = 0;
            for (; index >= 0; --index) {
                if (quotes.get(index).getVolume().doubleValue() > highestVolume) {
                    highestVolume = quotes.get(index).getVolume().doubleValue();
                    highestVolumeIndex = index;
                }
            }
            if (quotes.get(highestVolumeIndex).getAdjClose().doubleValue() < emaIndicator30.getValue(highestVolumeIndex).toDouble()) {
                return false;
            }
            double prevAvgVolume = 0;
            for (int i = highestVolumeIndex + 1; i < highestVolumeIndex + 10; ++i) {
                prevAvgVolume += quotes.get(i).getVolume().doubleValue();
            }
            prevAvgVolume /= 10;

            if (quotes.get(highestVolumeIndex).getVolume().doubleValue() < 5 * prevAvgVolume) {
                return false;
            }


        /*
        double breakoutAverageVolume = 0;
        for (int j = 0; j <= index; ++j) {
           breakoutAverageVolume += quotes.get(j).getVolume().doubleValue();
        }
        breakoutAverageVolume /= (index + 1);

        double overallAverageVolume = 0;
        int avgVolumeDays = EMA_INTERVAL;
        for (int j = 0; j < avgVolumeDays; ++j) {
           overallAverageVolume += quotes.get(j).getVolume().doubleValue();
        }
        overallAverageVolume /= EMA_INTERVAL;

        if (breakoutAverageVolume < 5 * overallAverageVolume) {
            // breakout volume is low, not a good breakout
            return false;
        }

        if (breakoutAverageVolume * latestPrice < 1000000) {
            // trading is too light
            return false;
        }
        */

        /*

            RSIIndicator rsi = new RSIIndicator(new ClosePriceIndicator(timeSeries), 14);
            double value = rsi.getValue(quotes.size() - 1).toDouble();
            // check if it's above 69.1
            if (rsi.getValue(quotes.size() - 1).toDouble() < 69.1) {
                return false;
            }
            */
        } catch (Exception e) {
            logger.error("Failed to process {} ", ticker, e);
            return false;
        }
        evaluationContext.populate(ticker, name(), longOrShort());
        return true;

    }

    @Override
    public String name() {
        return StrategyConstants.BREAKUP_50EMA_WITH_VOLUME_HIKE;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
