package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by haiyanluo on 10/16/16.
 */
// 10 week price is above 20 week price and 20 week price is above 50 week price
public class AveragePriceStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(AveragePriceStrategy.class);

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {

        BigDecimal total10WeeksPrice = new BigDecimal(0);
        BigDecimal total20WeeksPrice = new BigDecimal(0);
        BigDecimal total50WeeksPrice = new BigDecimal(0);
        int counter = 0;
        for (HistoricalQuote quote : quotes) {
            if (counter < 10) {
                total10WeeksPrice = total10WeeksPrice.add(quote.getAdjClose());
            }
            if (counter < 20) {
                total20WeeksPrice = total20WeeksPrice.add(quote.getAdjClose());
            }
            if (counter < 50) {
                total50WeeksPrice = total50WeeksPrice.add(quote.getAdjClose());
            }

            if (++counter >= 50) {
                break;
            }
        }
        BigDecimal avg10WeeksPrice = total10WeeksPrice.divide(new BigDecimal(10));
        BigDecimal avg20WeeksPrice = total20WeeksPrice.divide(new BigDecimal(20));
        BigDecimal avg50WeeksPrice = total50WeeksPrice.divide(new BigDecimal(50));

        if (avg10WeeksPrice.compareTo(avg20WeeksPrice) > 0 && avg20WeeksPrice.compareTo(avg50WeeksPrice) > 0) {
            evaluationContext.populate(ticker, name(), longOrShort());
            return true;
        }

        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.AVERAGE_PRICE_LONG;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
