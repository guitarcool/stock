package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by haiyanluo on 10/22/16.
 */
public class RecentlyBecameBullStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(RecentlyBecameBullStrategy.class);

    private static final int BULL_MARKET_WEEKS = 50; // 50 weeks year line
    private static final int RECENCY = 10; // over the past 10 weeks

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.isEmpty()) {
            return false;
        }

        int numOfRecords = quotes.size();
        List<Tick> tickList = new ArrayList<>();

        HistoricalQuote latestQuote = quotes.get(0);

        for (int i = 0; i < numOfRecords; ++i) {
            HistoricalQuote quote = quotes.get(i);
            DateTime dateTime = new DateTime(quote.getDate().getTime());
            Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());
            tickList.add(tick) ;
        }
        Collections.reverse(tickList);

        TimeSeries timeSeries = new TimeSeries(tickList);

        EMAIndicator ema = new EMAIndicator(new ClosePriceIndicator(timeSeries), BULL_MARKET_WEEKS);

        int i = 0;
        int j = quotes.size() - 1;
        int count = 0;
        while (i < j && count < RECENCY) {
            HistoricalQuote quote = quotes.get(i);
            double price = quote.getAdjClose().doubleValue();
            double emaPrice = ema.getValue(j).toDouble();
            if (price >= emaPrice) {
                // check if it's below 50 weeks line before
                if (i + 1 >= quotes.size() || j - 1 < 0) {
                    // edge case
                    break;
                }
                double lastWeekPrice = quotes.get(i + 1).getAdjClose().doubleValue();
                double lastWeekEmaPrice = ema.getValue(j - 1).toDouble();
                if (lastWeekPrice < lastWeekEmaPrice)  {
                    evaluationContext.populate(ticker, name(), longOrShort());
                    return true;
                }
            }
            ++i;
            --j;
            ++count;
        }
        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.RECENTLY_BECAME_BULL;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
