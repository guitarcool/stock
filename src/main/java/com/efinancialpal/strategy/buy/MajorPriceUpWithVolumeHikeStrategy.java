package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.strategy.utils.StrategyUtils;
import com.efinancialpal.swagger.service.StockMain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 11/23/16.
 */
public class MajorPriceUpWithVolumeHikeStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(StockMain.class);

    private static double PRICE_UP_THRESHOLD = 1.04; // price up 4%
    private static double VOLUME_UP_THRESHOLD = 3; // volume up 500%
    private static final int CONSIDERED_DAYS = 35; // over the past 35 days

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {

        if (quotes.size() <= 61) {
            return false;
        }

        try {
            double avgVolume = StrategyUtils.getAverageVolume(quotes, 20);
        /*
        if (avgVolume < 200000) {
            return false;
        }
        */
            double currentClosePrice = quotes.get(0).getClose().doubleValue();

            for (int i = 0; i < CONSIDERED_DAYS; ++i) {
                double todayOpen = quotes.get(i).getOpen().doubleValue();
                double todayClose = quotes.get(i).getClose().doubleValue();
                double todayHigh = quotes.get(i).getHigh().doubleValue();
                double todayVolume = quotes.get(i).getVolume().doubleValue();
                double yesterdayOpen = quotes.get(i + 1).getOpen().doubleValue();
                double yesterdayClose = quotes.get(i + 1).getClose().doubleValue();
                double yesterdayVolume = quotes.get(i + 1).getVolume().doubleValue();

                /*
                if (todayOpen <= yesterdayClose) {
                    continue;
                } */
                if (todayClose / yesterdayClose < PRICE_UP_THRESHOLD) {
                    continue;
                }
                if (todayVolume / avgVolume < VOLUME_UP_THRESHOLD) {
                    continue;
                }

                // check if there is too much top shadow
                /*
                double topShadow = quotes.get(i).getHigh().doubleValue() - Math.max(quotes.get(i).getClose().doubleValue(), quotes.get(i).getOpen().doubleValue());
                double range = quotes.get(i).getHigh().doubleValue() - quotes.get(i).getLow().doubleValue();
                if (topShadow / range > 0.1) {
                    continue;
                }
                */

                if (!StrategyUtils.isEmaSlopeUp(currentClosePrice, quotes, true /* up */,  false /* relaxed uptrend */)) {
                    continue;
                }
                evaluationContext.populate(ticker, name(), longOrShort());
                return true;

            /*
            double topShadow = todayHigh - todayClose;
            if (topShadow > (todayClose - todayOpen)) {
                continue;
            }
            */
            }
        } catch (Exception e) {
            logger.error("Error on processing ticker {}", ticker, e);
            return false;
        }

        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.MAJOR_PRICE_UP_WITH_VOLUME_HIKE_LONG;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
