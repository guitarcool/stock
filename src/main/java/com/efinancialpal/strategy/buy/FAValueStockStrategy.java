package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.swagger.model.StockStats;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 12/1/16.
 */
public class FAValueStockStrategy extends Strategy {
    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        List<StockStats> stocks = StockStats.find("ticker = ?", ticker);
        if (stocks == null || stocks.isEmpty() || stocks.size() > 1) {
            return false;
        }
        StockStats stock = stocks.get(0);
        if (stock.getPe() != null && stock.getPe().doubleValue() < 0) {
            return false;
        }
        if (stock.getPe() != null && stock.getPe().doubleValue() > 100) {
            return false;
        }
        if (stock.getPeg() != null && stock.getPeg().doubleValue() < 0) {
            return false;
        }
        if (stock.getPeg() != null && stock.getPeg().doubleValue() > 10) {
            return false;
        }
        evaluationContext.populate(ticker, name(), longOrShort());
        return true;
    }

    @Override
    public String name() {
        return StrategyConstants.FA_VALUE_STOCK;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
