package com.efinancialpal.strategy.buy;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 12/4/16.
 *
 * horizontal consolidation for 6 months with little price movement:
 * if breaking up, very bullish
 * this one only finds the watch list, not breaking up yet
 * To find out the stocks that recently breaks out through rectangle consolidation, refer to RectangelConsolidationBreakoutWithVolumeHikeStrategy
 */
public class RectangleConsolidationStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(RectangleConsolidationStrategy.class);

    private static final int CONSOLIDATION_DAYS = 125; // half a year
    private static final double CONSOLIDATION_AMPTITUDE_PERCENTAGE = 0.2; // up or down no more than 15%
    private static final int TOTAL_DAYS_TO_CONSIDER = 375; // one and half a year

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < TOTAL_DAYS_TO_CONSIDER) {
            return false;
        }
        if (quotes.get(0).getVolume().doubleValue() * quotes.get(0).getClose().doubleValue() < 2000000) {
            // trading volume is too light
            return false;
        }

        double highPrice = quotes.get(0).getAdjClose().doubleValue();
        double lowPrice = quotes.get(0).getAdjClose().doubleValue();
        double highPriceOverall = quotes.get(0).getAdjClose().doubleValue();
        double lowPriceOverall = quotes.get(0).getAdjClose().doubleValue();
        for (int i = 0; i < TOTAL_DAYS_TO_CONSIDER; ++i) {
            double openPrice = quotes.get(i).getOpen().doubleValue();
            double closePrice = quotes.get(i).getAdjClose().doubleValue();
            if (i < CONSOLIDATION_DAYS) {
                if (closePrice > highPrice) {
                    highPrice = closePrice;
                }
                if (openPrice > highPrice) {
                    highPrice = openPrice;
                }
                if (closePrice < lowPrice) {
                    lowPrice = closePrice;
                }
                if (openPrice < lowPrice) {
                    lowPrice = openPrice;
                }
            }
            if (closePrice > highPriceOverall) {
                highPriceOverall = closePrice;
            }
            if (openPrice > highPriceOverall) {
                highPriceOverall = openPrice;
            }
            if (closePrice < lowPriceOverall) {
                lowPriceOverall = closePrice;
            }
            if (openPrice < lowPriceOverall) {
                lowPriceOverall = openPrice;
            }
        }
        double amptitude = (highPrice - lowPrice) / 2;

        // consolidation price range should be relative small compared with the overall price change range over a longer term
        if (amptitude / (highPriceOverall - lowPriceOverall) > CONSOLIDATION_AMPTITUDE_PERCENTAGE) {
            return false;
        }

        // missing at least three times the rectangle was touched

        evaluationContext.populate(ticker, name(), longOrShort());

        return true;
    }

    @Override
    public String name() {
        return StrategyConstants.RECTANGLE_CONSOLIDATION;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.LONG;
    }
}
