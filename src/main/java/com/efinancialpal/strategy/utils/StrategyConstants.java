package com.efinancialpal.strategy.utils;

/**
 * Created by haiyanluo on 10/21/17.
 */
public class StrategyConstants {
    public static final String LONG = "long";
    public static final String SHORT = "short";

    public static final String SHOOTING_SHAR_SHORT = "ShootingStar_Short";
    public static final String PULLBACK123_SHORT = "Pullback123_Short";
    public static final String MAJOR_PRICE_DOWN_WITH_VOLUME_HIKE_SHORT = "MajorPriceDownWithVolumeHike_Short";
    public static final String EMBRACE_SHORT = "Embrace_Short";
    public static final String CUT_LOSSS = "CutLoss";

    public static final String MAJOR_PRICE_UP_WITH_VOLUME_HIKE_LONG = "MajorPriceUpWithVolumeHike_Long";
    public static final String AVERAGE_PRICE_LONG = "AveragePrice";
    public static final String BOTTOM_MACD_DIVERGENCE = "BottomMacdDivergence_Long";
    public static final String BREAKUP_50EMA_WITH_VOLUME_HIKE = "Breakup50EmaWithVolumeHike";
    public static final String BULL_TERRITORY = "BullTerritory";
    public static final String CROSS_ABOVE_50EMA = "CrossAbove50Ema";
    public static final String EMBRACE_LONG = "Embrace_Long";
    public static final String EXPANSION_BREAKOUT = "ExpansionBreakout";
    public static final String FA_VALUE_STOCK = "FaValueStock";
    public static final String FIFTY_FIVE_DAY_HIGH = "FiftyFiveDayHigh";
    public static final String FOUR_BY_FOUR_PULLBACK = "FourByFourPullback";
    public static final String HAMMMER = "Hammer_Long";
    public static final String HIGH_PLATEAU_CONSOLIDATION = "HighPlateauConsolidation";
    public static final String PIERCE_LONG = "Pierce";
    public static final String PRICE_INCREASE_WITH_VOLUME_HIGH = "PriceIncreaseWithVolumeHike_Long";
    public static final String PULLBACK123_LONG = "Pullback123_Long";
    public static final String PULLBACK_LONG = "Pullback_Long";
    public static final String RECENTLY_BECAME_BULL = "RecentlyBacameBull";
    public static final String RECENTLY_CROSSED_UP = "RecentlyCrossedUp";
    public static final String RECTANGLE_CONSOLIDATION_BREAKOUT_WITH_VOLUME_HIKE = "RectangleConsolidationBreakoutWithVolumeHike_Long";
    public static final String RECTANGLE_CONSOLIDATION = "RectangleConsolidation";
    public static final String STOCHASTIC_OVERSOLD = "StochasticOversold";
}
