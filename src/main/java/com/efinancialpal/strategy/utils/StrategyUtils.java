package com.efinancialpal.strategy.utils;

import com.efinancialpal.couchbase.StockRecordData;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.helpers.AverageDirectionalMovementUpIndicator;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import org.joda.time.DateTime;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by haiyanluo on 10/15/17.
 */
public class StrategyUtils {

    private static final int VOLUME_DAYS = 20;
    private static final int VOLUME_THRESHOLD = 200000;
    private static final int ADX_LENGTH = 14;
    private static final double ADX_THRESHOLD = 30;

    public static boolean isEmaSlopeUp(double currentClosePrice, List<HistoricalQuote> quotes, boolean longOrShort) {
        return isEmaSlopeUp(currentClosePrice, quotes, longOrShort, true /* strict */);
    }
    public static boolean isEmaSlopeUp(double currentClosePrice, List<HistoricalQuote> quotes, boolean longOrShort, boolean strictOrRelax) {
        if (quotes.size() < 200) {
            return false;
        }

        // check the latest price is above 50-day and 200-day average
        List<Tick> tickList = new ArrayList<>();
        for (int i = 0; i < quotes.size(); ++i) {
            HistoricalQuote quote = quotes.get(i);
            DateTime dateTime = new DateTime(quote.getDate().getTime());
            // Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());
            Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getClose().doubleValue(), quote.getVolume().doubleValue());
            tickList.add(tick);
        }

        TimeSeries timeSeries = new TimeSeries(tickList);
        // Collections.reverse(tickList);

        // TODO: ema values are a little different from tradingview values
        int daysToAverage50 = 50;
        EMAIndicator ema50 = new EMAIndicator(new ClosePriceIndicator(timeSeries), daysToAverage50);

        double current50DayAveragePrice = ema50.getValue(daysToAverage50 - 1).toDouble();

        int daysToAverage200 = 200;
        EMAIndicator ema200 = new EMAIndicator(new ClosePriceIndicator(timeSeries), daysToAverage200);
        double current200DayAveragePrice = ema200.getValue(daysToAverage200 - 1).toDouble();

        // if in the past 50 days, price range is less than 7%, skip, might be some bond index
        double low = quotes.get(0).getLow().doubleValue();
        double high = quotes.get(0).getHigh().doubleValue();
        for (int i = 0; i < 50; ++i) {
            if (quotes.get(i).getHigh().doubleValue() > high) {
                high = quotes.get(i).getHigh().doubleValue();
            }
            if (quotes.get(i).getLow().doubleValue() < low) {
                low = quotes.get(i).getLow().doubleValue();
            }
        }
        if ((high - low) / low < 0.07) {
            return false;
        }

        if (longOrShort) {
            // long
            // check if the 50 and 200 moving average is slope upward
            if (strictOrRelax) {
                if (current200DayAveragePrice < ema200.getValue(quotes.size() - 2).toDouble()) {
                    return false;
                }
            }
            if (current50DayAveragePrice < ema50.getValue(quotes.size() - 2).toDouble()) {
                return false;
            }
            if (strictOrRelax) {
                if (current50DayAveragePrice < current200DayAveragePrice) {
                    return false;
                }
            }
            if (currentClosePrice / current50DayAveragePrice < 0.97) {
                return false;
            }
            if (strictOrRelax) {
                if (currentClosePrice < current200DayAveragePrice) {
                    return false;
                }
            }
        } else {
            // short
            // check if the 50 and 200 moving average is slope download
            if (current200DayAveragePrice > ema200.getValue(quotes.size() - 2).toDouble()) {
                return false;
            }
            if (current50DayAveragePrice > ema50.getValue(quotes.size() - 2).toDouble()) {
                return false;
            }
            if (current50DayAveragePrice > current200DayAveragePrice) {
                return false;
            }
            if (currentClosePrice > current50DayAveragePrice || currentClosePrice > current200DayAveragePrice) {
                return false;
            }
        }

        return true;
    }

    public static boolean isVolumeLight(List<HistoricalQuote> quotes) {
        double avgVolume = 0;
        for (int i = 0; i < VOLUME_DAYS; ++i) {
            avgVolume += quotes.get(i).getVolume().doubleValue();
        }
        avgVolume /= VOLUME_DAYS;
        return avgVolume > VOLUME_THRESHOLD;
    }

    public static double getAverageVolume(List<HistoricalQuote> quotes, int consideredDays) {
        double avgVolume = 0;
        for (int i = 0; i < consideredDays; ++i) {
            avgVolume += quotes.get(i).getVolume().doubleValue();
        }
        avgVolume /= consideredDays;
        return avgVolume;
    }

    public static boolean isTrendingUp(List<HistoricalQuote> quotes) {
        List<Tick> tickList = new ArrayList<>();
        for (int i = 0; i < quotes.size(); ++i) {
            HistoricalQuote quote = quotes.get(i);
            DateTime dateTime = new DateTime(quote.getDate().getTime());
            // Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());
            Tick tick = new Tick(dateTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getClose().doubleValue(), quote.getVolume().doubleValue());
            tickList.add(tick);
        }

        Collections.reverse(tickList);
        AverageDirectionalMovementUpIndicator adx = new AverageDirectionalMovementUpIndicator(new TimeSeries(tickList), ADX_LENGTH);
       //  AverageDirectionalMovementDownIndicator adx1 = new AverageDirectionalMovementDownIndicator(new TimeSeries(tickList), ADX_LENGTH);
        double di = adx.getValue(0).toDouble();
        double di1 = adx.getValue(1).toDouble();
        double di2 = adx.getValue(2).toDouble();
        if (di < ADX_THRESHOLD) {
            return false;
        }
        return  true;
    }

    public static void main(String[] args) {
        String ticker = "APH";
        List<HistoricalQuote> quotes = StockRecordData.getInstance().get(ticker);

        isTrendingUp(quotes);
    }

}
