package com.efinancialpal.strategy.utils;

import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.oscillators.StochasticOscillatorKIndicator;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.simple.MaxPriceIndicator;
import eu.verdelhan.ta4j.indicators.simple.MinPriceIndicator;

/**
 * Created by haiyanluo on 2/4/17.
 */
public class MyStochasticOscillatorKIndicator extends StochasticOscillatorKIndicator {

    public MyStochasticOscillatorKIndicator(TimeSeries timeSeries, int timeFrame) {
        super(new ClosePriceIndicator(timeSeries), timeFrame, new MaxPriceIndicator(timeSeries), new MinPriceIndicator(
                timeSeries));
    }

    public Decimal getK(int index) {
        return calculate(index);
    }
}
