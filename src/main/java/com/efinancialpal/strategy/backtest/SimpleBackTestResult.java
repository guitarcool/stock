package com.efinancialpal.strategy.backtest;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by haiyanluo on 3/3/18.
 */
public class SimpleBackTestResult {
    private String symbol;
    private Calendar date;
    private static SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");

    public SimpleBackTestResult(String symbol, Calendar date) {
        this.symbol = symbol;
        this.date = date;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(symbol).append(", ").append(dt.format(date.getTime())).append("\n");
        return builder.toString();
    }
}
