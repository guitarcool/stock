package com.efinancialpal.strategy.backtest;

import java.util.Date;

/**
 * Created by haiyanluo on 11/26/16.
 */
public class BackTestResult {
    private String ticker;
    private Date entryTime;
    private Date exitTime;
    private double entryPrice;
    private double exitPrice;
    private double gain;

    public BackTestResult(String ticker, Date entryTime, Date exitTime, double entryPrice, double exitPrice, double gain) {
        this.ticker = ticker;
        this.entryTime = entryTime;
        this.exitTime = exitTime;
        this.entryPrice = entryPrice;
        this.exitPrice = exitPrice;
        this.gain = gain;
    }
    public BackTestResult() {

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ticker: ");
        builder.append(ticker);
        builder.append(" entry time: ");
        builder.append(entryTime);
        builder.append(" exit time: ");
        builder.append(exitTime);
        builder.append(" entryPrice: ");
        builder.append(entryPrice);
        builder.append(" exitPrice: ");
        builder.append(exitPrice);
        builder.append(" gain: ");
        builder.append(exitPrice - entryPrice);
        return builder.toString();
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getGain() {
        return gain;
    }

    public void setGain(double gain) {
        this.gain = gain;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public Date getExitTime() {
        return exitTime;
    }

    public void setExitTime(Date exitTime) {
        this.exitTime = exitTime;
    }

    public double getEntryPrice() {
        return entryPrice;
    }

    public void setEntryPrice(double entryPrice) {
        this.entryPrice = entryPrice;
    }

    public double getExitPrice() {
        return exitPrice;
    }

    public void setExitPrice(double exitPrice) {
        this.exitPrice = exitPrice;
    }
}
