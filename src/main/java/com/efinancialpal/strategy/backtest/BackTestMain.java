package com.efinancialpal.strategy.backtest;

import com.efinancialpal.couchbase.StockRecordData;
import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.buy.Pullback123LongStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haiyanluo on 11/26/16.
 */
public class BackTestMain {
    private static final Logger logger = LoggerFactory.getLogger(BackTestMain.class);

    private static final int TOTDAL_DAYS = 10 * 200; // 10 years
    private static final int AFTER_ONE_WEEK = 5;
    private static final double BUY_ABOVE = 0.1;
    private static final int MAX_HOLD_DAYS = 10;
    private static final double CUT_LOSS = 0.02;
    private static final double CUT_WIN = 0.06;

    public static boolean triggerEntry(int index, List<HistoricalQuote> quotes) {
        HistoricalQuote todayQuote = quotes.get(index);
        HistoricalQuote yesterdayQuote = quotes.get(index + 1);
        if (todayQuote.getHigh().doubleValue() < yesterdayQuote.getHigh().doubleValue() + BUY_ABOVE) {
            return false;
        }
        if (todayQuote.getLow().doubleValue() > yesterdayQuote.getHigh().doubleValue() + BUY_ABOVE) {
            // gap up above our buy limit order
            return false;
        }
        return true;
    }

    public static boolean triggerExit(int index, List<HistoricalQuote> quotes, BackTestResult backTestResult) {
        HistoricalQuote quote = quotes.get(index);
        double lowPrice = quote.getLow().doubleValue();
        double highPrice = quote.getHigh().doubleValue();
        double entryPrice = backTestResult.getEntryPrice();
        if (lowPrice < entryPrice && (entryPrice - lowPrice) / entryPrice > CUT_LOSS) {
            backTestResult.setExitPrice(entryPrice * (1 - CUT_LOSS));
            backTestResult.setExitTime(quote.getDate().getTime());
            return true;
        }
        if (highPrice > entryPrice && (highPrice - entryPrice) / entryPrice > CUT_WIN) {
            backTestResult.setExitPrice(entryPrice * (1 + CUT_WIN));
            backTestResult.setExitTime(quote.getDate().getTime());
            return true;
        }
        return false;
    }

    public static List<BackTestResult> backTest(String ticker, List<HistoricalQuote> quotes, Strategy strategy) {
        List<BackTestResult> results = new ArrayList<>();
        /*
        if (quotes.size() < TOTDAL_DAYS) {
            return results;
        } */
        // start from 10 days ago, in case there is some trigger too late to exit
        for (int i = MAX_HOLD_DAYS; i < quotes.size(); ++i) {
            List<HistoricalQuote> subQuotes = quotes.subList(i, quotes.size());
            EvaluationContext evaluationContext = new EvaluationContext();
            if (strategy.meet(ticker, subQuotes, evaluationContext)) {
                if (triggerEntry(i - 1, quotes)) {
                    BackTestResult backTestResult = new BackTestResult();
                    backTestResult.setTicker(ticker);
                    backTestResult.setEntryPrice(quotes.get(i).getHigh().doubleValue() + BUY_ABOVE);
                    backTestResult.setEntryTime(quotes.get(i - 1).getDate().getTime());

                    boolean triggeredExit = false;
                    int j = i - 2;
                    for (j = i - 2; j >= 0 && j > i - 2 - MAX_HOLD_DAYS; --j) {
                        if (triggerExit(j, quotes, backTestResult)) {
                            triggeredExit = true;
                            break;
                        }
                    }
                    if (!triggeredExit) {
                        backTestResult.setExitTime(quotes.get(j).getDate().getTime());
                        backTestResult.setExitPrice(quotes.get(j).getClose().doubleValue());
                    }
                    results.add(backTestResult);
                }
            }
        }
        return results;
    }

    public static void main(String[] args) {
        // TODO, right now can take only one symbol
        String[] symbols = new String[]{"AMAT", "AAPL", "ACLS", "BABA", "NVDA", "TSLA", "SQ", "AMZN", "FB", "NFLX"};
        List<BackTestResult> allResults = new ArrayList<>();
        for (String symbol : symbols) {
            List<HistoricalQuote> quotes = StockRecordData.getInstance().get(symbol);
            Strategy strategy = new Pullback123LongStrategy();
            List<BackTestResult> results = backTest(symbol, quotes, strategy);
            allResults.addAll(results);
        }
        for (BackTestResult result : allResults) {
            System.out.println(result);
        }
    }


}
