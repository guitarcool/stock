package com.efinancialpal.strategy;

import com.efinancialpal.strategy.backtest.BackTestResult;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haiyanluo on 10/22/16.
 */
public class AndCompositeStrategy extends Strategy {
    private List<Strategy> strategies = new ArrayList<>();
    private final static String NAME = "AndCompositeStrategy";

    public AndCompositeStrategy(List<Strategy> strategies) {
        this.strategies = strategies;
    }

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        for (Strategy strategy : strategies) {
            if (!strategy.meet(ticker, quotes, evaluationContext)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public List<BackTestResult> backTest(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        for (Strategy strategy : strategies) {
            // TODO: only considered one strategy for now
            return strategy.backTest(ticker, quotes, evaluationContext);
        }
        return new ArrayList<BackTestResult>();
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String longOrShort() {
        return null;
    }

    public void addStrategy(Strategy strategy) {
        strategies.add(strategy);
    }

    @Override
    public String toString() {
       return NAME;
    }

}
