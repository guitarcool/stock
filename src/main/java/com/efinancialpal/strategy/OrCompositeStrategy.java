package com.efinancialpal.strategy;

import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haiyanluo on 10/22/16.
 */
public class OrCompositeStrategy extends Strategy {
    private List<Strategy> strategies = new ArrayList<>();
    private static final String NAME = "OrCompositeStrategy";

    public OrCompositeStrategy(List<Strategy> strategies) {
        this.strategies = strategies;
    }

    @Override
    // Not exactly an Or operator, we wanted to evaluate a stock to multiple strategies if possbile, which per se is
    // a strong signal of action
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        boolean result = false;
        for (Strategy strategy : strategies) {
            if (strategy.meet(ticker, quotes, evaluationContext)) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String longOrShort() {
        return null;
    }

    @Override
    public String toString() {
        return NAME;
    }

    public void addStrategy(Strategy strategy) {
        strategies.add(strategy);
    }
}
