package com.efinancialpal.strategy.sell;

/**
 * Created by haiyanluo on 11/21/17.
 */

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.strategy.utils.StrategyUtils;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;
public class Down4PercentStrategy extends Strategy {
    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 20) {
            return  false;
        }
        if (ticker.equals("SIG")) {
            System.out.println("SIG");
        }
        double todayClose = quotes.get(0).getClose().doubleValue();
        double yesterdayClose = quotes.get(1).getClose().doubleValue();
        long todayVolume = quotes.get(0).getVolume();
        if (todayClose / yesterdayClose > 0.96) {
            return false;
        }
        double avg20Volume = StrategyUtils.getAverageVolume(quotes, 20);
        if (1.0 * todayVolume / avg20Volume < 1.5) {
            return false;
        }
        evaluationContext.populate(ticker, name(), longOrShort());
        return true;
    }

    @Override
    public String name() {
        return "down4Percent";
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.SHORT;
    }
}

