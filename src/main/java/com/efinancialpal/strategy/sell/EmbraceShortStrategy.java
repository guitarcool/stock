package com.efinancialpal.strategy.sell;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 10/17/17.
 * Embrace or Marubozu, short signal if there is top divergence
 */
public class EmbraceShortStrategy extends Strategy {
    private static final int CONSIDERED_DAY = 2;
    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < CONSIDERED_DAY + 20) {
            return false;
        }

        for (int i = 0; i < CONSIDERED_DAY; ++i) {
            HistoricalQuote quote0 = quotes.get(i);
            double open0 = quote0.getOpen().doubleValue();
            double close0 = quote0.getClose().doubleValue();
            double high0 = quote0.getHigh().doubleValue();
            double low0 = quote0.getLow().doubleValue();
            double volume0 = quote0.getVolume().doubleValue();
            if (open0 <= close0) {
                // should be down day
                continue;
            }

            HistoricalQuote quote1 = quotes.get(i + 1);
            double open1 = quote1.getOpen().doubleValue();
            double close1 = quote1.getClose().doubleValue();
            double high1 = quote1.getHigh().doubleValue();
            double low1 = quote1.getLow().doubleValue();
            if (open1 > close1) {
                // yesterday should not be down
                continue;
            }

            if (high0 < high1) {
                continue;
            }

            // should be below yesterday's low or close enough
            if (low0 > Math.min(open1, close1) + (Math.max(open1, close1) - Math.min(open1, close1)) / 3) {
                continue;
            }

            if ((high0 - low0) / open0 < 0.02) {
                // bar is not wide enough
                continue;
            }

            // 20 day high?
            boolean notHigest = false;
            for (int j = i + 1; j < i + 20; ++j) {
                if (high0 < quotes.get(j).getHigh().doubleValue()) {
                    notHigest = true;
                    break;
                }
            }
            if (notHigest) {
                continue;
            }

            // ideally, we want huge down with volume spike
            double avgVolume = 0;
            for (int j = i + 1; j < i + 6; ++j) {
                avgVolume += quotes.get(j).getVolume().doubleValue();
            }
            avgVolume /= 5;
            if (volume0 < 0.9 * avgVolume) {
                return false;
            }

            evaluationContext.populate(ticker, name(), longOrShort());

            return true;
        }
        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.EMBRACE_SHORT;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.SHORT;
    }

}


