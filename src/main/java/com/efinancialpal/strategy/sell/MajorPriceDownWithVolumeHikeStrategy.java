package com.efinancialpal.strategy.sell;

/**
 * Created by haiyanluo on 10/21/17.
 */

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.strategy.utils.StrategyUtils;
import com.efinancialpal.swagger.service.StockMain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 11/23/16.
 */
public class MajorPriceDownWithVolumeHikeStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(StockMain.class);

    private static double PRICE_DOWN_THRESHOLD = 0.96; // price up 4%
    private static double VOLUME_DOWN_THRESHOLD = 1.5; // volume up 150%
    private static final int CONSIDERED_DAYS = 2; // only consider two days
    private static final int LIGHT_VOLUME = 200000;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {

        if (quotes.size() <= 61) {
            return false;
        }
        double avgVolume = StrategyUtils.getAverageVolume(quotes, 20);
        if (avgVolume < LIGHT_VOLUME) {
            return false;
        }

        for (int i = 0; i < CONSIDERED_DAYS; ++i) {
            try {
                double todayOpen = quotes.get(i).getOpen().doubleValue();
                double todayClose = quotes.get(i).getClose().doubleValue();
                double todayHigh = quotes.get(i).getHigh().doubleValue();
                double todayVolume = quotes.get(i).getVolume().doubleValue();
                double yesterdayOpen = quotes.get(i + 1).getOpen().doubleValue();
                double yesterdayClose = quotes.get(i + 1).getClose().doubleValue();
                double yesterdayVolume = quotes.get(i + 1).getVolume().doubleValue();
                /*
                if (todayOpen <= yesterdayClose) {
                    continue;
                } */
                if (todayClose / yesterdayClose > PRICE_DOWN_THRESHOLD) {
                    continue;
                }
                if (todayVolume / avgVolume < VOLUME_DOWN_THRESHOLD) {
                    continue;
                }

                if (quotes.get(i).getOpen().doubleValue() < quotes.get(i).getClose().doubleValue()) {
                    continue;
                }

                // check if there is too much top shadow
                double bottomShadow = Math.min(quotes.get(i).getClose().doubleValue(), quotes.get(i).getOpen().doubleValue()) - quotes.get(i).getLow().doubleValue();
                double range = quotes.get(i).getHigh().doubleValue() - quotes.get(i).getLow().doubleValue();
                if (bottomShadow / range > 0.1) {
                    continue;
                }

                evaluationContext.populate(ticker, name(), longOrShort());
                return true;

            /*
            double topShadow = todayHigh - todayClose;
            if (topShadow > (todayClose - todayOpen)) {
                continue;
            }
            */
            } catch (Exception e) {
                logger.error("Error on processing ticker {}", ticker, e);
                return false;
            }

        }

        return false;
    }

    @Override
    public String name() {
        return StrategyConstants.MAJOR_PRICE_DOWN_WITH_VOLUME_HIKE_SHORT;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.SHORT;
    }
}

