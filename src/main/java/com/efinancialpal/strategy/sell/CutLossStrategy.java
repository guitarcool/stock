package com.efinancialpal.strategy.sell;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 10/22/16.
 */
public class CutLossStrategy extends Strategy {
    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        // TODO: we need the buy point
        return false;
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String longOrShort() {
        return null;
    }
}
