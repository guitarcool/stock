package com.efinancialpal.strategy.sell;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 11/12/16.
 */
public class ShootingStarStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(ShootingStarStrategy.class);
    private static double threshold = 0.2;
    private static int CONSIDERED_DAYS = 2;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < 20) {
            return false;
        }
        for (int i = 0; i < CONSIDERED_DAYS; ++i) {
            HistoricalQuote quote = quotes.get(i);
            double open = quote.getOpen().doubleValue();
            double close = quote.getClose().doubleValue();
            double high = quote.getHigh().doubleValue();
            double low = quote.getLow().doubleValue();
            double volume = quote.getVolume().doubleValue();

            double range = high - low;
            if ((high - low) / low < 0.03) {
                continue;
            }

            double bodyLength = Math.abs(open - close);
            if (bodyLength / range > threshold) {
                // the body is too large;
                continue;
            }

            double closePosition = Math.abs(low - close);
            if (closePosition / range > threshold) {
                // close price not in the top
                continue;
            }

            double openPosition = Math.abs(low - open);
            if (openPosition / range > threshold) {
                // open position is not in the top
                continue;
            }

            int j = i + 1;
            for (; j < i + 20; ++j) {
                // 20 day high
                if (high < quotes.get(j).getHigh().doubleValue()) {
                    break;
                }
            }
            if (j < i + 20) {
                continue;
            }
            if (volume < 0.8 * StrategyUtils.getAverageVolume(quotes.subList(i + 1, quotes.size()), 20)) {
                continue;
            }
            evaluationContext.populate(ticker, name(), longOrShort());
            return true;
        }
        return false;

    }

    @Override
    public String name() {
        return "ShootingStar_Short";
    }

    @Override
    public String longOrShort() {
        return "short";
    }

}
