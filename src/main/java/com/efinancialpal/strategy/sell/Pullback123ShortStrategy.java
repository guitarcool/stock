package com.efinancialpal.strategy.sell;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.strategy.utils.StrategyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.List;

/**
 * Created by haiyanluo on 10/17/17.
 */
public class Pullback123ShortStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(Pullback123ShortStrategy.class);

    private static final int TOTAL_DAYS_TO_CONSIDER = 40;
    private static final double LOW_PRICE_STOCK = 20;
    private static final int MIN_CONSOLIDATION_DAY = 2;
    private static final int MAX_CONSOLIDATION_DAY = 6; // after 7 days, it tends to lose the momentum
    private static final int ADX_LENGTH = 14;
    private static final double ADX_THRESHOLD = 30;

    @Override
    public boolean meet(String ticker, List<HistoricalQuote> quotes, EvaluationContext evaluationContext) {
        if (quotes.size() < TOTAL_DAYS_TO_CONSIDER + 1) {
            // should have more than one year data
            return false;
        }
        try {
            double lowestLowPrice = quotes.get(0).getLow().doubleValue();
            double lowestConsolidationPrice = quotes.get(0).getLow().doubleValue();
            double currentClosePrice = quotes.get(0).getClose().doubleValue();
            /*
            if (currentClosePrice < LOW_PRICE_STOCK) {
                // low stock price is filtered out
                return false;
            }
            */

            int lowestIndex = 0;
            for (int i = 1; i < TOTAL_DAYS_TO_CONSIDER; ++i) {
                if (quotes.get(i).getLow().doubleValue() < lowestLowPrice) {
                    lowestIndex = i;
                    lowestLowPrice = quotes.get(i).getLow().doubleValue();
                }
            }
            if (lowestIndex < MIN_CONSOLIDATION_DAY || lowestIndex > MAX_CONSOLIDATION_DAY) {
                // only consolidated for 3-7 days
                return false;
            }

            // if today's price range is too large, filter it out, since it's hard to set stop loss.
            // However, if it's a hammer, we want to keep it, so we only look at close price
            // TODO: don't need this for short?
            /*
            double todayClose = quotes.get(0).getClose().doubleValue();
            double yesterdayClose = quotes.get(1).getClose().doubleValue();
            if (todayClose / yesterdayClose < 0.97) {
                return false;
            }
            */

            for (int i = 1; i < lowestIndex; ++i) {
                double currentLow = quotes.get(i - 1).getLow().doubleValue();
                double previousdayLow = quotes.get(i).getLow().doubleValue();
                if (currentLow < previousdayLow) {
                    return false;
                }
                /*
                if (quotes.get(i).getLow().doubleValue() < lowestConsolidationPrice) {
                    lowestConsolidationPrice = quotes.get(i).getLow().doubleValue() ;
                } */
            }
            /* TODO: do I need this for short
            if (lowestConsolidationPrice / highestHihgPrice < 0.5) {
                // the retracement is too deep
                return false;
            }
            */

            /* TODO: volume ?
            double downVolume = 0;
            for (int i = 0; i <= lowestIndex; ++i) {
                downVolume += quotes.get(i).getVolume();
            }
            downVolume = downVolume * 1.0 / (currentClosePrice + 1);

            if (downVolume * currentClosePrice < 750000) {
                // average daily trading volume is less than 750K
                return false;
            }
            */

            if (!StrategyUtils.isEmaSlopeUp(currentClosePrice, quotes, false /* short */)) {
                return false;
            }

            // TODO: check if ADX +DI is greater than 30
            /*
            AverageDirectionalMovementUpIndicator adx = new AverageDirectionalMovementUpIndicator(new TimeSeries(tickList), ADX_LENGTH);
            AverageDirectionalMovementDownIndicator adx1 = new AverageDirectionalMovementDownIndicator(new TimeSeries(tickList), ADX_LENGTH);
            double di = adx.getValue(0).toDouble();
            if (di < ADX_THRESHOLD) {
                return false;
            }
            */
            evaluationContext.populate(ticker, name(), longOrShort());

        } catch (Exception e) {
            logger.error("error in processing {}, ", ticker, e);
            return false;
        }
        logger.info("Found one stock: {}", ticker);

        return true;
    }

    @Override
    public String name() {
        return StrategyConstants.PULLBACK123_SHORT;
    }

    @Override
    public String longOrShort() {
        return StrategyConstants.SHORT;
    }
}
