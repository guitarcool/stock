package com.efinancialpal.strategy;

import com.efinancialpal.strategy.buy.*;
import com.efinancialpal.strategy.sell.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by haiyanluo on 10/22/16.
 */
public class StrategyCreator {
    public static Strategy createStrategies() {
        List<Strategy> strategyList = Arrays.asList(new Strategy[] {
                // new BullTerritoryStrategy(),
                // new BackTestMain(),
                // new EmbraceLongStrategy(),
                // new PierceStrategy(),
                // new FAValueStockStrategy(),
                // new FiftyFiveDayHighStrategy(),
                // new RectangleConsolidationStrategy(),
                // new CrossAbove50EmaStrategy(),
                // new StochasticOverSoldStrategy(),

                // new BottomMacdDivergenceStrategy(),

                // new Breakup50EMAWithVolumeHikeStrategy(),

                // new HighPlateauConsolidationStrategy(),

                new Down4PercentStrategy(),
                new EmbraceLongStrategy(),
                new ShootingStarStrategy(),
                new EmbraceShortStrategy(),
                new HammerStrategy(),
                new Pullback123LongStrategy(),
                new PullbackStrategy(),
                new ExpansionBreakoutStrategy(),
                new MajorPriceUpWithVolumeHikeStrategy(),
                new MajorPriceDownWithVolumeHikeStrategy(),
                new Pullback123ShortStrategy(),
                new Up4PercentStrategy(),
        });


        Strategy result = new OrCompositeStrategy(strategyList);
        return result;
    }


    public static Strategy createValueInvestingStrategy() {
        return null;
    }

    public static Strategy createGrowthInvestingStrategy() {
        return null;
    }

    public static Strategy createBuyDipInvestingStrategy() {
        return null;
    }

    public static Strategy createMomentumInvestingStrategy() {
        return null;
    }

    public static Strategy createYearEndSpeculationStrategy() {
        return null;
    }

    public static Strategy createPresidentialSpeculationStrategy() {
        return null;
    }

    public static Strategy createTopMutualFundFollowingStrategy() {
        return null;
    }




}
