package com.efinancialpal.strategy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haiyanluo on 10/21/17.
 */
public class EvaluationContext {
    public String ticker;
    public List<String> strategies = new ArrayList<>();
    public String longOrShort;

    public EvaluationContext(String ticker, String strategy, String longOrShort) {
        this.ticker = ticker;
        this.strategies.add(strategy);
        this.longOrShort = longOrShort;
    }
    public EvaluationContext() {
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public List<String> getStrategies() {
        return this.strategies;
    }

    public void addStrategy(String strategy) {
        this.strategies.add(strategy);
    }

    public String getLongOrShort() {
        return longOrShort;
    }

    public void setLongOrShort(String longOrShort) {
        this.longOrShort = longOrShort;
    }

    public void populate(String ticker, String name, String longOrShort) {
        this.ticker = ticker;
        this.strategies.add(name);
        this.longOrShort = longOrShort;
    }
}
