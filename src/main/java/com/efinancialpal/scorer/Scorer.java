package com.efinancialpal.scorer;

/**
 * Created by haiyanluo on 12/5/16.
 */
public interface Scorer {
    double score();
    double weight();
}
