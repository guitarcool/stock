package com.efinancialpal.scorer;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * Created by haiyanluo on 12/5/16.
 *
 * Implements Stan Weinstein's Secrets for Profiting in Bull and Bear Markets
 */
public class IndexCalendarScorer implements Scorer {
    private Set<Integer> ELECTION_YEAR = ImmutableSet.of(1996, 2000, 2004, 2008, 2012, 2016, 2020);


    @Override
    public double score() {
       // LocalDateTime.now().
        return 0;
    }

    @Override
    public double weight() {
        return 0;
    }

    public double caculateElectionFactor() {
        return 0;
    }

    public double calculateMonthFactor() {
        return 0;
    }

    public double calculateWeekdayFactor() {
        return 0;
    }

    public double calculateHolidayFactor() {
        return 0;
    }
}
