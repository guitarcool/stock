package com.efinancialpal.scorer;

/**
 * Created by haiyanluo on 12/5/16.
 */
public class CheckResult {
    private String ticker;
    private boolean aboveEMA50Weekly;
    private double score;
    private double marketCap;
    private double pe;
    private double pes;
    private double freeCashFlow;
    private boolean isTaxCutSell;


    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public boolean isAboveEMA50Weekly() {
        return aboveEMA50Weekly;
    }

    public void setAboveEMA50Weekly(boolean aboveEMA50Weekly) {
        this.aboveEMA50Weekly = aboveEMA50Weekly;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
