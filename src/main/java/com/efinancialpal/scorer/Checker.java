package com.efinancialpal.scorer;

/**
 * Created by haiyanluo on 12/5/16.
 */
public class Checker {
    public CheckResult check(String ticker) {
        CheckResult result = new CheckResult();

        result.setTicker(ticker);
        result.setAboveEMA50Weekly(isAboveEMA50Weekly(ticker));


        return result;
    }

    public boolean isAboveEMA50Weekly(String ticker) {
        return false;
    }

    public boolean isTaxCutSell(String ticker) {
        // time is end of november and december
        // price has been dropping for most of the time
        // better: low price stock
       return false;
    }
}
