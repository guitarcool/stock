package com.efinancialpal.service;

import com.efinancialpal.swagger.model.DailyTransactionData;
import com.efinancialpal.swagger.rest.Constants;
import com.efinancialpal.utils.db.ActiveJDBCUtil;
import org.javalite.activejdbc.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by haiyan on 7/24/2016.
 */
public class ModelTest {
    private static Logger logger = LoggerFactory.getLogger(ModuleManager.class);

    public static void main(String[] args) {
        DB db = null;
        db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
        try {
            DailyTransactionData daily = new DailyTransactionData();
            daily.setTicker("YHOO");
            daily.insert();
        } catch (Throwable t) {
            logger.error("Exception happened while inserting daily transaction data", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

}

