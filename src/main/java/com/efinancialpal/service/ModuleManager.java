/*******************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *******************************************************************************/
package com.efinancialpal.service;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * All modules will be registered with module manager. 
 * 
 * Module manager will enforce the initialization order of modules based on its dependency. 
 * 
 */
public class ModuleManager {
	private static Logger logger = LoggerFactory.getLogger(ModuleManager.class);

	// flag to prevent re-initialization
	private static volatile boolean isInitialized;
	
	//flag to indicate module activation
	private static volatile boolean isActive = false;

	// List to record the initialization order
	private static List<Module> initOrder = new ArrayList<Module>();

	// Key is module class name, value is module instance
	private static Map<String, Module> modulesMap = new HashMap<String, Module>();
	
	private static final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

	private static final AtomicBoolean shutDownFlag = new AtomicBoolean(false);
	
	public static enum Status {
		Idle,
		Initializing,
		Running,
		Destroying
	}
	
	private static Status currentStatus = Status.Idle;
	
	/**
	 * Find out running status.
	 */
	public static Status getStatus() {
		return currentStatus;
	}
	
	/**
	 * Get all module names.
	 * 
	 * @return
	 */
	public static String[] getModuleNames() {
		if (initOrder.isEmpty()) {
			return null;
		}
		
		String[] moduleNames = new String[initOrder.size()];
		for (int i = 0; i < moduleNames.length; i++) {
			moduleNames[i] = initOrder.get(i).getClass().getName();
		}
		
		return moduleNames;
	}
	
	/**
	 * Determine if all modules have been initialized.
	 * 
	 * @return
	 */
	public static boolean isInitialized() {
		return isInitialized;
	}

	static void setDestroy() {
		isInitialized = false;
	}
	
//	public static void setInitialized(boolean initialized) {
//		isInitialized = initialized;
//	}
	
	/**
	 * @return the isActive
	 */
	public static boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public static void setActive(boolean isActive) {
		ModuleManager.isActive = isActive;
	}

	/**
	 * Register a management bean.
	 * 
	 * @param mBean
	 * @param serviceName
	 * @throws NotCompliantMBeanException
	 * @throws MBeanRegistrationException
	 * @throws InstanceAlreadyExistsException
	 * @throws InstanceNotFoundException 
	 */
	public static void registerMBean(Object mBean, ObjectName serviceName) 
			throws NotCompliantMBeanException, MBeanRegistrationException, InstanceAlreadyExistsException, InstanceNotFoundException {
		mbs.registerMBean(mBean, serviceName);

	}
	
	/**
	 * Add a module by its class name.
	 * It will add all dependents recursively.
	 * 
	 * @param className
	 * @return
	 */
	public static Module addModule(String className) {
		Module module = null;
		try	{
			return addModuleInternal(className);
		}
		catch (ClassNotFoundException e) {
			logger.error("Failed to initialize Module {}.", className, e);
		}
		catch (InstantiationException e) {
			logger.error("Failed to initialize Module {}.", className, e);
		}
		catch (IllegalAccessException e) {
			logger.error("Failed to initialize Module {}.", className, e);
		}

		return module;
	}

	/**
	 * Find a module by its class name.
	 * 
	 * @param className
	 * @return
	 */
	public static Module getModule(String className) {
		return modulesMap.get(className);
	}
	
	/**
	 * Initialize all modules according to initialization order.
	 * 
	 * @return True if succeed
	 */
	public static boolean init() {
		if (initOrder.isEmpty()) {
			logger.warn("No module has been added to module manager");
		}
		
		currentStatus = Status.Initializing;

		// initialize all the modules
		for (Module module : initOrder) {
			if (!module.isInitialized()) {
				logger.info("Initializing {} module", module.getClass().getName());

				try	{
					isInitialized = module.init();
				} 
				catch (Throwable ex) {
					logger.error("Exception thrown by module {} during initialization", 
							module.getClass().getName(), 
							ex);

					isInitialized = false;
				}

				// stop initialization when there's an error
				if (!isInitialized) {
					break;
				}
				
				logger.info("Finished initializing {} module", module.getClass().getName());
			}
		}

		if (initOrder.isEmpty()) {
			isInitialized = true;
		}
		
		logger.info("Module manager finished module initialization");

		currentStatus = Status.Running;
		
		return isInitialized;
	}

	/**
	 * Destroying all modules according to the reverse of initialization order.
	 * 
	 * @return
	 */
	public static void destroy()	{
		// destroy only once in JVM
		if (!shutDownFlag.compareAndSet(false, true)) {
			logger.info("Modules are being shut down ...");
			return;
		}
		
		currentStatus = Status.Destroying;

		// destroy modules
		for (int i = initOrder.size() - 1; i >= 0; i--) {
			Module module = initOrder.get(i);
			if (module.isInitialized())	{
				logger.info("Destroying {} module", module.getClass().getName());

				module.destroy();
			}
			else {
				logger.error("Module {} is already destroyed.", module.getClass().getName());
			}
		}
		initOrder.clear();
		modulesMap.clear();
	}

	/**
	 * Initialize all the modules and wait for the modules to be destroyed.
	 */
	public static void initWaitForComplete () {
		if(ModuleManager.init()) {

			while (ModuleManager.isInitialized()) {
				// wait until being signaled to destroy
				try	{
					Thread.sleep(1000L);
				}
				catch (InterruptedException e) {}
			}
		}

		ModuleManager.destroy();
	}

	/**
	 * Add module and its dependents recursively.
	 * 
	 * @param className
	 * @return
	 * @throws ClassNotFoundException
	 */
	private static Module addModuleInternal (String className) 
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		// check if module has been added before
		Module existingModule = modulesMap.get(className);
		if ( existingModule != null ) {
			return existingModule;
		}

		// try to get an instance of the current module
		Module currentModule = null;
		Class<?> moduleClass = Class.forName(className);
		Object obj=  moduleClass.newInstance();
		if (obj instanceof Module) {
			currentModule = (Module) obj;
		}

		// add current module and its dependents recursively
		if (currentModule != null) {
			// recording current module has been added
			modulesMap.put(className, currentModule);

			// add dependents recursively
			List<String> dependents = currentModule.getDependents();
			if (dependents != null && !dependents.isEmpty()) {
				for (String dependent: dependents) {
					addModuleInternal(dependent);
				}
			}

			// add current module to the end of initialization order list
			initOrder.add(currentModule);
		}

		return currentModule;
	}
}
