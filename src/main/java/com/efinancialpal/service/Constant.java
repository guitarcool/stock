/*******************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *******************************************************************************/
package com.efinancialpal.service;

/**
 * Defines constants for configuration.
 * 
 */
public class Constant {
	private Constant() { }
	
	static final String JMX_PORT = "Server.JMXPort";
	public static final String SERVER_TYPE = "Server.Type";
	public static final String SERVER_PREFIX = "Server.";
	
	static final String JAR_TEMP_PATH = "/tmp";
	static final String GRACE_SHUTDOWN_MAX_WAIT_MS = "Server.GraceShutdown.MaxWaitMs";
}
