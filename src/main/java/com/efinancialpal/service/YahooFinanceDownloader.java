package com.efinancialpal.service;

import com.efinancialpal.swagger.model.DailyTransactionData;
import com.efinancialpal.swagger.rest.Constants;
import com.efinancialpal.utils.db.ActiveJDBCUtil;
import org.javalite.activejdbc.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import yahoofinance.quotes.stock.StockStats;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by haiyan on 7/23/2016.
 */
public class YahooFinanceDownloader {
    private static Logger logger = LoggerFactory.getLogger(ModuleManager.class);
    private static YahooFinanceDownloader instance = new YahooFinanceDownloader();

    private YahooFinanceDownloader() {
    }

    public static YahooFinanceDownloader getInstance() {
        return instance;
    }

    public Response store(List<HistoricalQuote> historicQuotes, String ticker) {
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);

            for (HistoricalQuote quote : historicQuotes) {
                Date date = quote.getDate().getTime();
                BigDecimal high = quote.getHigh();
                BigDecimal low = quote.getLow();
                BigDecimal open = quote.getOpen();
                BigDecimal close = quote.getClose();
                Long volume = quote.getVolume();

                logger.info("inserting daily transaction data for {}", ticker);
                DailyTransactionData daily = new DailyTransactionData();
                daily.setTicker(ticker);
                daily.setDate(date);
                daily.setHigh(high);
                daily.setLow(low);
                daily.setOpen(open);
                daily.setClose(close);
                daily.setVolume(volume);
                daily.insert();
                logger.info("Successfully inserted a daily transaction data for {}", date);
            }
        } catch (Throwable t) {
            logger.error("Exception happened while inserting daily transaction data", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return Response.status(Response.Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
    }


    public Response downloadQuotes(String ticker) throws Exception {
        logger.info("Injecting historic data for ticker {}", ticker);
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();

        populateFromDate(from, ticker);

        Stock stock = YahooFinance.get(ticker, from, to, Interval.DAILY);
        List<HistoricalQuote> historicalQuotes = stock.getHistory();
        return store(historicalQuotes, ticker);
    }

    public List<HistoricalQuote> downloadQuotes(String ticker, Interval weekOrDay) throws Exception {
        Calendar to = Calendar.getInstance();
        Calendar from = Calendar.getInstance();
        from.setTime(new Date());
        from.add(Calendar.YEAR, -10); // download 10 years of history data
        List<HistoricalQuote> result = new ArrayList<>();
        try {
            Stock stock = YahooFinance.get(ticker, from, to, weekOrDay);
            List<HistoricalQuote> historicalQuotes = stock.getHistory();
            for (int i = historicalQuotes.size() - 1; i >= 0; --i) {
                result.add(historicalQuotes.get(i));
            }
        }
        catch (Exception e) {
            logger.error("Exception getting {}", ticker, e);
        }
        return result;
    }

    public Long downloadAndStoreStockStats(String ticker) throws Exception {
        logger.info("Injecting stock stats for ticker {}", ticker);
        try {
            StockStats stockStats = YahooFinance.get(ticker).getStats();
            return storeStockStats(ticker, stockStats);
        }
        catch (Exception e) {
            logger.error("Failed to get stats for {}, ignored", ticker, e) ;
            return 0L;
        }
    }


    public Long storeStockStats(String ticker, StockStats stockStats) throws Exception {
        com.efinancialpal.swagger.model.StockStats stats = new com.efinancialpal.swagger.model.StockStats();
        stats.setTicker(stockStats.getSymbol());
        stats.setMarketCap(stockStats.getMarketCap());
        stats.setSharesFloat(stockStats.getSharesFloat());
        stats.setSharesOutstanding(stockStats.getSharesOutstanding());
        stats.setSharesOwned(stockStats.getSharesOwned());
        stats.setEps(stockStats.getEps());
        stats.setPe(stockStats.getPe());
        stats.setPeg(stockStats.getPeg());
        stats.setEpsEstimateCurrentYear(stockStats.getEpsEstimateCurrentYear());
        stats.setEpsEstimateNextQuarter(stockStats.getEpsEstimateNextQuarter());
        stats.setEpsEstimateNextYear(stockStats.getEpsEstimateNextYear());
        stats.setPriceBook(stockStats.getPriceBook());
        stats.setPriceSales(stockStats.getPriceSales());
        stats.setBookValuePerShare(stockStats.getBookValuePerShare());
        stats.setRevenue(stockStats.getRevenue());
        stats.setEbitda(stockStats.getEBITDA());
        stats.setOneYearTargetPrice(stockStats.getOneYearTargetPrice());
        stats.setShortRatio(stockStats.getShortRatio());

        stats.insert();

        return stats.getId();
    }


    // calculate how many days of data we need to download
    public void populateFromDate(Calendar from, String ticker) {
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            // get the latest record of the ticker symbol
            String query = "select * from daily_transaction_data where ticker = '" + ticker
                    + "' order by version_updated_at desc limit 1 ";
            logger.info("query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result == null || result.isEmpty()) {
                from.add(Calendar.YEAR, -10); // default 10 years
            } else {
                Date date = (Date) (result.get(0).get("version_updated_at"));
                logger.info("latest record for {} is at {}", ticker, date);
                from.setTime(date);
                from.add(Calendar.DATE, 1);
            }
            logger.info("From date was setting to {} ", from);
        } catch (Throwable t) {
            logger.error("Exception happened while populating from date: ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        String ticker = "BABA";
        //YahooFinanceDownloader.getInstance().downloadAndStoreStockStats(ticker);
        YahooFinanceDownloader.getInstance().downloadQuotes("^DJUSBMT", Interval.DAILY);
        // StockStats stats = YahooFinance.get(ticker).getStats();
        // System.out.println(stats);
    }


}