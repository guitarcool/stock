/*******************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *******************************************************************************/
package com.efinancialpal.service;

import java.util.List;

/**
 * Module is the building block of a service. A service will consists of multiple modules.
 * 
 * Modules will be initialized according to dependency.
 * 
 */
public abstract class Module {
	
    protected volatile boolean initialized = false;
    
	/**
	 * Get the state of the module.
	 * 
	 * @return True if the module has been initialized.
	 */
	public final boolean isInitialized() {
		return initialized;
	}
	
	/**
	 * The modules the current module depends on.
	 * 
	 * @return List of class names
	 */
	public List<String> getDependents() {
		return null;
	}
	
	/**
	 * Initialize the module.
	 * 
	 * @return True if succeeds
	 */
	public abstract boolean init();

	/**
	 * Release all the resource this module depends on.
	 */
	public void destroy() {
		initialized = false;
	}
}
