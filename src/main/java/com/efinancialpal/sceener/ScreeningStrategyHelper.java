package com.efinancialpal.sceener;

import com.efinancialpal.swagger.rest.Constants;
import com.efinancialpal.ta.TechnicalAnalyzer;
import com.efinancialpal.utils.db.ActiveJDBCUtil;
import org.javalite.activejdbc.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by haiyan on 7/31/2016.
 */
public class ScreeningStrategyHelper {
    private static final Logger logger = LoggerFactory.getLogger(ScreeningStrategyHelper.class);
    private static BigDecimal PEG_THRESHOLD = new BigDecimal(2);

    // TODO: Refactor the lowPeg() function in TechnicalAnalyzer so that it can be used here
    public static boolean checkPeg(String ticker) {
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            // get the latest record of the ticker symbol
            String query = "select * from stock_stats where ticker = '" + ticker + "'";
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result == null || result.isEmpty()) {
                return false;
            } else {
                BigDecimal peg = (BigDecimal) result.get(0).get("peg");
                logger.info("{} meets peg criteria", ticker);
                return peg.compareTo(PEG_THRESHOLD) < 0;
            }
        } catch (Throwable t) {
            logger.error("Exception happened while checking peg: ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public static boolean breakthrough50Weeks(String ticker) {
        return TechnicalAnalyzer.getInstance().newHigh50Weeks(ticker);
    }

}
