package com.efinancialpal.sceener;

/**
 * Created by haiyan on 7/31/2016.
 */
public interface ScreeningStrategy {
    public boolean meetCriteria(String ticker);
}
