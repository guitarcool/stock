package com.efinancialpal.sceener;

import com.efinancialpal.swagger.rest.config.ConfigurationManager;
import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haiyan on 7/31/2016.
 */
public class StockScreener {
    private static final Logger logger = LoggerFactory.getLogger(StockScreener.class);
    private static final Configuration conf   = ConfigurationManager.getConfiguration();
    private static StockScreener instance = new StockScreener();

    public static StockScreener getInstance() {
        return instance;
    }

    /** returns a list of stock tickers and meet the screen criteria */
    public List<String> screen(ScreeningStrategy strategy) {
        List<String> result = new ArrayList<>();
        String[] tickers = conf.getStringArray("tickers");
        for (String ticker : tickers) {
            if (strategy.meetCriteria(ticker)) {
                result.add(ticker);
            }
        }
        return result;
    }
}
