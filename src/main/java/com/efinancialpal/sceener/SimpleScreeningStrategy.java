package com.efinancialpal.sceener;

/**
 * Created by haiyan on 7/31/2016.
 */
public class SimpleScreeningStrategy implements ScreeningStrategy{
    @Override
    public boolean meetCriteria(String ticker) {
        return ScreeningStrategyHelper.checkPeg(ticker) && ScreeningStrategyHelper.breakthrough50Weeks(ticker);
    }
}
