package com.efinancialpal.fa;

import com.efinancialpal.swagger.rest.Constants;
import com.efinancialpal.utils.db.ActiveJDBCUtil;
import org.javalite.activejdbc.DB;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by haiyan on 7/24/2016.
 */
public class FinancialAnalyzer {
    private static final Logger logger = LoggerFactory.getLogger(FinancialAnalyzer.class);

    private static FinancialAnalyzer instance = new FinancialAnalyzer();

    public static FinancialAnalyzer getInstance() {
        return instance;
    }

    public boolean lowPeg(String ticker, BigDecimal threshold) {
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.STOCK);
            // get the latest record of the ticker symbol
            String query = "select * from stock_stats where ticker = '" + ticker + "' and peg < " + threshold + " order by version_updated_at desc limit 1";
            logger.info("query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result == null || result.isEmpty()) {
                logger.info("ticker symbol {} doesn't have stock stats in our system", ticker);
                return false;
            }
            return true;
        } catch (Throwable t) {
            logger.error("Exception happened while querying lower peg: ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
}
