package com.efinancialpal.couchbase;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;

/**
 * Created by haiyanluo on 10/29/17.
 */
public class CouchbaseKeyValueCache {
    private static CouchbaseKeyValueCache instance = new CouchbaseKeyValueCache();

    private CouchbaseKeyValueCache() {

    }

    public static CouchbaseKeyValueCache getInstance() {
        return instance;
    }

    public void set(String key, String value) {
        JsonObject jsonObject = JsonObject.create().put(key, value);
        CouchbaseClient.getInstance().set(key, jsonObject);
    }

    public String get(String key) {
        JsonDocument jsonDocument = CouchbaseClient.getInstance().get(key);
        if (jsonDocument == null || jsonDocument.content() == null || jsonDocument.content().get(key) == null) {
            return null;
        }
        return (String) jsonDocument.content().get(key);
    }
}
