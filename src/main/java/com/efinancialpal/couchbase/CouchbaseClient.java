package com.efinancialpal.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;

/**
 * Created by haiyanluo on 10/7/17.
 */
public class CouchbaseClient {
    private static final String COUCHBASE_SERVER = "127.0.0.1";
    private static CouchbaseClient instance = new CouchbaseClient();
    private static CouchbaseCluster cluster;
    private static Bucket bucket;

    private CouchbaseClient() {
        // Create a cluster reference
        cluster = CouchbaseCluster.create();
        cluster.authenticate("Administrator", "Cisco123");

        // Connect to the bucket and open it
        bucket = cluster.openBucket("mybucket");
    }

    public JsonDocument set(String docId, JsonObject jsonObject) {
        return bucket.upsert(JsonDocument.create(docId, jsonObject));
    }

    public JsonDocument get(String docId) {
        return bucket.get(docId);
    }

    public static CouchbaseClient getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        String docId = "helloword";
        JsonObject content = JsonObject.create().put("hello", "world");
        CouchbaseClient.getInstance().set(docId, content);
        System.out.println(CouchbaseClient.getInstance().get(docId).content());
    }
}
