package com.efinancialpal.couchbase;

import downloaders.barchart.BarchartDownloader;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.efinancialpal.service.YahooFinanceDownloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by haiyanluo on 10/7/17.
 */
public class StockRecordData {
    private static final Logger logger = LoggerFactory.getLogger(StockRecordData.class);

    private final static long ONE_DAY_IN_MS = (24 - 10) * 60 * 60 * 1000; // sometimes I run this less than 24 hours away
    private static StockRecordData instance = new StockRecordData();

    private StockRecordData()  {}

    public static StockRecordData getInstance() {
        return instance;
    }

    /* external interface to user */
    public List<HistoricalQuote> get(String symbol) {
        if (todayHasUpdated(symbol)) {
            List<HistoricalQuote> quotes = getFromCouchbase(symbol);
            if (!quotes.isEmpty()) {
                return quotes;
            }
        }
        try {
            // List<HistoricalQuote> quotes = YahooFinanceDownloader.getInstance().downloadQuotes(symbol, Interval.DAILY);
            // List<HistoricalQuote> quotes = QuandlDownloader.getInstance().download(symbol);
            // List<HistoricalQuote> quotes = GoogleDownloader.getInstance().download(symbol);
            List<HistoricalQuote> quotes = BarchartDownloader.getInstance().download(symbol);
            // TODO: this should be changed to async to improve performance
            set(symbol, quotes);
            return quotes;
        } catch (Exception e) {
            logger.error("failed to download transaction data for {} ", symbol, e);
            return new ArrayList<>();
        }
    }

    /* internal function to use */
    public List<HistoricalQuote> getFromCouchbase(String symbol) {
        List<HistoricalQuote> quotes = new ArrayList<>();
        JsonDocument jsonDocument = CouchbaseClient.getInstance().get(symbol);
        try {
            if (jsonDocument != null) {
                JsonArray jsonArray = (JsonArray) jsonDocument.content().get("daily");
                for (int i = 0; i < jsonArray.size(); ++i) {
                    HistoricalQuote quote = new HistoricalQuote();
                    JsonObject jsonObject = (JsonObject) jsonArray.get(i);
                    quote.setSymbol(symbol);
                    quote.setOpen(new BigDecimal((Double) jsonObject.get("open")));
                    quote.setLow(new BigDecimal((Double) jsonObject.get("low")));
                    quote.setHigh(new BigDecimal((Double) jsonObject.get("high")));
                    quote.setClose(new BigDecimal((Double) jsonObject.get("close")));
                    if (jsonObject.get("adjClose") != null) {
                        quote.setAdjClose(new BigDecimal((Double) jsonObject.get("adjClose")));
                    }
                    quote.setVolume((long) (Integer) jsonObject.get("volume"));
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis((long) jsonObject.get("date"));
                    quote.setDate(calendar);
                    quotes.add(quote);
                }
            }
        } catch (Exception e) {
            logger.error("failed to get {} with result {} ", symbol, jsonDocument, e);
        }
        return quotes;
    }

    /* external interface to use */
    public boolean set(String symbol, List<HistoricalQuote> quotes) {
        JsonArray jsonArray = JsonArray.create();
        for (int i = 0; i < quotes.size(); ++i) {
            JsonObject jsonObject = JsonObject.create();
            HistoricalQuote quote = quotes.get(i);
            jsonObject.put("open", quote.getOpen());
            jsonObject.put("low", quote.getLow());
            jsonObject.put("high", quote.getHigh());
            jsonObject.put("close", quote.getClose());
            if (quote.getAdjClose() != null) {
                jsonObject.put("adjClose", quote.getAdjClose());
            }
            jsonObject.put("volume", quote.getVolume());
            jsonObject.put("date", quote.getDate().getTime().getTime());
            jsonArray.add(jsonObject);
        }
        JsonObject insertObj = JsonObject.create().put("daily", jsonArray).put("last_updated", System.currentTimeMillis());
        CouchbaseClient.getInstance().set(symbol, insertObj);
        return true;
    }

    public long getUpdatedTime(String symbol) {
        JsonDocument jsonDocument = CouchbaseClient.getInstance().get(symbol);
        if (jsonDocument == null) {
            return 0L;
        }
        return (long) jsonDocument.content().get("last_updated");
    }

    // TODO: right now only check time interval. there is actually weekend, holidays or sometime manually run the program
    public boolean todayHasUpdated(String symbol) {
        long currentTime = System.currentTimeMillis();
        long updateTime = getUpdatedTime(symbol);
        return (currentTime - updateTime) < ONE_DAY_IN_MS;
    }

    public static void main(String[] args) throws Exception {
        String ticker = "MSFT";
        List<HistoricalQuote> quotes = YahooFinanceDownloader.getInstance().downloadQuotes(ticker, Interval.DAILY);
        StockRecordData.getInstance().set(ticker, quotes);
        List<HistoricalQuote> quotes1 = StockRecordData.getInstance().get(ticker);
        System.out.println(quotes1);
        boolean hasUpdated = StockRecordData.getInstance().todayHasUpdated(ticker);
        System.out.println(hasUpdated);
    }
}
