package com.efinancialpal.datacrawl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by haiyanluo on 10/22/16.
 */
public class DataRomaCrawler {
    private static final Logger logger = LoggerFactory.getLogger(DataRomaCrawler.class);

    static final String WARREN_BUFFET_URL = "http://www.dataroma.com/m/holdings.php?m=brk";
}
