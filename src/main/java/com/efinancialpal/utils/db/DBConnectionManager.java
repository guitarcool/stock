package com.efinancialpal.utils.db;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.efinancialpal.swagger.rest.config.ConfigurationManager;

public class DBConnectionManager {
    private static Logger logger = LoggerFactory.getLogger(DBConnectionManager.class);
    
    private static DBConnectionManager instance = new DBConnectionManager();
    
    private Map<String, DataSource> dbPools = new HashMap<>();
    
    private Configuration conf = ConfigurationManager.getConfiguration();
    
    private DBConnectionManager() {
    }
    
    public static DBConnectionManager getInstance() {
        return instance;
    }
    
    public DataSource createDataSource(String dbName) {
        PoolProperties properties = new PoolProperties();
        properties.setName(dbName);
            
        String databaseUrl = conf.getString("database_url", "jdbc:mysql://localhost:3306/");
        String userName = conf.getString("database_username", "root");
        String password = conf.getString("database_password", "Cisco123");
        properties.setUrl(databaseUrl + dbName);
        properties.setDriverClassName("com.mysql.jdbc.Driver");
        properties.setUsername(userName);
        if (password != null) {
            properties.setPassword(password);
            }
        
        properties.setJmxEnabled(true);
        properties.setTestWhileIdle(false);
        properties.setTestOnBorrow(true);
        properties.setValidationQuery("SELECT 1");
        properties.setTestOnReturn(false);
        properties.setValidationInterval(30000);
        properties.setTimeBetweenEvictionRunsMillis(30000);
        properties.setMaxActive(100);
        properties.setInitialSize(10);
        properties.setMaxWait(10000);
        properties.setRemoveAbandonedTimeout(60);
        properties.setMinEvictableIdleTimeMillis(30000);
        properties.setMinIdle(10);
        properties.setLogAbandoned(true);
        properties.setRemoveAbandoned(true);
        /*
        properties.setJdbcInterceptors(
          "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
          "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
          */
        properties.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
                        + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer;"
                        + "org.apache.tomcat.jdbc.pool.interceptor.ResetAbandonedTimer");
        
        logger.info("connection info {}", properties.toString());
        DataSource dataSource = new DataSource();
        
        dataSource.setPoolProperties(properties);
        
        try {
            dataSource.createPool();
        }
        catch (Throwable t) {
            logger.error("Failed to create connection pool for {}", dbName);
        }
        
        return dataSource;
    }
    
    public synchronized DataSource getDataSource(String dbName) {
       if (!dbPools.containsKey(dbName))  {
           DataSource dataSource = createDataSource(dbName);
           dbPools.put(dbName, dataSource);
       }
       
       return dbPools.get(dbName);
    }
    
    public void returnConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            logger.error("Exception happened.", e);
        }
    }

    public Map<String, DataSource> getDbPools() {
        return dbPools;
    }
}
