package com.efinancialpal.utils.db;

import java.util.List;
import java.util.Map;

import org.javalite.activejdbc.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.efinancialpal.utils.Constants;

public class DatabaseConnector {
    private static Logger logger = LoggerFactory.getLogger(DatabaseConnector.class);

    public static void main(String[] args) throws Exception {
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            db.exec("insert into crop_plan(name, description, active) values('test1', 'test1', 1)");
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll("select id, name, description active from crop_plan");
            
            System.out.println(result);
        } catch (Throwable t) {
            logger.error("Exception in processing database:", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }

    }
}
