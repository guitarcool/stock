package com.efinancialpal.utils.ticker;

import com.efinancialpal.couchbase.CouchbaseKeyValueCache;
import com.efinancialpal.swagger.model.Ticker;
import com.efinancialpal.swagger.rest.Constants;
import com.efinancialpal.utils.db.ActiveJDBCUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.ReaderInputStream;
import org.javalite.activejdbc.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadFactory;

/**
 * Created by haiyanluo on 9/20/16.
 */
public class TickerLoader {
    private static final Logger logger = LoggerFactory.getLogger(TickerLoader.class);

    private static final String nyseTickerFile = "/Users/haiyanluo/workspace/stock/tickers/nyse.csv";
    private static final String nasdaqTickerFile = "/Users/haiyanluo/workspace/stock/tickers/nasdaq.csv";
    private static final String etfTickerFile = "/Users/haiyanluo/workspace/stock/tickers/ETFList.csv";
    private static final String sp500TickerFile = "/Users/haiyanluo/workspace/stock/tickers/sp500.txt";
    private static final String finvizTickerFile = "/Users/haiyanluo/workspace/stock/tickers/finviz.txt";
    private static final String nasdaqTickerURL = "http://www.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=nasdaq&render=download";
    private static final String nyseTickerURL = "http://www.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=nyse&render=download";

    private static final String TICKER_REFRESHED_TIMESTAMP_KEY = "ticker_refreshed_timestamp";

    private static final Long ONE_DAY_IN_MILLISECOND = 24 * 60 * 60 * 1000L;
    private static final Long TICKER_UPDATE_INTERVAL_IN_DAYS = 30L;

    private static final String cvsSplitBy = ",";
    private static final float FLOOR_PRICE = 5.0f;
    private static final float FLOOR_MARKET_CAP = 10000f; // 10B, 10,000M

    private static TickerLoader instance = new TickerLoader();
    private volatile boolean isInitialized = false;
    private Object lock = new Object();

    private TickerLoader() {
        init();
    }

    public static TickerLoader getInstance() {
        return instance;
    }

    public void init() {
        // empty
    }

    /** Load the stock tickers into DB, where the CSVs are downloaded from NASDAQ exchange.
     * This operation only needs to be down rarely, e.g. once every 6 months, thus it can be
     * done manually.
     */
    public void loadTickersIntoDB() throws Exception {
        logger.info("First delete everything from ticker table");

        logger.info("Loading nyse tickers...");
        int count = loadTickerCSV(new FileReader(nyseTickerFile), "nyse");
        logger.info("Loaded {} nyse tickers into DB", count);
        logger.info("Loading nasdaq tickers...");
        count = loadTickerCSV(new FileReader(nasdaqTickerFile), "nasdaq");
        logger.info("Loaded {} nasdaq tickers into DB", count);
    }

    /** Load ticker symbols from tickers/ diretory which were downloaded manually
     *
     * @return
     * @throws Exception
     */
    public List<String> loadTickerSymbolsFromFiles() throws Exception {
        List<String> result = new ArrayList<>();
        File folder = new File("/Users/haiyanluo/workspace/stock/crawler/panda/data/");
        String[] fileNames = folder.list();
        for (String fileName : fileNames) {
           result.add(fileName.split("\\.")[0]);
        }

        logger.info("Loaded {} symbols", result.size());

        return result;
    }

    public void loadTickersFromUrl2DB() throws Exception {
        try (InputStream input = new URL(nasdaqTickerURL).openStream()) {
            Reader in = new InputStreamReader(input);
            int count = loadTickerCSV(in, "nasdaq");
            logger.info("Loaded {} nasdaq tickers into DB from {}", count, nasdaqTickerURL);
        } catch (Exception e) {
            logger.error("Failed to load nasdaq tickers from {}", nasdaqTickerURL, e);
            throw e;
        }

        try (InputStream input = new URL(nyseTickerURL).openStream()) {
            Reader in = new InputStreamReader(input);
            int count = loadTickerCSV(in, "nyse");
            logger.info("Loaded {} nyse tickers into DB from {}", count, nyseTickerURL);
        } catch (Exception e) {
            logger.error("Failed to load nyse tickers from {}", nasdaqTickerURL, e);
            throw e;
        }
    }


    // input csv format: "Symbol","Name","LastSale","MarketCap","IPOyear","Sector","industry","Summary Quote",
    private int loadTickerCSV(Reader in, String exchange) throws FileNotFoundException {
        int counter = 0;

        Iterable<CSVRecord> records = null;
        try {
            records = CSVFormat.RFC4180.withHeader("Symbol", "Name", "LastSale", "MarketCap", "ADR TSO", "IPOyear", "Sector", "Industry", "Summary Quote").parse(in);
        } catch (IOException e) {
            e.printStackTrace();
            return counter;
        }
        for (CSVRecord record : records) {
            String symbol = record.get("Symbol");
            String name = record.get("Name");

            Float lastSale = null;
            try {
                lastSale = Float.valueOf(record.get("LastSale"));
            }
            catch (Exception e) {
                logger.warn("last sale price for {} with value {} is unknown, skip", symbol, lastSale);
                lastSale = null;
            }

            String marketCap = record.get("MarketCap");
            // ignore ADR TSO for now
            String ipoYear = record.get("IPOyear");
            String sector = record.get("Sector");
            String industry = record.get("Industry");
            String summaryQuote = record.get("Summary Quote");
            if (symbol.equals("Symbol")) {
                // skip header
                continue;
            }

            // logger.debug("symbol: {}, name: {}, lastSale: {}, marketCap: {}, ipoYear: {}, sector: {}, industry: {}, summaryQuote: {}", symbol, name, lastSale, marketCap, ipoYear, sector, industry, summaryQuote) ;

            try {
                Ticker ticker = new Ticker();
                ticker.setSymbol(symbol);
                ticker.setName(name);
                ticker.setLastSale(lastSale);
                Float marketCapValue = null;
                /*
                if (marketCap.startsWith("$")) {
                    marketCap = marketCap.substring(1, marketCap.length());
                }
                if (marketCap.endsWith("M")) {
                    marketCap = marketCap.substring(0, marketCap.length() - 1);
                    marketCapValue = Float.valueOf(marketCap);
                } else if (marketCap.endsWith("B")) {
                    marketCap = marketCap.substring(0, marketCap.length() - 1);
                    marketCapValue = Float.valueOf(marketCap);
                    marketCapValue *= 1000;
                } */
                try {
                   marketCapValue = (float) (Double.valueOf(marketCap) / 1000000.0f); // convert to million
                } catch (Exception e) {
                    logger.error("Failed to convert marketCap {}", marketCap, e);
                }
                ticker.setMaketCap(marketCapValue);
                if (!ipoYear.equals("n/a")) {
                    ticker.setIpoYear(ipoYear);
                }
                ticker.setSector(sector);
                ticker.setIndustry(industry);
                ticker.setSummaryQuote(summaryQuote);
                ticker.setExchange(exchange);

                ticker.insert();

            } catch (Throwable t) {
                logger.error("Failed to read ticker symbol {}", symbol, t);
            }
        }
        return counter;
    }

    public boolean needToRefreshTickers() {
        try {
            String lastTickerUpdatedTimestamp = CouchbaseKeyValueCache.getInstance().get(TICKER_REFRESHED_TIMESTAMP_KEY);
            if (lastTickerUpdatedTimestamp != null) {
                final Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(Long.valueOf(lastTickerUpdatedTimestamp));
                final String timeString =
                        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(cal.getTime());
                logger.info("Tickers were updated last time at {}", timeString);
            } else {
                logger.info("Tickers were not updated before, first first this is being rum?");
            }
            if (lastTickerUpdatedTimestamp == null
                    || (System.currentTimeMillis() - Long.valueOf(lastTickerUpdatedTimestamp) > TICKER_UPDATE_INTERVAL_IN_DAYS * ONE_DAY_IN_MILLISECOND)) {
                return true;
            }
        } catch (Exception e) {
            logger.error("Failed to read and refresh tickers ", e);
            throw  e;
        }
        return false;
    }

    public List<Ticker> getCandidateStocks(float floorMarketCap, float floorPrice) throws Exception {
        if (needToRefreshTickers()) {
            logger.info("refreshing tickers...");
            // first delete all old tickers
            deleteAllTickers();
            loadTickersFromUrl2DB();
            CouchbaseKeyValueCache.getInstance().set(TICKER_REFRESHED_TIMESTAMP_KEY, String.valueOf(System.currentTimeMillis()));
        }
        // return Ticker.find("market_cap is not null and last_sale is not null and market_cap > ? and last_sale > ?", floorMarketCap, floorPrice);
        // return Ticker.find("market_cap is not null and last_sale is not null and market_cap > ? and last_sale > ?", floorMarketCap, floorPrice);
        return Ticker.find("last_sale is not null and last_sale > ? ", floorPrice);
    }

    public void deleteAllTickers() {
        Ticker.deleteAll();
    }

    public List<Ticker> getLowPriceCandidateStocks(float minPrice, float maxPrice) {
        if (Ticker.findAll().isEmpty()) {
            // lazy loading
            try {
                loadTickersIntoDB();
            }catch (Exception e) {
                logger.error("unable to load tickers into database", e);
            }
        }
        return Ticker.find("last_sale is not null and last_sale between ? and ?", minPrice, maxPrice);
    }


    public static void main(String[] args) throws Exception {
        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
            TickerLoader.getInstance().loadTickersIntoDB();
        }
        catch (Exception e) {
            System.out.println(e);
        }

        /*
        float floorMarketCap = 10000f;
        float floorPrice = 10f;
        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
            List<Ticker> candidates = TickerLoader.getInstance().getCandidateStocks(floorMarketCap, floorPrice);
            for (Ticker ticker : candidates) {
                logger.info("candidate stock: {}", ticker.getSymbol());
            }
        }
        catch (Exception e) {

        }
        */

    }

}
