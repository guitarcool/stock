package com.efinancialpal.utils.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UpstreamCache {
    private static final Logger logger = LoggerFactory.getLogger(UpstreamCache.class);

    private static int CACHE_SIZE = 100;
    private static int QUEUE_SIZE = 1000; // not used for now, so the event queue is unbounded for now
    
    /** Cache queue to hold the latest events, so that they can be retrieved fast enough */
    
    
    private static UpstreamCache instance = new UpstreamCache();
    
    private UpstreamCache() {
    }
    
    public static UpstreamCache  getInstance() {
        return instance;
    }
    
}
