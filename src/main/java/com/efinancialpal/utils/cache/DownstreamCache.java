package com.efinancialpal.utils.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DownstreamCache {
    private static final Logger logger = LoggerFactory.getLogger(UpstreamCache.class);

    private static int QUEUE_SIZE = 1000;
    
    
    private static DownstreamCache instance = new DownstreamCache();
    
    private DownstreamCache() {
    }
    
    public static DownstreamCache  getInstance() {
        return instance;
    }
    
}
