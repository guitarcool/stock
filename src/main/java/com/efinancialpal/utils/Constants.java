package com.efinancialpal.utils;

public class Constants {
    public final static String MEDIA_TYPE_PROTOBUF = "application/x-protobuf";
    public final static String EFINANCIALPAL = "efinancialpal";
}
