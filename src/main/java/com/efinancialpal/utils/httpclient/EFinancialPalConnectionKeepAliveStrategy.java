package com.efinancialpal.utils.httpclient;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

public class EFinancialPalConnectionKeepAliveStrategy implements ConnectionKeepAliveStrategy {
    private long timeout;
    
    public EFinancialPalConnectionKeepAliveStrategy(long timeout) {
        this.timeout = timeout;
    }

    @Override
    public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
        if (response == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
        while (it.hasNext()) {
           HeaderElement he = it.nextElement();
           String param = he.getName();
           String value = he.getValue();
           if (value != null && param.equalsIgnoreCase("timeout")) {
               try {
                   return Long.parseLong(value) * 1000;
               }
               catch (NumberFormatException e) {
                   
               }
           }
        }
        
        return timeout * 1000;
    }

}
