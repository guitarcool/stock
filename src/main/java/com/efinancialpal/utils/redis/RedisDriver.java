package com.efinancialpal.utils.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

public class RedisDriver {

    private static JedisPool redisDriver;
    private static final Logger logger = LoggerFactory.getLogger(RedisDriver.class);
    
    private static final String REDIS_HOST = "localhost";
    private static final int REDIS_TIMEOUT_SECONDS = 15;

    static {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(230);
        jedisPoolConfig.setTestOnBorrow(true);
        redisDriver = new JedisPool(jedisPoolConfig, REDIS_HOST, Protocol.DEFAULT_PORT, REDIS_TIMEOUT_SECONDS * 1000, null, Protocol.DEFAULT_DATABASE, null);
    }

    public static JedisPool getRedisDriver() {
        return redisDriver;
    }

    public static void destroyPool() {
        redisDriver.destroy();
    }
}
