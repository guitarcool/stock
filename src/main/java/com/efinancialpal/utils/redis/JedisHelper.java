package com.efinancialpal.utils.redis;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;

public class JedisHelper {
    private static final Logger logger = LoggerFactory.getLogger(JedisHelper.class);
    private static final String KEY_SEPARATOR = ":";
    // private static Gson gson = new Gson();
    private static Calendar calendar = Calendar.getInstance();

       
    @SuppressWarnings("unchecked")
    public static String getFromRedis(String key) {
        List<String> storedEvents = new ArrayList<String>();
        try (Jedis jedis = RedisDriver.getRedisDriver().getResource()) {
            storedEvents = jedis.lrange(key, 0, -1); // returning all values
        }
        catch (Throwable t) {
            logger.error("Error on retrieving events for key {}", key);
            // metrics
            throw t;
        }
        
        logger.info("Retrieved {} events for key {}", storedEvents.size(), key);
        
        JSONArray list = new JSONArray();
        for (String event : storedEvents) {
            list.add(event);
        }
        
        StringWriter out = new StringWriter();
        try {
            list.writeJSONString(out);
        } catch (IOException e) {
            logger.error("Failed to write json string for {}", out, e);
            return null;
        }
        
        String jsonText = out.toString();
        return jsonText;
    }
    
        
}
