package com.efinancialpal.schedule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.matchers.GroupMatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulerManager implements SchedulerManagerMBean {
	private static Logger logger = LoggerFactory.getLogger(SchedulerManager.class);

	private static final SchedulerManager instance = new SchedulerManager();
	static SchedulerManager getInstance() {
		return instance;
	}
	
	private SchedulerManager() { }

	@Override
	public String[] getScheduledJobs() {
		List<String> jobList = new ArrayList<String>();
		
		try {
			Scheduler scheduler = SchedulerModule.getInstance().getScheduler();
			
			List<String> groupNameList = scheduler.getJobGroupNames();
			if (groupNameList != null && !groupNameList.isEmpty()) {
				for (String groupName : groupNameList) {
					
					Set<JobKey> keySet = scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName));
					
					for (JobKey jobKey : keySet) {
						jobList.add(jobKey.toString());
					}
				}
			}
		}
		catch (SchedulerException e) {
			logger.error("Failed to find scheduled jobs.", e);
		}
		
		String[] result = null;
		if (!jobList.isEmpty()) {
			result = new String[jobList.size()];
			jobList.toArray(result);
			
			Arrays.sort(result);
		}
		
		return result;
	}

	@Override
	public void executeJob(String jobKeyString) {
		if (StringUtils.isBlank(jobKeyString)) {
			return;
		}
		
		// parse job key
		try {
			String[] input = jobKeyString.split("\\.");
			JobKey jobKey = new JobKey(input[1].trim(), input[0].trim());
			
			Scheduler scheduler = SchedulerModule.getInstance().getScheduler();
			scheduler.triggerJob(jobKey);
		}
		catch (Exception e) {
			logger.error("Failed to execute job {}.", jobKeyString, e);
		}
	}
	
	public static void main(String[] args) {
	}
}
