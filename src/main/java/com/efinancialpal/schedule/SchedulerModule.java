package com.efinancialpal.schedule;

import javax.management.ObjectName;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.efinancialpal.service.Module;
import com.efinancialpal.service.ModuleManager;

/**
 * Job scheduling module based on quartz.
 * 
 */
public class SchedulerModule extends Module {
	private static Logger logger = LoggerFactory.getLogger(SchedulerModule.class);

	private Scheduler scheduler;
	
	private static SchedulerModule instance;
	public static SchedulerModule getInstance() {
		return instance;
	}
	
	@Override
	public boolean init() {
		instance = this;

		try {
			// Grab the Scheduler instance from the Factory 
            scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();
			
			ModuleManager.registerMBean(
					SchedulerManager.getInstance(), 
					new ObjectName("com.yahoo.ads.dc.platform.schedule:name=SchedulerManager"));
            
			initialized = true;
		}
		catch (Exception e) {
			logger.error("Failed to initialize SchedulerModule", e);
		}

		return initialized;
	}

	@Override
	public void destroy() {
		super.destroy();

		try {
			scheduler.shutdown(true);
		}
		catch (SchedulerException e) {
			logger.error("Failed to shutdown SchedulerModule", e);
		}
	}
	
	/**
	 * Get scheduler.
	 * 
	 * @return
	 */
	Scheduler getScheduler() {
		return scheduler;
	}
	
	/**
	 * Schedule a job.
	 * 
	 * @param job
	 * @param trigger
	 * @throws SchedulerException
	 */
	public void scheduleJob(JobDetail job, Trigger trigger) throws SchedulerException {
		scheduler.scheduleJob(job, trigger);
	}
}