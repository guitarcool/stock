package com.efinancialpal.schedule;

import com.efinancialpal.service.YahooFinanceDownloader;
import com.efinancialpal.swagger.rest.config.ConfigurationManager;
import org.apache.commons.configuration.Configuration;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by haiyan on 7/23/2016.
 */
public class StockStatsDownloadJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(StockStatsDownloadJob.class);
    private static final Configuration conf   = ConfigurationManager.getConfiguration();
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String[] tickers = conf.getStringArray("tickers");
        for (String ticker : tickers) {
            logger.info("Executing the stock stats download job for ticker {}", ticker);
            try {
                YahooFinanceDownloader.getInstance().downloadAndStoreStockStats(ticker);
            }
            catch (Exception e) {
                logger.error("Failed to schedule downloading stock stats job for ticker: {}", ticker, e);
            }
        }
    }
}
