package com.efinancialpal.schedule;

/**
 * JMX MBean for scheduler.
 * 
 */
public interface SchedulerManagerMBean {
	/**
	 * Get all scheduled jobs.
	 * 
	 * @return scheduled jobs
	 */
	public String[] getScheduledJobs();

	/**
	 * Schedule a job now.
	 * 
	 * @param jobKeyString
	 */
	public void executeJob(String jobKeyString);
}
