package com.efinancialpal.swagger.rest;

public class Constants {
        public final static String MEDIA_TYPE_PROTOBUF = "application/x-protobuf";
        public final static String EFINANCIALPAL = "efinancialpal";
        public final static String EFINANCIALPAL_DB_NAME = "efinancialpal";
        public final static String STOCK = "stock";
}
