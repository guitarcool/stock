package com.efinancialpal.swagger.rest;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.efinancialpal.fa.FinancialAnalyzer;
import com.efinancialpal.service.YahooFinanceDownloader;
import com.efinancialpal.ta.TechnicalAnalyzer;
import org.apache.commons.collections4.map.HashedMap;
import org.javalite.activejdbc.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.efinancialpal.swagger.model.CompanyProduct;
import com.efinancialpal.swagger.model.ExecutiveSummary;
import com.efinancialpal.swagger.model.ReportReference;
import com.efinancialpal.swagger.model.Category;
import com.efinancialpal.swagger.model.InvestmentRisk;
import com.efinancialpal.swagger.model.TradingStrategy;
import com.efinancialpal.swagger.model.ValueAnalysis;
import com.efinancialpal.utils.db.ActiveJDBCUtil;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

@Api(value = "/stock", description = "Endpoint for stock operations")
@Path("/stock")
public class StockEndpoint {
    private static Logger logger = LoggerFactory.getLogger(StockEndpoint.class);

    private static final BigDecimal PEG_THRESHOLD = new BigDecimal(2.0);

    @GET
    @Path("/stats/{ticker}")
    @ApiOperation(value = "Get a stock stats")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "stats", response = Response.class)})
    public Response getStock(@ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker)
            throws Exception {
        Stock stock = YahooFinance.get(ticker);

        Map<String, BigDecimal> map = new HashMap<>();
        BigDecimal price = stock.getQuote().getPrice();
        BigDecimal change = stock.getQuote().getChangeInPercent();
        BigDecimal peg = stock.getStats().getPeg();
        BigDecimal dividend = stock.getDividend().getAnnualYieldPercent();

        logger.info("price: {}", price);
        logger.info("change: {}", change);
        logger.info("peg: {}", peg);
        logger.info("dividend: {}", dividend);
        map.put("price", price);
        map.put("change", change);
        map.put("peg", peg);
        map.put("dividend", dividend);

        return Response.status(Status.OK).entity(map).type(MediaType.APPLICATION_JSON).build();

    }

    @GET
    @Path("/generate_report/{ticker}")
    @ApiOperation(value = "generate report")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "report", response = Response.class)})
    public Response generateReport(@ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker)
            throws Exception {
        logger.info("Generating stock report for ticker {}", ticker);
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);

            String query = "select * from company_profile where ticker = '" + ticker + "' and active=true";
            logger.info("sql query: {}", query);

            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);

            query = "select * from executive_summary where ticker = '" + ticker + "' and active=true";
            logger.info("sql query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result1 = db.findAll(query);

            query = "select * from report_reference where ticker = '" + ticker + "' and active=true";
            logger.info("sql query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> refResult = db.findAll(query);

            query = "select * from category where ticker = '" + ticker + "' and active=true";
            logger.info("sql query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> catResult = db.findAll(query);

            query = "select * from company_product where ticker = '" + ticker + "' and active=true";
            logger.info("sql query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> productResult = db.findAll(query);

            query = "select * from investment_risk where ticker = '" + ticker + "' and active=true";
            logger.info("sql query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> riskResult = db.findAll(query);

            query = "select * from trading_strategy where ticker = '" + ticker + "' and active=true";
            logger.info("sql query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> strategyResult = db.findAll(query);

            query = "select * from value_analysis where ticker = '" + ticker + "' and active=true";
            logger.info("sql query: {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> analysisResult = db.findAll(query);


            Map<String, List<Map>> finalResult = new HashMap<>();
            if (result != null) {
                finalResult.put("company_profile", result);
            }
            if (result1 != null) {
                finalResult.put("executive_summary", result1);
            }
            if (refResult != null) {
                finalResult.put("references", refResult);
            }
            if (catResult != null) {
                finalResult.put("category", catResult);
            }
            if (productResult != null) {
                finalResult.put("company_product", productResult);
            }
            if (riskResult != null) {
                finalResult.put("investment_risk", riskResult);
            }
            if (strategyResult != null) {
                finalResult.put("trading_strategy", strategyResult);
            }
            if (analysisResult != null) {
                finalResult.put("value_analysis", analysisResult);
            }

            // technical analysis
            popluateTechnicalAnalysis(finalResult, ticker);

            // financial analysis
            popluateFinancialAnalysis(finalResult, ticker);

            if (!finalResult.isEmpty()) {
                return Response.status(Status.OK).entity(finalResult).type(MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
            }
        } catch (Throwable t) {
            logger.error("Failed to get ticker info for {}:", ticker, t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }

    }

    private void popluateTechnicalAnalysis( Map<String, List<Map>> finalResult, String ticker) {
        boolean breakout = TechnicalAnalyzer.getInstance().newHigh50Weeks(ticker);
        List<Map>  ta = new ArrayList<>();
        Map<String, String> taKeyValues = new HashedMap<>();
        taKeyValues.put("50weeksBreakout", String.valueOf(breakout));
        ta.add(taKeyValues);

        finalResult.put("technical_analysis", ta);

        return;
    }

    private void popluateFinancialAnalysis( Map<String, List<Map>> finalResult, String ticker) {
        boolean lowPeg = FinancialAnalyzer.getInstance().lowPeg(ticker, PEG_THRESHOLD); // TODO: hard code for now
        List<Map> fa = new ArrayList<>();
        Map<String, String> faKeyValues = new HashedMap<>();
        faKeyValues.put("peg < " + PEG_THRESHOLD, String.valueOf(lowPeg));
        fa.add(faKeyValues);

        finalResult.put("financial_analysis", fa);

        return;
    }


    @GET
    @Path("/daily_data_inject_full/{ticker}")
    @ApiOperation(value = "inject daily transction data from scratch")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "quotes", response = Response.class)})
    public Response populateDailyTransactionData(
            @ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker) throws Exception {
        logger.info("Injecting historic data for ticker {} from Yahoo Finance from scratch", ticker);

        return YahooFinanceDownloader.getInstance().downloadQuotes(ticker);
    }

    @GET
    @Path("/inject_stock_stats/{ticker}")
    @ApiOperation(value = "inject stock stats data from scratch")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "stats", response = Response.class)})
    public IdResponse populateStockStats(
            @ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker) throws Exception {
        logger.info("Injecting stock stats for ticker {} from Yahoo Finance from scratch", ticker);

        Long id = YahooFinanceDownloader.getInstance().downloadAndStoreStockStats(ticker);
        IdResponse response = new IdResponse(id);
        return response;
    }

    public static void showStock(String ticker) throws IOException {

        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.YEAR, -10); // from 10 years ago

        Stock stock = YahooFinance.get(ticker, from, to, Interval.DAILY);
        List<HistoricalQuote> historicQuotes = stock.getHistory();
        BigDecimal accu20wks = new BigDecimal("0");
        BigDecimal accu10wks = new BigDecimal("0");
        int numOfHistoricData = historicQuotes.size();
        Collections.reverse(historicQuotes);
        boolean uptrend = false;
        for (int i = 0; i < numOfHistoricData; ++i) {
            HistoricalQuote hq = historicQuotes.get(i);
            accu10wks = accu10wks.add(hq.getAdjClose());
            accu20wks = accu20wks.add(hq.getAdjClose());
            if (i >= 10) {
                accu10wks = accu10wks.subtract(historicQuotes.get(i - 10).getAdjClose());
            }
            if (i >= 20) {
                accu20wks = accu20wks.subtract(historicQuotes.get(i - 20).getAdjClose());
                BigDecimal avg10wks = accu10wks.divide(new BigDecimal("10"));
                BigDecimal avg20wks = accu20wks.divide(new BigDecimal("20"));
                if (!uptrend) {
                    if (avg10wks.compareTo(avg20wks) > 0) {
                        System.out.print("up cross:");
                        System.out.print(hq.getDate().getTime());
                        System.out.print("==>");
                        System.out.println(hq.getAdjClose());
                        uptrend = true;
                    }
                } else {
                    if (avg10wks.compareTo(avg20wks) < 0) {
                        System.out.print("down cross:");
                        System.out.print(hq.getDate().getTime());
                        System.out.print("==>");
                        System.out.println(hq.getAdjClose());
                        uptrend = false;
                    }

                }
            }

        }
    }

    /**
     * Endpoint to create a executive summary
     */
    @POST
    @Path("/executive_summary")
    @ApiOperation(value = "create an executive summary", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created", response = IdResponse.class)})
    public IdResponse createExcutiveSummary(
            @ApiParam(value = "ticker", required = true) @FormParam("ticker") String ticker,
            @ApiParam(value = "healine", required = true) @FormParam("headline") String headline,
            @ApiParam(value = "suggestion", required = true) @FormParam("suggestion") String suggestion,
            @ApiParam(value = "author", required = false) @FormParam("author") String author)
            throws Exception {

        logger.info("Adding a executive suggestion for ticker: {} with: {}", ticker, suggestion);

        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            ExecutiveSummary executiveSummary = new ExecutiveSummary();
            executiveSummary.setTicker(ticker);
            executiveSummary.setSuggestion(suggestion);
            executiveSummary.setHeadline(headline);
            if (author != null) {
                executiveSummary.setAuthor(author);
            }
            executiveSummary.insert();
            IdResponse response = new IdResponse(executiveSummary.getId());
            logger.info("Successfully added executive summary with id {}", executiveSummary.getId());
            return response;
        } catch (Throwable t) {
            logger.error("Exception happened while creating executive summary ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    /**
     * Endpoint to create a reference
     */
    @POST
    @Path("/report_reference")
    @ApiOperation(value = "create a reference", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created", response = IdResponse.class)})
    public IdResponse createReference(
            @ApiParam(value = "ticker", required = true) @FormParam("ticker") String ticker,
            @ApiParam(value = "reference", required = true) @FormParam("reference") String reference,
            @ApiParam(value = "author", required = false) @FormParam("author") String author)
            throws Exception {

        logger.info("Adding a reference for ticker: {} with: {}", ticker, reference);

        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            ReportReference ref = new ReportReference();
            ref.setTicker(ticker);
            ref.setReference(reference);
            if (author != null) {
                ref.setReference(author);
            }
            ref.insert();
            IdResponse response = new IdResponse(ref.getId());
            logger.info("Successfully added reference with id {}", ref.getId());
            return response;
        } catch (Throwable t) {
            logger.error("Exception happened while creating reference ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    /**
     * Endpoint to create a category
     */
    @POST
    @Path("/category")
    @ApiOperation(value = "create a category ", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created", response = IdResponse.class)})
    public IdResponse createCategory(
            @ApiParam(value = "ticker", required = true) @FormParam("ticker") String ticker,
            @ApiParam(value = "category", required = true) @FormParam("category") String category,
            @ApiParam(value = "categoryDescription", required = true) @FormParam("categoryDescription") String categoryDescription,
            @ApiParam(value = "author", required = false) @FormParam("author") String author)
            throws Exception {

        logger.info("Adding a category for ticker: {} with: {}", ticker, category);

        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            Category cat = new Category();
            cat.setTicker(ticker);
            cat.setCategory(category);
            cat.setCategoryDescription(categoryDescription);
            if (author != null) {
                cat.setAuthor(author);
            }
            cat.insert();
            IdResponse response = new IdResponse(cat.getId());
            logger.info("Successfully added category with id {}", cat.getId());
            return response;
        } catch (Throwable t) {
            logger.error("Exception happened while creating category ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    /**
     * Endpoint to create an investment risk
     */
    @POST
    @Path("/investment_risk")
    @ApiOperation(value = "create an investment risk", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created", response = IdResponse.class)})
    public IdResponse createInvestmentRisk(
            @ApiParam(value = "ticker", required = true) @FormParam("ticker") String ticker,
            @ApiParam(value = "risk_category", required = true, allowableValues = "competitor,opertion_pressure,financial_pressure") @FormParam("riskCategory") String riskCategory,
            @ApiParam(value = "risk", required = true) @FormParam("risk") String risk,
            @ApiParam(value = "author", required = false) @FormParam("author") String author)
            throws Exception {

        logger.info("Adding investment risk for ticker: {} with: {}", ticker, riskCategory);

        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            InvestmentRisk ir = new InvestmentRisk();
            ir.setTicker(ticker);
            ir.setRiskCategory(riskCategory);
            ir.setRisk(risk);
            if (author != null) {
                ir.setAuthor(author);
            }
            ir.insert();
            IdResponse response = new IdResponse(ir.getId());
            logger.info("Successfully added investment risk with id {}", ir.getId());
            return response;
        } catch (Throwable t) {
            logger.error("Exception happened while adding investment risk ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    /**
     * Endpoint to create a trading strategy
     */
    @POST
    @Path("/trading_strategy")
    @ApiOperation(value = "create a trading strategy", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created", response = IdResponse.class)})
    public IdResponse createTradingStrategy(
            @ApiParam(value = "ticker", required = true) @FormParam("ticker") String ticker,
            @ApiParam(value = "term", required = true, allowableValues = "short_term,mid_term,long_term") @FormParam("term") String term,
            @ApiParam(value = "strategy", required = true) @FormParam("strategy") String strategy,
            @ApiParam(value = "author", required = false) @FormParam("author") String author)
            throws Exception {

        logger.info("Adding trading strategy for ticker: {} with: {}", ticker, term);

        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            TradingStrategy ts = new TradingStrategy();
            ts.setTicker(ticker);
            ts.setTerm(term);
            ts.setStrategy(strategy);
            if (author != null) {
                ts.setAuthor(author);
            }
            ts.insert();
            IdResponse response = new IdResponse(ts.getId());
            logger.info("Successfully added trading strategy with id {}", ts.getId());
            return response;
        } catch (Throwable t) {
            logger.error("Exception happened while adding trading strategy ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }


    /**
     * Endpoint to create a value analysis
     */
    @POST
    @Path("/value_analysis")
    @ApiOperation(value = "create a value analysis", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created", response = IdResponse.class)})
    public IdResponse createValueAnalysis(
            @ApiParam(value = "ticker", required = true) @FormParam("ticker") String ticker,
            @ApiParam(value = "targetPrice", required = true) @FormParam("targetPrice") Float targetPrice,
            @ApiParam(value = "targetTime (yyyy-MM-dd)", required = true) @FormParam("targetTime") String targetTime,
            @ApiParam(value = "analysis", required = true) @FormParam("analysis") String analysis,
            @ApiParam(value = "author", required = false) @FormParam("author") String author)
            throws Exception {

        logger.info("Adding value analysis for ticker: {} with: {}", ticker, analysis);

        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            ValueAnalysis va = new ValueAnalysis();
            va.setTicker(ticker);
            va.setTargetPrice(targetPrice);
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date targetDate = (Date) formatter.parse(targetTime);
            va.setTargetTime(targetDate);
            va.setAnalysis(analysis);
            if (author != null) {
                va.setAuthor(author);
            }
            va.insert();
            IdResponse response = new IdResponse(va.getId());
            logger.info("Successfully added value analysis with id {}", va.getId());
            return response;
        } catch (Throwable t) {
            logger.error("Exception happened while adding value analysis ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    @GET
    @Path("/executive_summary/{ticker}")
    @ApiOperation(value = "Get executive summaries of a given ticker")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "all executive summary", response = Response.class)})
    public Response getExecutiveSummaries(
            @ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker) throws Exception {
        logger.info("Get executive summary for {}", ticker);
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);

            String query = "select * from executive_summary where ticker = '" + ticker + "'" + " and active=true";

            logger.info("Execting sql {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result != null) {
                return Response.status(Status.OK).entity(result).type(MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
            }
        } catch (Throwable t) {
            logger.error("Failed to get all executive summaries: ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    @GET
    @Path("/report_reference/{ticker}")
    @ApiOperation(value = "Get references of a given ticker")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "all references", response = Response.class)})
    public Response getReferences(
            @ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker) throws Exception {
        logger.info("Get references for {}", ticker);
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);

            String query = "select * from report_reference where ticker = '" + ticker + "'" + " and active=true";

            logger.info("Execting sql {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result != null) {
                return Response.status(Status.OK).entity(result).type(MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
            }
        } catch (Throwable t) {
            logger.error("Failed to get all references: ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    @GET
    @Path("/category/{ticker}")
    @ApiOperation(value = "Get the category of a given ticker")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "all categories ", response = Response.class)})
    public Response getCategory(
            @ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker) throws Exception {
        logger.info("Get category for {}", ticker);
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);

            String query = "select * from category where ticker = '" + ticker + "'" + " and active=true";

            logger.info("Execting sql {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result != null) {
                return Response.status(Status.OK).entity(result).type(MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
            }
        } catch (Throwable t) {
            logger.error("Failed to get all category : ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    @GET
    @Path("/investment_risk/{ticker}")
    @ApiOperation(value = "Get the investment risk of a given ticker")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "all investment risks ", response = Response.class)})
    public Response getInvestmentRisk(
            @ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker) throws Exception {
        logger.info("Get investment risk for {}", ticker);
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);

            String query = "select * from investment_risk where ticker = '" + ticker + "'" + " and active=true";

            logger.info("Execting sql {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result != null) {
                return Response.status(Status.OK).entity(result).type(MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
            }
        } catch (Throwable t) {
            logger.error("Failed to get investment risk : ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }


    @GET
    @Path("/trading_strategy/{ticker}")
    @ApiOperation(value = "Get the trading strategies of a given ticker")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "all trading strategies", response = Response.class)})
    public Response getTradingStrategies(
            @ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker) throws Exception {
        logger.info("Get trading strategies for {}", ticker);
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);

            String query = "select * from trading_strategy where ticker = '" + ticker + "'" + " and active=true";

            logger.info("Execting sql {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result != null) {
                return Response.status(Status.OK).entity(result).type(MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
            }
        } catch (Throwable t) {
            logger.error("Failed to get trading strategy: ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    @GET
    @Path("/value_analysis/{ticker}")
    @ApiOperation(value = "Get the value analysis of a given ticker")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "all value analysis ", response = Response.class)})
    public Response getValueAnalysis(
            @ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker) throws Exception {
        logger.info("Get value analysis for {}", ticker);
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);

            String query = "select * from value_analysis where ticker = '" + ticker + "'" + " and active=true";

            logger.info("Execting sql {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result != null) {
                return Response.status(Status.OK).entity(result).type(MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
            }
        } catch (Throwable t) {
            logger.error("Failed to get value analysis: ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    /**
     * Endpoint to create a company product
     */
    @POST
    @Path("/company_product/{ticker}")
    @ApiOperation(value = "create an company product", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created", response = IdResponse.class)})
    public IdResponse createCompanyProduct(
            @PathParam("ticker") String ticker,
            @ApiParam(value = "productName", required = true) @FormParam("productName") String productName,
            @ApiParam(value = "productDescription", required = true) @FormParam("productDescription") String productDescription,
            @ApiParam(value = "author", required = false) @FormParam("author") String author)
            throws Exception {

        logger.info("Adding a company product for ticker: {} with product name: {}, product description: {}", ticker, productName, productDescription);

        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            CompanyProduct product = new CompanyProduct();
            product.setTicker(ticker);
            product.setName(productName);
            product.setDescription(productDescription);
            if (author != null) {
                product.setAuthor(author);
            }
            product.insert();
            IdResponse response = new IdResponse(product.getId());
            logger.info("Successfully added compoabny product with id {}", product.getId());
            return response;
        } catch (Throwable t) {
            logger.error("Exception happened while creating company product ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    /**
     * Endpoint to update executive summary
     */
    @PUT
    @Path("/update_executive_summary/{ticker}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "update executive summary", notes = "Returns the updatestatus")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully upload a battery event")})
    public SuccessResponse updateExecutiveSummary(@PathParam("ticker") String ticker,
                                                  @ApiParam(value = "id", required = true) @FormParam("id") String id,
                                                  @ApiParam(value = "suggestion", required = true) @FormParam("suggestion") String suggestion,
                                                  @ApiParam(value = "author", required = false) @FormParam("author") String author
    ) throws Exception {

        logger.info("received PUT for exectuvie summary: {} for id {}", ticker, id);

        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            ExecutiveSummary oldExecutiveSummary = EFinancialPalValidator.validateExecutiveSummary(id, ticker);
            ExecutiveSummary newExecutiveSummary = new ExecutiveSummary();
            newExecutiveSummary.setId(oldExecutiveSummary.getId());
            newExecutiveSummary.setTicker(oldExecutiveSummary.getTicker());
            newExecutiveSummary.setSuggestion(suggestion);
            if (author != null) {
                newExecutiveSummary.setAuthor(author);
            }

            newExecutiveSummary.saveIt();

            logger.info("Updated executive summary {}", id);

            return new SuccessResponse();
        } catch (Throwable t) {
            logger.error("Failed to update executvie summary {}", id, t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    @DELETE
    @Path("/executive_summary/{ticker}")
    @ApiOperation(value = "Delete an executive summary")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "true", response = SuccessResponse.class)})
    public SuccessResponse deleteExecutiveSummary(@PathParam("ticker") String ticker,
                                                  @ApiParam(value = "id", required = true) @FormParam("id") String id
    ) throws Exception {
        logger.info("Deleting executive summary id: {} for ticker {}", id, ticker);
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);
            ExecutiveSummary executiveSummary = EFinancialPalValidator.validateExecutiveSummary(id, ticker);

            executiveSummary.setActive(false);
            executiveSummary.saveIt();
            logger.info("Successfully deleted executive summary {} for ticker {} ", id);
            return new SuccessResponse();
        } catch (Throwable t) {
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }


    @GET
    @Path("/company_product/{ticker}")
    @ApiOperation(value = "Get company products of a given ticker")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "all company products ", response = Response.class)})
    public Response getCompanyProducts(@ApiParam(value = "ticker", required = true) @PathParam("ticker") String ticker)
            throws Exception {
        logger.info("Get company products for {}", ticker);
        DB db = null;
        try {
            db = ActiveJDBCUtil.getDB(Constants.EFINANCIALPAL);

            String query = "select * from company_product where ticker = '" + ticker + "'";

            logger.info("Execting sql {}", query);
            @SuppressWarnings("rawtypes")
            List<Map> result = db.findAll(query);
            if (result != null) {
                return Response.status(Status.OK).entity(result).type(MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
            }
        } catch (Throwable t) {
            logger.error("Failed to get all company products: ", t);
            throw t;
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        showStock("WFC");
    }
}
