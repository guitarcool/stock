package com.efinancialpal.swagger.rest;

import org.json.simple.JSONObject;

public class SuccessfulJSONObject extends JSONObject{

	private static final long serialVersionUID = 1L;
	
	private int status;
	private String message;

	public SuccessfulJSONObject() {
		this(200, "Success");
	}
	
	public SuccessfulJSONObject(int status, String message) {
		this.status = status;
		this.message = message;
		this.put("status", status);
		this.put("message", message);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "SuccessfulJSONObject[status=" + status + ", message=" + message + "]";
	}
	
}
