package com.efinancialpal.swagger.rest.response;

public enum ErrorCode {
    Validation(1, "Validation"),
    ObjectNotExist(2, "Object does not exist");

    public int getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    private int code;
    private String type;
    
    private ErrorCode(int code, String type) {
        this.code = code;
        this.type = type;
    }
}
