package com.efinancialpal.swagger.rest;

public class IdResponse {
    private long id;
    
    // default constructor for serialization
    public IdResponse() {
        this.id = -1L;
    }
    
    public IdResponse(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
