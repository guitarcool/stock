package com.efinancialpal.swagger.rest;

import org.slf4j.LoggerFactory;

import com.efinancialpal.utils.HTTPUtil;
import com.efinancialpal.utils.httpclient.IdleConnectionMonitorThread;
import com.efinancialpal.utils.httpclient.EFinancialPalConnectionKeepAliveStrategy;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.slf4j.Logger;

public class EFinancialPalClient {
    private static final Logger logger = LoggerFactory.getLogger(EFinancialPalClient.class);
   
    private static volatile EFinancialPalClient instance = null;
    private static DefaultHttpClient httpClient;
    private static  PoolingClientConnectionManager connManager;
    
    private static volatile boolean initialized = false;
    private static Object initLock = new Object();
    
    public static EFinancialPalClient getInstance() {
        if (initialized) {
            return instance;
        }
        synchronized (initLock) {
            if (!initialized) {
                instance = new EFinancialPalClient();
                initialized = true;
            }
        }
        return instance;
    }
    
    private EFinancialPalClient() {
        long keepAliveTimeout = 60000;
        int maxConnection = 10;
        
        connManager = new PoolingClientConnectionManager();
        connManager.setDefaultMaxPerRoute(maxConnection);
        connManager.setMaxTotal(maxConnection);
        
        BasicHttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 2000);
        HttpConnectionParams.setSoTimeout(httpParams, 1000);
        HttpConnectionParams.setStaleCheckingEnabled(httpParams, true);
        
        httpClient = new DefaultHttpClient(connManager, httpParams);
        EFinancialPalConnectionKeepAliveStrategy strategy = new EFinancialPalConnectionKeepAliveStrategy(keepAliveTimeout);
        
        httpClient.setKeepAliveStrategy(strategy);
        
        int idleTimeout = 30000;
        
        new IdleConnectionMonitorThread(connManager, idleTimeout);
        
    }
    
    
    private boolean sendHttpRequest(HttpPut put) {
        try {
            HttpResponse response = httpClient.execute(put);
            
            logger.info("response : {} for putUrl {}", response, put.getURI());
            
            int code = response.getStatusLine().getStatusCode();
            HTTPUtil.consumeResponse(response);
            
            if (code != 200 && code != 201) {
                logger.error("Error sending http request with status code {} for put url {}", code, put.getURI());
                return false;
            }
            
            return true;
        }
        catch (Exception e) {
            logger.error("Exception while sending http request: ", e);
        }
        finally {
            if (put != null) {
                put.releaseConnection();
            }
        }
        return false;
    }
    
}
