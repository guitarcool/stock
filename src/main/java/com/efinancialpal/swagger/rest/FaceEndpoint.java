package com.efinancialpal.swagger.rest;

import com.efinancialpal.swagger.bean.HistoryBean;
import com.efinancialpal.swagger.model.History;
import com.efinancialpal.utils.db.ActiveJDBCUtil;
import com.efinancialpal.youtu.Youtu;
import com.wordnik.swagger.annotations.*;
import org.javalite.activejdbc.DB;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Created by haiyanluo on 10/27/17.
 */

@Api(value = "/face", description = "Endpoint for face recognition")
@Path("/face")
public class FaceEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(FaceEndpoint.class);
    private static final String APP_ID = "10105476";
    private static final String SECRET_ID = "AKIDoZDjGi9EIEAxb7riolaL0b5Qdjles1Vr";
    private static final String SECRET_KEY = "NcirvpjW9dy9A8vXyFPOBnQuCIEzYv6I";
    private static Youtu youtuInstance = new Youtu(APP_ID, SECRET_ID, SECRET_KEY,Youtu.API_YOUTU_END_POINT);

    @POST
    @Path("/person")
    @ApiOperation(value = "add a person", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully added", response = Response.class)})
    public Response addPerson(
            @ApiParam(value = "personId", required = true) @FormParam("personId") String personId,
            @ApiParam(value = "personName", required = true) @FormParam("personName") String personName,
            @ApiParam(value = "groupId", required = true) @FormParam("groupId") String groupId,
            @ApiParam(value = "photoBase64", required = true) @FormParam("photoBase64") String photoBase64,
            @ApiParam(value = "tag", required = true) @FormParam("tag") String tag
            ) throws Exception {
        JSONObject response = youtuInstance.NewPerson(photoBase64, personId, Arrays.asList(new String[]{groupId}), photoBase64, tag);
        return Response.status(Response.Status.OK).entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/face")
    @ApiOperation(value = "add a face", notes = "Returns the adding status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully added", response = Response.class)})
    public Response addFace(
            @ApiParam(value = "personId", required = true) @FormParam("personId") String personId,
            @ApiParam(value = "photoBase64", required = true) @FormParam("photoBase64") String photoBase64
    ) throws Exception {
        JSONObject response = youtuInstance.AddFace(personId, Arrays.asList(new String[]{photoBase64}));
        return Response.status(Response.Status.OK).entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/person/{personId}")
    @ApiOperation(value = "find a person", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully searched", response = Response.class)})
    public Response searchPerson(
            @ApiParam(value = "personId", required = true) @PathParam("personId") String personId
    ) throws Exception {
        JSONObject response = youtuInstance.GetInfo(personId);
        return Response.status(Response.Status.OK).entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/history/{personId}")
    @ApiOperation(value = "find a person's in out hisotry", notes = "Returns the history")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully searched", response = Response.class)})
    public Response inOutHistory(
            @ApiParam(value = "personId", required = true) @PathParam("personId") String personId
    ) throws Exception {
        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
            List<History> historyList = History.where("person_id = ?", personId);
            List<HistoryBean> result = new ArrayList<>();
            for (History history : historyList) {
                HistoryBean bean = new HistoryBean();
                bean.setId(history.getId());
                bean.setActive(history.getActive());
                bean.setInOut(history.getInOut());
                bean.setPersonId(history.getPersonId());
                bean.setTimestamp(history.getTimestamp());
                bean.setVersionUpdatedAt(history.getVersionUpdatedAt());
                result.add(bean);
            }

            return Response.status(Response.Status.OK).entity(result).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            logger.error("Unable to get DB instance");
            throw e;
        }
    }

    @POST
    @Path("/history")
    @ApiOperation(value = "add a in out entry to a person", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully added", response = Response.class)})
    public IdResponse addHistory(
            @ApiParam(value = "personId", required = true) @FormParam("personId") String personId,
            @ApiParam(value = "timestamp", required = true) @FormParam("timestamp") Date timestamp,
            @ApiParam(value = "inOut", required = true, allowableValues = "in,out") @FormParam("inOut") String inOut
    ) throws Exception {
        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
            History history = new History();
            history.setPersonId(personId);
            history.setTimestamp(timestamp);
            history.setInOut(inOut);
            history.setActive(true);
            history.setVersionUpdatedAt(new Date());

            history.insert();
            IdResponse respose = new IdResponse(history.getId());
            return respose;
        } catch (Exception e) {
            logger.error("Unable to get DB instance");
            throw e;
        }
    }

    @POST
    @Path("/compare")
    @ApiOperation(value = "compare two faces", notes = "Returns the comparison result")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully compared", response = Response.class)})
    public Response compareTwoFaces(
            @ApiParam(value = "faceA", required = true) @FormParam("faceA") String faceA,
            @ApiParam(value = "faceB", required = true) @FormParam("faceB") String faceB
    ) throws Exception {
        JSONObject response = youtuInstance.FaceCompare(faceA, faceB);
        return Response.status(Response.Status.OK).entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/faceverify")
    @ApiOperation(value = "Given a face, verify the person", notes = "Returns the verification result")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully verified", response = Response.class)})
    public Response faceVerify(
            @ApiParam(value = "personId", required = true) @FormParam("personId") String personId,
            @ApiParam(value = "faceImage", required = true) @FormParam("faceImage") String faceImage
    ) throws Exception {
        JSONObject response = youtuInstance.FaceVerify(faceImage, personId);
        return Response.status(Response.Status.OK).entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/faceidentify")
    @ApiOperation(value = "Given a face, identify the person from a group", notes = "Returns the identification result")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully verified", response = Response.class)})
    public Response faceIdentify(
            @ApiParam(value = "groupId", required = true) @FormParam("groupId") String groupId,
            @ApiParam(value = "faceImage", required = true) @FormParam("faceImage") String faceImage
    ) throws Exception {
        JSONObject response = youtuInstance.FaceIdentify(faceImage, groupId);
        return Response.status(Response.Status.OK).entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
    }


    public static void main(String[] args) throws Exception {
        FaceEndpoint faceEndpoint = new FaceEndpoint();
        // faceEndpoint.createPerson("");
    }
}
