package com.efinancialpal.swagger.rest.exception;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.efinancialpal.swagger.rest.response.ErrorCode;

public class ApiException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 2544429383162606639L;
    /**
     * 
     */
    private ErrorCode errorCode;
    
    public ApiException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
    
    public String toJson() {
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("code", errorCode.getCode());
        props.put("Type", errorCode.getType());
        props.put("message", getMessage());
        
        try {
            return new ObjectMapper().writeValueAsString(props);
        }
        catch (JsonProcessingException e) {
            return "";
        }
    }

}
