package com.efinancialpal.swagger.rest.exception;

import com.efinancialpal.swagger.rest.response.ErrorCode;

public class ValidationException extends ApiException {
    /**
     * 
     */
    private static final long serialVersionUID = 3323671994874914065L;

    public ValidationException(String message) {
        super(ErrorCode.Validation, message);
    }

}
