package com.efinancialpal.swagger.rest.exception;

import com.efinancialpal.swagger.rest.response.ErrorCode;

public class ObjectNotExistException extends ApiException {
    /**
     * 
     */
    private static final long serialVersionUID = 8125742409275473418L;

    public ObjectNotExistException(String message) {
        super(ErrorCode.ObjectNotExist, message);
    }
}
