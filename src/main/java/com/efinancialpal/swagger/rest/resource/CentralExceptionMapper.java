package com.efinancialpal.swagger.rest.resource;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.efinancialpal.swagger.rest.exception.ApiException;


@Provider
public class CentralExceptionMapper implements ExceptionMapper<ApiException> {

    @Override
    public Response toResponse(ApiException e) {
        return Response.status(Status.NOT_FOUND).entity(e.toJson()).type(MediaType.APPLICATION_JSON).build();
    }

}
