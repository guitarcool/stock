package com.efinancialpal.swagger.rest.config;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationManager {
    private static Logger logger = LoggerFactory
            .getLogger(ConfigurationManager.class);

    public static final String CONFIG_DELIM_REG = "\\.";
    public static final String CONFIG_DELIM = ".";

    private static final CompositeConfiguration cc = new CompositeConfiguration();

    static {
        try {
            logger.info("loading system properties ...");
            cc.addConfiguration(new SystemConfiguration());

            String configDir = cc.getString("config.dir", "src/main/resources");
            logger.info("config.dir is {}", configDir);

            PropertiesConfiguration prodConfiguration = new PropertiesConfiguration(String.format(
                    "%s/production.properties", configDir));
            cc.addConfiguration(prodConfiguration);
            
            PropertiesConfiguration swaggerConfiguration = new PropertiesConfiguration(String.format(
                    "%s/swagger.properties", configDir));
            cc.addConfiguration(swaggerConfiguration);

        } catch (ConfigurationException e) {
            logger.error("Failed to load configuration files", e);
        }
    }

    private ConfigurationManager() {
    }

    public static Configuration getConfiguration() {
        return cc;
    }

}