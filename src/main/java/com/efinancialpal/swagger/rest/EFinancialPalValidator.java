package com.efinancialpal.swagger.rest;

import org.javalite.activejdbc.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.efinancialpal.swagger.model.ExecutiveSummary;
import com.efinancialpal.swagger.rest.exception.ObjectNotExistException;
import com.efinancialpal.swagger.rest.exception.ValidationException;

import java.util.List;
import java.util.Map;

public class EFinancialPalValidator {
    private static final Logger logger = LoggerFactory.getLogger(EFinancialPalValidator.class);

    public static ExecutiveSummary validateExecutiveSummary(String id, String ticker)
            throws ObjectNotExistException, ValidationException {
        ExecutiveSummary executiveSummary = ExecutiveSummary.findById(id);
        if (executiveSummary == null || executiveSummary.getActive() == false) {
            throw new ObjectNotExistException("Cannot find executive summary for id: " + id);
        }
        if (!executiveSummary.getTicker().equalsIgnoreCase(ticker)) {
            throw new ValidationException("executive summary id " + id + " is not for ticker: " + ticker);
        }

        return executiveSummary;
    }

    public static Long stockStatsExist(DB db, String ticker) throws ObjectNotExistException {
        String query = "select * from stock_stats where ticker='" + ticker + "'";
        @SuppressWarnings("rawtypes")
        List<Map> result = db.findAll(query);
        if (result == null || result.isEmpty()) {
            return -1L;
        }
        return (Long)result.get(0).get("id");
    }

}
