package com.efinancialpal.swagger.rest;

import com.efinancialpal.swagger.model.InstitutionHoldings;
import com.efinancialpal.swagger.model.InvestInstitution;
import com.efinancialpal.utils.db.ActiveJDBCUtil;
import com.wordnik.swagger.annotations.*;
import org.javalite.activejdbc.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by haiyanluo on 10/22/16.
 */
@Api(value = "/dataroma", description = "Endpoint for dataroma operations")
@Path("/dataroma")
public class DataRomaEndpoint {
    private static final Logger logger = LoggerFactory
            .getLogger(DataRomaEndpoint.class);

    @GET
    @Path("/institutions")
    @ApiOperation(value = "Get a stock stats")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "stats", response = Response.class)})
    public Response getAllInstitutions()
            throws Exception {
        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
            List<InvestInstitution> investInstitutionList = InvestInstitution.findAll();
            return Response.status(Response.Status.OK).entity(investInstitutionList).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            logger.error("Failed to get all institutions");
        }
        return Response.status(Response.Status.OK).entity("").type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/institutions")
    @ApiOperation(value = "add an institution", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully added", response = IdResponse.class)})
    public IdResponse createInstitution(
            @ApiParam(value = "name", required = true) @FormParam("name") String name,
            @ApiParam(value = "portfolioValue", required = true) @FormParam("portfolioValue") BigDecimal portfolioValue,
            @ApiParam(value = "noOfStocks", required = true) @FormParam("noOfStocks") Long noOfStocks)
            throws Exception {
        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
            InvestInstitution investInstitution = new InvestInstitution();
            investInstitution.setName(name);
            investInstitution.setPortfolioValue(portfolioValue);
            investInstitution.setNoOfStocks(noOfStocks);
            investInstitution.insert();

            IdResponse response = new IdResponse(investInstitution.getId());
            return response;
        } catch (Exception e) {
            logger.error("Failed to insert institution {}", name, e);
            throw e;
        }
    }

    @GET
    @Path("/institution_holdings")
    @ApiOperation(value = "Get a institution holdings")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "stats", response = Response.class)})
    public Response getInstitutionHoldings(@ApiParam(value = "institutionId", required = true) @PathParam("institutionId") Long institutionId)
            throws Exception {
        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
            List<InstitutionHoldings> institutionHoldingses = InstitutionHoldings.find("institution_id = ?", institutionId);
            return Response.status(Response.Status.OK).entity(institutionHoldingses).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            logger.error("Failed to get all institutions");
            throw e;
        }
    }

    @POST
    @Path("/institution_holdings")
    @ApiOperation(value = "add an institution holding", notes = "Returns the creation status")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully added", response = IdResponse.class)})
    public IdResponse createInstitutionHolding(
            @ApiParam(value = "institutionId", required = true) @FormParam("institutionId") Long institutionId,
            @ApiParam(value = "ticker", required = true) @FormParam("ticker") String ticker,
            @ApiParam(value = "stockName", required = false) @FormParam("stockname") String stockName,
            @ApiParam(value = "percentage", required = false) @FormParam("percentage") Float percentage,
            @ApiParam(value = "shares", required = false) @FormParam("shares") Long shares,
            @ApiParam(value = "recentActivity", allowableValues = "Reduce,Add,Buy", required = false) @FormParam("recentActivity") String recentActivity,
            @ApiParam(value = "activityPercentage", required = false) @FormParam("activityPercentage") Float activityPercentage,
            @ApiParam(value = "reportedPrice", required = false) @FormParam("reportedPrice") Float reportedPrice
            )
            throws Exception {
        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
            InstitutionHoldings institutionHoldings = new InstitutionHoldings();
            institutionHoldings.setInstitutionId(institutionId);
            institutionHoldings.setTicker(ticker);
            institutionHoldings.setStockName(stockName);
            institutionHoldings.setPercentage(percentage);
            institutionHoldings.setShares(shares);
            institutionHoldings.setRecentActivity(recentActivity);
            institutionHoldings.setActivityPercentage(activityPercentage);
            institutionHoldings.setReportedPrice(reportedPrice);
            institutionHoldings.insert();

            IdResponse response = new IdResponse(institutionHoldings.getId());
            return response;
        } catch (Exception e) {
            logger.error("Failed to insert institution holdings for institution {}", institutionId, e);
            throw e;
        }
    }

}
