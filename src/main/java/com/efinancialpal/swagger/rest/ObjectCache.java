package com.efinancialpal.swagger.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObjectCache {
    private static Logger logger = LoggerFactory.getLogger(ObjectCache.class);

    private static volatile boolean initialized = false;
    public static Object initLock = new Object();
    
    private static ObjectCache instance =  null;
    
    public static ObjectCache getInstance() {
        if (initialized) {
            return instance;
        }
        synchronized (initLock) {
            if (!initialized) {
                instance = new ObjectCache();
                // instance.initFromDatabase();
                initialized = true;
            }
            return instance;
        }
    }
    
}
