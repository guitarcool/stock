package com.efinancialpal.swagger.rest;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceContext implements ServletContextListener {
    private final Logger logger = LoggerFactory.getLogger(ServiceContext.class);
	
//	@Override
	public void contextInitialized(ServletContextEvent event) {
		logger.info("Starting the web server...");

		// TODO: init the service logic
		
		logger.info("web server started.");
	}
	
//	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// shutdown service
		logger.info("Shutting down the web server...");
		
		// shut down the service
		
		logger.info("web server shut down");
	}
}
