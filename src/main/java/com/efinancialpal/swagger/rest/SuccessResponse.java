package com.efinancialpal.swagger.rest;

public class SuccessResponse {
    private boolean success = true;

    public boolean isSuccess() {
        return this.success;
    }

}
