package com.efinancialpal.swagger.bean;

import java.util.Date;

/**
 * Created by haiyanluo on 11/12/17.
 */
public class HistoryBean {
    private Long id;
    private String personId;
    private String inOut;
    private Date timestamp;
    private Boolean active;
    private Date versionUpdatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getInOut() {
        return inOut;
    }

    public void setInOut(String inOut) {
        this.inOut = inOut;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getVersionUpdatedAt() {
        return versionUpdatedAt;
    }

    public void setVersionUpdatedAt(Date versionUpdatedAt) {
        this.versionUpdatedAt = versionUpdatedAt;
    }
}
