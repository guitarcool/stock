package com.efinancialpal.swagger;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.efinancialpal.schedule.SchedulerModule;
import com.efinancialpal.schedule.StockQuoteDownloadJob;
import com.efinancialpal.schedule.StockStatsDownloadJob;
import com.efinancialpal.service.ModuleManager;
import com.efinancialpal.swagger.rest.ServiceContext;
import com.efinancialpal.swagger.rest.config.AccessHiddenSpecFilter;
import com.wordnik.swagger.config.FilterFactory;
import com.wordnik.swagger.jaxrs.json.JacksonJsonProvider;
import com.wordnik.swagger.jersey.config.JerseyJaxrsConfig;
import org.apache.commons.configuration.Configuration;
import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.*;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.ResourceCollection;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pakulov.jersey.protobuf.ProtobufFeature;
import com.efinancialpal.swagger.rest.config.ConfigurationManager;

// @ApplicationPath("resources")
public class EFinancialPalApp extends ResourceConfig {
    private static final Logger logger = LoggerFactory.getLogger(EFinancialPalApp.class);
    
    private static final Configuration configuration = ConfigurationManager.getConfiguration();

    public static final String docBasePath = "/efinancialpal/rest";

    private final Server mJetty = new Server();

    public EFinancialPalApp() {
        super(ProtobufFeature.class);
        
        final String myRestPackage = "com.efinancialpal.swagger.rest";
        final String jacksonPackage = "org.codehaus.jackson.jaxrs";

        final String swaggerJaxrsJsonPackage = "com.wordnik.swagger.jaxrs.json";
        final String swaggerJaxrsListingPackage = "com.wordnik.swagger.jaxrs.listing";

        packages(swaggerJaxrsJsonPackage, swaggerJaxrsListingPackage, jacksonPackage, myRestPackage);

        // enable multipart
        register(MultiPartFeature.class);
        register(JacksonJsonProvider.class);

    }

    public void stop() throws Exception {
        mJetty.stop();
        mJetty.join();
    }

    public void start() throws Exception {
        logger.info("starting efinancialpal application...");
        // This is needed for retrieving Jetty metrics
        // Refer to:
        // http://www.eclipse.org/jetty/documentation/current/jmx-chapter.html
        MBeanContainer mbContainer = new MBeanContainer(
                ManagementFactory.getPlatformMBeanServer());
        mJetty.addEventListener(mbContainer);
        mJetty.addBean(mbContainer);
        mJetty.start();
    }


    public void init() throws Exception {
        /*
        Configuration configuration = ConfigurationManager.getConfiguration();
        ModuleManager.addModule("com.efinancialpal.schedule.SchedulerModule");
        ModuleManager.init();

        // TODO: register mbean
        logger.info("Module manager initialized...");
        */

        /*
        try {
            startExecutors();
        }
        catch (Exception e) {
            logger.error("Failed to start executors, ignoring anyway...", e);
        }
        */

        if (configuration.getBoolean("enable.http", true)) {
            configureHttpServerConnector(mJetty);
        }
        FilterFactory.setFilter(new AccessHiddenSpecFilter());

        // We need to use the handlerCollection because we have more
        // than one handlers
        HandlerCollection handlerCollection = new HandlerCollection();

        // Handler for swagger UI, static handler.
        ResourceHandler swaggerUIResourceHandler = buildSwaggerUIResourceHandler();
        // Add the ResourceHandler to the server.
        handlerCollection.addHandler(swaggerUIResourceHandler);

        ServletContextHandler handler = new ServletContextHandler(mJetty, "/",
                ServletContextHandler.SESSIONS);
        handler.addServlet(new ServletHolder(new ServletContainer(this)),
                docBasePath + "/*");

        /*
        // TODO:
        handler.addServlet(new ServletHolder(
                        new MetricsServlet(MetricRegistryManager.getInstance())),
                "/metrics/*");
    */
        handler.addEventListener(new ServiceContext());
        handlerCollection.addHandler(handler);

        // Handler for swagger api.
        ContextHandler swaggerContextHandler = buildSwaggerContextHandler(
                docBasePath);
        handlerCollection.addHandler(swaggerContextHandler);


        // StatisticsHandler gives us jetty metrics
        StatisticsHandler stats = new StatisticsHandler();
        stats.setHandler(mJetty.getHandler());
        handlerCollection.addHandler(stats);

        /*
        InstrumentedHandler io = new InstrumentedHandler(
                MetricRegistryManager.getInstance());
        io.setHandler(handler);
        handlerCollection.addHandler(io);
        */


        enableAccessLog(handlerCollection);

        mJetty.setHandler(handlerCollection);

    }

    private static ContextHandler buildSwaggerContextHandler(String basePath)
            throws Exception {
        final ServletHolder swaggerServletHolder = new ServletHolder(new JerseyJaxrsConfig());
        swaggerServletHolder.setName("JerseyJaxrsConfig");
        swaggerServletHolder.setInitParameter("api.version", "1.0.0");
        swaggerServletHolder.setInitParameter("swagger.api.basepath", basePath);
        swaggerServletHolder.setInitOrder(1);
        ServletContextHandler swaggerServletContextHandler = new ServletContextHandler(
                ServletContextHandler.SESSIONS);
        swaggerServletContextHandler.setSessionHandler(new SessionHandler());
        swaggerServletContextHandler.setContextPath("/api-docs");
        swaggerServletContextHandler.addServlet(swaggerServletHolder, "/*");
        return swaggerServletContextHandler;
    }

    private static ResourceHandler buildSwaggerUIResourceHandler()
            throws Exception {
        // Create the ResourceHandler. It is the object that will actually
        // handle the request for a given file. It is
        // a Jetty Handler object so it is suitable for chaining with other
        // handlers as you will see in other examples.
        ResourceHandler resourceHandler = new ResourceHandler();
        // Configure the ResourceHandler. Setting the resource base indicates
        // where the files should be served out of.
        // In this example it is the current directory but it can be configured
        // to anything that the jvm has access to.
        resourceHandler.setDirectoriesListed(true);
        resourceHandler.setWelcomeFiles(new String[]{"index.html"});
        ResourceCollection resources;

        /*
        //resources = new ResourceCollection((new String[]{"src/main/webapp/html"}));
        resources = new ResourceCollection(new String[]{EFinancialPalApp.class
                .getClassLoader().getResource("webapp").toURI().toString()
                // configuration.getString("html.dir", "src/main/webapp/html"),
        });

        resourceHandler.setBaseResource(resources);
        */

        return resourceHandler;

    }

    private static void enableAccessLog(HandlerCollection handlerCollection) throws IOException {
        Path dir = Paths.get("/tmp/efinancialpal");
        Files.createDirectories(dir);
        NCSARequestLog requestLog = new NCSARequestLog(dir.toString() + "/jetty-access-yyyy_mm_dd.request.log");
        requestLog.setExtended(true);
        requestLog.setRetainDays(14);
        requestLog.setLogTimeZone("GMT+8");
        RequestLogHandler requestLogHandler = new RequestLogHandler();
        requestLogHandler.setRequestLog(requestLog);
        requestLog.setAppend(true);

        handlerCollection.addHandler(requestLogHandler);
    }

    private void configureHttpServerConnector(Server server)
            throws IOException {
        HttpConfiguration http_config = new HttpConfiguration();

        ServerConnector http = new ServerConnector(server,
                new HttpConnectionFactory(http_config));

        int port = configuration.getInt("jetty.http.port", 8888);
        http.setPort(port);
        server.addConnector(http);
    }
    
    public void startExecutors() throws Exception {
        this.startJobs();
    }

    public void startJobs() throws Exception {
        String groupName = "stockDownload";
        JobDetail job = JobBuilder.newJob(StockQuoteDownloadJob.class)
                .withIdentity("StartQuoteDownloadJob", groupName).build();
        int checkInterval = 24 * 60 * 60; // a day
        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("StartEndStockQutoesCheckTrigger", groupName)
                .startNow()
                .withSchedule(
                        SimpleScheduleBuilder.simpleSchedule()
                                .withIntervalInSeconds(checkInterval).repeatForever()
                )
                .build();
        SchedulerModule.getInstance().scheduleJob(job, trigger );


        JobDetail job2 = JobBuilder.newJob(StockStatsDownloadJob.class)
                .withIdentity("StartStatsDownloadJob", groupName).build();
        checkInterval = 24 * 60 * 60 * 2; // two days
        trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("StartEndStockStatsCheckTrigger", groupName)
                .startNow()
                .withSchedule(
                        SimpleScheduleBuilder.simpleSchedule()
                                .withIntervalInSeconds(checkInterval).repeatForever()
                )
                .build();
        SchedulerModule.getInstance().scheduleJob(job2, trigger );
    }

    public static void main(String[] args) throws Exception {
        try {
            EFinancialPalApp app = new EFinancialPalApp();
            app.init();
            app.start();
        } catch (Exception e) {
            logger.error("failed to start efinancialpal", e);
            Runtime.getRuntime().exit(-1);
        }
    }
}
