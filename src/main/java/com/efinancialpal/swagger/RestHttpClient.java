package com.efinancialpal.swagger;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class RestHttpClient {
    private static final Logger logger = LoggerFactory.getLogger(RestHttpClient.class);

    private static long DEFAULT_TIMEOUT = 90000;
    private static volatile RestHttpClient instance = null;
    private static volatile boolean initialized = false;
    private static final Object initLock = new Object();
    
    private OkHttpClient client;
    
    public static RestHttpClient getInstance() {
        if (initialized) {
            return instance;
        }
        
        return getInstance(DEFAULT_TIMEOUT);
    }
    
    public static RestHttpClient getInstance(long timeout) {
        if (initialized) {
            return instance;
        }
        
        synchronized (initLock) {
            if (!initialized) {
                instance = new RestHttpClient(timeout);
                initialized = true;
            }
            return instance;
        }
    }
    
    private RestHttpClient(long timeout) {
        this.client = new OkHttpClient();
        this.client.setReadTimeout(timeout, TimeUnit.MILLISECONDS);
    }
    
    public String doGet(String baseUrl, String path, Map<String, String> queryParams, Map<String, String> headers) throws IOException {
        int rc = 200;
        String bodyStr = "";
        long ts1 = System.currentTimeMillis();
        
        // compose url
        String url = assureNoEndSlash(baseUrl);
        if (path != null) {
            url += assureStartSlash(path);
        }
        
        if (queryParams != null && !queryParams.isEmpty()) {
            url += "?";
            Iterator<Map.Entry<String, String>> iter = queryParams.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, String> entry = iter.next();
                url += String.format("%s=%s", URLEncoder.encode(entry.getKey(), "UTF-8"), URLEncoder.encode(entry.getValue(), "UTF-8"));
                if (iter.hasNext()) {
                    url += "&";
                }
            }
        }
        
        logger.info("composed request for url {}", url);
        
        // build request
        Request.Builder reqBuilder = new Request.Builder();
        reqBuilder = reqBuilder.url(url);
        if (headers != null && !headers.isEmpty()) {
           for (Map.Entry<String, String> header : headers.entrySet()) {
               reqBuilder = reqBuilder.header(header.getKey(), header.getValue());
           }
        }
        
        Request req = reqBuilder.build();
        
        logger.info("sending url request...");
        
        Response response = this.client.newCall(req).execute();
        
        rc = response.code();
        bodyStr = response.body().string();
        
        logger.info("duration for the call: {}", System.currentTimeMillis() - ts1);
       
        return bodyStr;
    }
    
    private String assureNoEndSlash(String input) {
        return (input.endsWith("/")) ? input.substring(0,  input.length() - 1) : input;
    }
    
    private String assureStartSlash(String input){
        return (input.startsWith("/") ? input : ("/" + input));
    }
}
