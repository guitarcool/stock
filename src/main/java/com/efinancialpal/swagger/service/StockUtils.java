package com.efinancialpal.swagger.service;

import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.backtest.BackTestResult;
import com.efinancialpal.strategy.utils.StrategyConstants;
import com.efinancialpal.swagger.model.Ticker;
import com.google.common.collect.Multimap;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.tools.reflect.Eval;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by haiyanluo on 10/20/16.
 */
public class StockUtils {
    private static final Logger logger = LoggerFactory.getLogger(StockUtils.class);

    public static void simplePrint(Multimap<String, EvaluationContext> map) {
        Map<String, List<String>> outMap = new HashMap<>();
        for (EvaluationContext evaluationContext : map.values()) {
            for (String strategy : evaluationContext.getStrategies()) {
                if (!outMap.containsKey(strategy)) {
                    outMap.put(strategy, new ArrayList<>());
                }
                outMap.get(strategy).add(evaluationContext.getTicker());
            }
        }
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, List<String>> entry : outMap.entrySet()) {
            // logger.info(entry.getKey());
            builder.append(entry.getKey()).append(":").append("\n");
            for (String symbol : entry.getValue()) {
                builder.append(symbol).append(",");
            }
            builder.append("\n");
            //logger.info(builder.toString());
        }

        logger.info(builder.toString());
        // use current date as file name
        String file = new SimpleDateFormat("yyyyMMdd'.txt'").format(new Date());
        try(  PrintWriter out = new PrintWriter(file)  ){
            out.println( builder.toString());
        }
        catch (Exception e) {
            logger.error("Failed to store result to file {}", file);
        }
    }

    public static void printResults(Map<String, EvaluationContext> finalResult) {
        logger.info("Here are the found results, in total {}", finalResult.size());
        Map<String, List<Ticker>> sector2TicketMapLong = new HashMap<>();
        Map<String, List<Ticker>> sector2TicketMapShort = new HashMap<>();
        for (EvaluationContext evaluationContext: finalResult.values()) {

            Ticker ticker = Ticker.findFirst("symbol = ?", evaluationContext.getTicker());
            // logger.info("found ticker {}, name: {}, sector: {}, industry: {}", ticker.getSymbol(), ticker.getName(), ticker.getSector(), ticker.getIndustry());
            String sector = ticker.getSector();
            if (StrategyConstants.LONG.equals(evaluationContext.getLongOrShort())) {
                if (!sector2TicketMapLong.containsKey(sector)) {
                    sector2TicketMapLong.put(sector, new ArrayList<Ticker>());
                }
                sector2TicketMapLong.get(sector).add(ticker);
            } else {
                if (!sector2TicketMapShort.containsKey(sector)) {
                    sector2TicketMapShort.put(sector, new ArrayList<Ticker>());
                }
                sector2TicketMapShort.get(sector).add(ticker);
            }
        }

        logger.info("");
        logger.info("===========LONG CANDIDATES===========");
        logger.info("===========LONG CANDIDATES===========:");
        logger.info("");
        for (Map.Entry<String, List<Ticker>> entry : sector2TicketMapLong.entrySet()) {
            logger.info("");
            logger.info("sector: {} has {} stocks", entry.getKey(), entry.getValue().size());
            logger.info("============================");
            for (Ticker ticker : entry.getValue()) {
                logger.info("symbol: {}, strategy: {}, industry: {}, name: {} ", ticker.getSymbol(), finalResult.get(ticker.getSymbol()).getStrategies(), ticker.getIndustry(), ticker.getName());
            }
        }

        logger.info("");
        logger.info("===========SHORT CANDIDATES===========");
        logger.info("===========SHORT CANDIDATES===========:");
        logger.info("");
        for (Map.Entry<String, List<Ticker>> entry : sector2TicketMapShort.entrySet()) {
            logger.info("");
            logger.info("sector: {} has {} stocks", entry.getKey(), entry.getValue().size());
            logger.info("============================");
            for (Ticker ticker : entry.getValue()) {
                logger.info("symbol: {}, strategy: {}, industry: {}, name: {}", ticker.getSymbol(), finalResult.get(ticker.getSymbol()).getStrategies(), ticker.getIndustry(), ticker.getName());
            }
        }

    }

    // TODO: this needs to update to reflect the LONG/SHORT candidates
    public static String dumpResults(Map<String, EvaluationContext> finalResult) {
        Map<String, List<String>> sector2TickerMap = new HashMap<>();
        for (String symbol : finalResult.keySet()) {
            Ticker ticker = Ticker.findFirst("symbol = ?", symbol);
            // logger.info("found ticker {}, name: {}, sector: {}, industry: {}", ticker.getSymbol(), ticker.getName(), ticker.getSector(), ticker.getIndustry());
            String sector = ticker.getSector();
            if (!sector2TickerMap.containsKey(sector)) {
                sector2TickerMap.put(sector, new ArrayList<String>());
            }
            sector2TickerMap.get(sector).add(symbol);
        }

        JSONObject json = new JSONObject();
        json.putAll(sector2TickerMap);

        String str = json.toString();

        // use current date as file name
        String file = new SimpleDateFormat("yyyyMMdd'.txt'").format(new Date());

        try(  PrintWriter out = new PrintWriter(file)  ){
            out.println( str);
            // output finviz lookup url
            out.println();
            StringBuilder longBuilder = new StringBuilder();
            StringBuilder shortBuilder = new StringBuilder();
            for (String symbol : finalResult.keySet()) {
                if (finalResult.get(symbol).getLongOrShort().equals(StrategyConstants.LONG)) {
                    longBuilder.append(symbol).append(",");
                } else {
                    shortBuilder.append(symbol).append(",");
                }
            }
            out.println("LONG:");
            out.println(longBuilder.toString());
            out.println("SHORT:");
            out.println(shortBuilder.toString());
        }
        catch (Exception e) {
            logger.error("Failed to store result to file {}", file);
        }
        return str;
    }

    public static void printResults(List<BackTestResult> results) {
        for (BackTestResult backTestResult : results) {
            logger.info(backTestResult.toString());
        }
    }

    public static Set<String> intersection(Set<String> set1, Set<String> set2) {
        Set<String> result = new HashSet<>();
        for (String ticker : set2) {
            if (set1.contains(ticker)) {
                result.add(ticker);
            }
        }
        return result;
    }

}
