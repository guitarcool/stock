package com.efinancialpal.swagger.service;

import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Tick;
import org.joda.time.DateTime;

/**
 * Created by haiyanluo on 10/16/16.
 */
public class MyTick extends Tick {

    private Decimal amount = Decimal.ZERO;

    private int trades = 0;

    public MyTick(double closePrice) {
        this(new DateTime(), closePrice);
    }

    public MyTick(double closePrice, double volume) {
        super(new DateTime(), 0, 0, 0, closePrice, volume);
    }

    public MyTick(DateTime endTime, double closePrice) {
        super(endTime, 0, 0, 0, closePrice, 0);
    }

    public MyTick(double openPrice, double closePrice, double maxPrice, double minPrice) {
        super(new DateTime(), openPrice, maxPrice, minPrice, closePrice, 1);
    }

    public MyTick(double openPrice, double closePrice, double maxPrice, double minPrice, double volume) {
        super(new DateTime(), openPrice, maxPrice, minPrice, closePrice, volume);
    }

    public MyTick(DateTime endTime, double openPrice, double closePrice, double maxPrice, double minPrice, double amount, double volume, int trades) {
        super(endTime, openPrice, maxPrice, minPrice, closePrice, volume);
        this.amount = Decimal.valueOf(amount);
        this.trades = trades;
    }

    @Override
    public Decimal getAmount() {
        return amount;
    }

    @Override
    public int getTrades() {
        return trades;
    }
}
