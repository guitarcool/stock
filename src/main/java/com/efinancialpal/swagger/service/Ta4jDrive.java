package com.efinancialpal.swagger.service;

import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import eu.verdelhan.ta4j.indicators.trackers.MACDIndicator;
import org.joda.time.DateTime;
import yahoofinance.histquotes.HistoricalQuote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haiyanluo on 10/16/16.
 */

// test program for figure out how to use ta4j
public class Ta4jDrive {
    public static void main(String[] args) {
        HistoricalQuote quote = new HistoricalQuote();
        List<Tick> tickList = new ArrayList<>();
        DateTime endTime = DateTime.now();

        Tick tick = new Tick(endTime, quote.getOpen().doubleValue(), quote.getHigh().doubleValue(), quote.getLow().doubleValue(), quote.getAdjClose().doubleValue(), quote.getVolume().doubleValue());

        TimeSeries timeSeries = new TimeSeries(tickList);
        EMAIndicator emaIndicator12 = new EMAIndicator(new ClosePriceIndicator(timeSeries), 12);
        EMAIndicator emaIndicator26 = new EMAIndicator(new ClosePriceIndicator(timeSeries), 26);

        ClosePriceIndicator closePriceIndicator = new ClosePriceIndicator(timeSeries);
        MACDIndicator macd = new MACDIndicator(closePriceIndicator, 9, 26);

    }
}
