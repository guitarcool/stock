package com.efinancialpal.swagger.service;

import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haiyanluo on 10/16/16.
 */
public class MyTimeSeries extends TimeSeries {
    public MyTimeSeries(double... data) {
        super(doublesToTicks(data));
    }

    public MyTimeSeries(List<Tick> ticks) {
        super(ticks);
    }

    public MyTimeSeries(double[] data, DateTime[] times) {
        super(doublesAndTimesToTicks(data, times));
    }

    public MyTimeSeries(DateTime... dates) {
        super(timesToTicks(dates));
    }

    public MyTimeSeries() {
        super(arbitraryTicks());
    }
    private static List<Tick> doublesToTicks(double... data) {
        ArrayList<Tick> ticks = new ArrayList<Tick>();
        for (int i = 0; i < data.length; i++) {
            ticks.add(new MyTick(new DateTime().withMillisOfSecond(i), data[i]));
        }
        return ticks;
    }

    private static List<Tick> doublesAndTimesToTicks(double[] data, DateTime[] times) {
        if (data.length != times.length) {
            throw new IllegalArgumentException();
        }
        ArrayList<Tick> ticks = new ArrayList<Tick>();
        for (int i = 0; i < data.length; i++) {
            ticks.add(new MyTick(times[i], data[i]));
        }
        return ticks;
    }

    private static List<Tick> timesToTicks(DateTime... dates) {
        ArrayList<Tick> ticks = new ArrayList<Tick>();
        int i = 1;
        for (DateTime date : dates) {
            ticks.add(new MyTick(date, i++));
        }
        return ticks;
    }

    private static List<Tick> arbitraryTicks() {
        ArrayList<Tick> ticks = new ArrayList<Tick>();
        for (double i = 0d; i < 10; i++) {
            ticks.add(new MyTick(new DateTime(0), i, i + 1, i + 2, i + 3, i + 4, i + 5, (int) (i + 6)));
        }
        return ticks;
    }
}
