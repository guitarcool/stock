package com.efinancialpal.swagger.service;

import downloaders.alphavantage.AlphaVantageDownloader;
import com.efinancialpal.couchbase.CouchbaseKeyValueCache;
import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.StrategyCreator;
import com.efinancialpal.strategy.backtest.BackTestResult;
import com.efinancialpal.strategy.backtest.SimpleBackTestResult;
import com.efinancialpal.utils.ticker.TickerLoader;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import downloaders.morningstar.MorningstarDownloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

import javax.crypto.Mac;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by haiyanluo on 9/20/16.
 */
public class StockMain {
    private static final Logger logger = LoggerFactory.getLogger(StockMain.class);
    private static final float PRICE_BOOK_THRESHOLD = 5.0f;
    private static final float PEG_THRESHOLD = 5.0f;
    private static final float PRICE_SALES_THRESHOLD = 5.0f;
    private static final float FLOOR_MARKET_CAP = 100; // 100M
    private static final float FLOOR_SHARE_PRICE = 22f;
    private static int numOfRecordsToDownload = 100;
    private static final Interval dailyOrWeekly = Interval.DAILY;

    private static final String STOCK_STATS_REFRESHED_TIMESTAMP_KEY = "stock_stats_refreshed_timestamp";
    private static final Long ONE_DAY_IN_MILLISECOND = 24 * 60 * 60 * 1000L;
    private static final Long TICKER_UPDATE_INTERVAL_IN_DAYS = 7L;

    private static final boolean backTestEnabled = false;


    public static boolean needToRefreshStockStats() {
        try {
            String lastStockStatsUpdatedTimestamp = CouchbaseKeyValueCache.getInstance().get(STOCK_STATS_REFRESHED_TIMESTAMP_KEY);
            if (lastStockStatsUpdatedTimestamp != null) {
                final Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(Long.valueOf(lastStockStatsUpdatedTimestamp));
                final String timeString =
                        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(cal.getTime());
                logger.info("Stock stats were updated last time at {}", timeString);
            } else {
                logger.info("Stock stats were not updated before, first first this is being rum?");
            }
            if (lastStockStatsUpdatedTimestamp == null
                    || (System.currentTimeMillis() - Long.valueOf(lastStockStatsUpdatedTimestamp) > TICKER_UPDATE_INTERVAL_IN_DAYS * ONE_DAY_IN_MILLISECOND)) {
                return true;
            }
        } catch (Exception e) {
            logger.error("Failed to read and refresh stock stats", e);
            throw  e;
        }
        return false;
    }

    public static void main(String[] args) throws Exception {
        Multimap<String, EvaluationContext> result = ArrayListMultimap.create(); // TODO, multiple strategies could choose the same stock, ideally we want to keep that information
        List<BackTestResult> backTestResults = new ArrayList<>();

//        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
        try {
            // TODO: not usable anymore, we have to switch to Stooq for now because yahoo API is not usable anymore
            // List<Ticker> candidateList = TickerLoader.getInstance().getCandidateStocks(FLOOR_MARKET_CAP, FLOOR_SHARE_PRICE);
            List<String> candidateList = TickerLoader.getInstance().loadTickerSymbolsFromFiles();
         //   StooqDownloader.getFileNames(candidateList, Paths.get(StooqDownloader.BASE_PATH));

            /*
            float minPrice = 4;
            float maxPrice = 30;
            List<Ticker> candidateList = TickerLoader.getInstance().getLowPriceCandidateStocks(minPrice, maxPrice);
            */
            logger.info("candidate list size: {}", candidateList.size());

            /* Using the Public API (without authentication), you are limited to 2,000 requests
            per hour per IP (or up to a total of 48,000 requests a day).
            Using the Private API (with OAuth authentication using an API key),
            you are limited to 20,000 requests per hour per IP and you are limited to 100,000 requests per day per API Key.
            */
            /* From 11/1/2017 Yahoo stopped the API, need to find a replacement
            if (needToRefreshStockStats() || ConfigurationManager.getConfiguration().getBoolean("needToRefreshStockStats", false)) {
                logger.info("refreshing stock stats...");
                db.exec("delete from stock_stats");
                for (Ticker ticker : candidateList) {
                    YahooFinanceDownloader.getInstance().downloadAndStoreStockStats(ticker.getSymbol());
                }
                CouchbaseKeyValueCache.getInstance().set(STOCK_STATS_REFRESHED_TIMESTAMP_KEY, String.valueOf(System.currentTimeMillis()));
            } */

            List<SimpleBackTestResult> simpleBackTestResults = new ArrayList<>();
            Strategy strategy = StrategyCreator.createStrategies();

            int index = 0;
            for (String ticker : candidateList) {

                if (index++ % 10 == 0) {
                    logger.info("Processing {}/{}", index, candidateList.size());
                }

               // String ticker = StooqDownloader.getSymbolFromFullPath(path);
                // String ticker = stock.getSymbol();
                // ticker = "BCOM";
                // logger.info("Processing {}, {} of {}", ticker, index, candidateList.size());
                // List<HistoricalQuote> quotes = YahooFinanceDownloader.getInstance().downloadQuotes(ticker, dailyOrWeekly);
                // List<HistoricalQuote> quotes = StockRecordData.getInstance().get(ticker);
                //List<HistoricalQuote> quotes = StooqDownloader.getHistoricQuotes(path);
                // List<HistoricalQuote> quotes = AlphaVantageDownloader.getInstance().download(ticker);
                List<HistoricalQuote> quotes = MorningstarDownloader.getInstance().download(ticker);

                /*
                if (quotes.size() < numOfRecordsToDownload) {
                    logger.info("Skipping ticker {} since it doesnt have enough records", stock);
                    continue;
                }
                */


                if (shouldFilter(quotes)) {
                    continue;
                }

                EvaluationContext evaluationContext = new EvaluationContext();

                if (strategy.meet(ticker, quotes, evaluationContext)) {
                    result.put(ticker, evaluationContext);
                    logger.info("Found: {}", ticker);
                }

                // Thread.sleep(20000); // alphavantage is QPS limit is 5 per minute
                /*
                BackTestHammerStrategy backTestHammerStrategy = new BackTestHammerStrategy();
                List<SimpleBackTestResult> cur = backTestHammerStrategy.run(ticker, quotes, evaluationContext);
                if (!cur.isEmpty()) {
                    System.out.println(cur);
                }
                */


                /*
                if (backTestEnabled) {
                    backTestResults.addAll(strategy.test(ticker, quotes, evaluationContext));
                }
                */

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // print out results;
        /* TODO: since 11/1/2017, YAHOO stopped the API need to find a replacement
        try (DB db = ActiveJDBCUtil.getDB(Constants.STOCK)) {
            StockUtils.printResults(result);
            StockUtils.dumpResults(result);
            StockUtils.printResults(backTestResults);
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
        StockUtils.simplePrint(result);

    }

    public static boolean shouldFilter(List<HistoricalQuote> quotes) {
        // over the past 5 days, price doesnt change, filtered out
        // probably an acquisition or something
        if (quotes.size() < 20) {
            return true;
        }

         if (quotes.get(0).getVolume().doubleValue() < 80000
                //|| quotes.get(0).getVolume().doubleValue() * quotes.get(0).getClose().doubleValue() < 5000000
                || quotes.get(0).getClose().doubleValue() < 5) {
             return true;
         }

        double high = quotes.get(0).getHigh().doubleValue();
        double low = quotes.get(0).getLow().doubleValue();
        for (int i = 0; i < 5; ++i) {
            if (quotes.get(i).getLow().doubleValue() < low) {
                low = quotes.get(i).getLow().doubleValue();
            }
            if (quotes.get(i).getHigh().doubleValue() > high) {
                high = quotes.get(i).getHigh().doubleValue();
            }
        }
        if ((high - low) / low < 0.005) {
            return true;
        }
        return false;
    }


}
