package com.efinancialpal.swagger.service;

import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Indicator;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import eu.verdelhan.ta4j.indicators.trackers.MACDIndicator;
import org.joda.time.DateTime;

import java.util.List;


/**
 * Created by haiyanluo on 10/16/16.
 */

/*
    MACD: (12-day EMA - 26-day EMA)

    Signal Line: 9-day EMA of MACD

    MACD Histogram: MACD - Signal Line
    */

public class MyMacdIndicator extends MACDIndicator {
    private EMAIndicator emaIndicator9 = null;

    public MyMacdIndicator(Indicator<Decimal> indicator, int shortTimeFrame, int longTimeFrame, List<DateTime> dateTimes) {
        super(indicator, shortTimeFrame, longTimeFrame);

        int size = dateTimes.size();
        double[] data = new double[size];
        for (int i = 0; i < size; ++i) {
            data[i] = calculate(i).toDouble();
        }
        TimeSeries macdTimeSeries = new MyTimeSeries(data, dateTimes.toArray(new DateTime[dateTimes.size()]));
        emaIndicator9 = new EMAIndicator(new ClosePriceIndicator(macdTimeSeries), 9);
    }

    public Double getHistogram(int index) {
        return getMACD(index) - getSignal(index);
    }

    public Double getSignal(int index) {
       return emaIndicator9.getValue(index).toDouble();
    }

    public Double getMACD(int index) {
        return calculate(index).toDouble();
    }


    public static void main(String[] args) {

    }



}
