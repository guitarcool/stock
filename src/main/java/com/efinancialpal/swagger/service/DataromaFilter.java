package com.efinancialpal.swagger.service;

import com.efinancialpal.service.YahooFinanceDownloader;
import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.StrategyCreator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by haiyanluo on 3/4/17.
 */
public class DataromaFilter {
    private static final Logger logger = LoggerFactory.getLogger(StockMain.class);
    private static final int WEEKS_TO_DOWNLOAD = 100;
    private static final String BASE_PATH = "";

    public static void main(String[] args) throws Exception {
        String fileName = getFileName();
        List<String> tickers = loadCandidates(fileName);
        List<String> results = new ArrayList<>();
        Strategy strategy = StrategyCreator.createStrategies();
        for (String symbol : tickers) {
            if (StringUtils.isNotBlank(symbol)) {
                List<HistoricalQuote> quotes = YahooFinanceDownloader.getInstance().downloadQuotes(symbol, Interval.WEEKLY);
                EvaluationContext evaluationContext = new EvaluationContext();
                if (strategy.meet(symbol, quotes, evaluationContext)) {
                    results.add(symbol);
                }
            }
        }
        System.out.println(results);
        writeToFile(results);
    }

    private static String getFileName() {
        return BASE_PATH + getDateStr() + "_insider.txt";
    }

    private static String getDateStr() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String dateStr = dateFormat.format(cal.getTime());
        return dateStr;
    }

    private static List<String> loadCandidates(String fileName) throws IOException {
        List<String> lines = FileUtils.readLines(new File(fileName));
        return lines;
    }

    private static void writeToFile(List<String> tickers) throws Exception {
        StringBuilder builder = new StringBuilder();
        for (String symbol : tickers) {
            builder.append(symbol);
            builder.append(" ");
        }
        FileUtils.writeStringToFile(new File(getDateStr() + ".txt"), builder.toString());
    }
}
