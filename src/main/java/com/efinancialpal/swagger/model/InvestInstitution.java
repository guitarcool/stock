package com.efinancialpal.swagger.model;

/**
 * Created by haiyanluo on 10/22/16.
 */

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.DbName;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

import java.math.BigDecimal;
import java.util.Date;

@DbName("stock")
@Table("invest_institution")
@IdName("id")
public class InvestInstitution extends Model {
    public void setId(Long id) {
        set("id", id);
    }

    public Long getId() {
        if (get("id") != null) {
            return getLong("id");
        }
        return null;
    }

    public void setName(String name) {
        set("name", name);
    }

    public String getName() {
        if (get("name") != null) {
            return getString("name");
        }
        return null;
    }

    public void setPortfolioValue(BigDecimal portfolioValue) {
        set("portfolio_value", portfolioValue);
    }

    public BigDecimal getPortfolioValue() {
        if (get("portfolio_value") != null) {
            return getBigDecimal("portfolio_value");
        }
        return null;
    }

    public void setNoOfStocks(Long noOfStocks) {
        set("no_of_stocks", noOfStocks);
    }

    public Long getNoOfStocks() {
        if (get("num_of_stocks") != null) {
            return getLong("no_of_stocks");
        }
        return null;
    }

    public void setActive(Boolean active) {
        set("active", active);
    }

    public Boolean getActive() {
        if (get("active") != null) {
            return getBoolean("active");
        }
        return false;
    }

    public void setVersion(Long version) {
        set("version", version);
    }

    public Long getVersion() {
        if (get("version") != null) {
            return getLong("version");
        }
        return 0L;
    }

    public void setVersionUpdatedAt(Date versionUpdatedAt) {
        set("version_updated_at", versionUpdatedAt);
    }

    public Date getVersionUpdatedAt() {
        if (get("version_updated_at") != null) {
            return getDate("version_updated_at");
        }
        return null;
    }


}

