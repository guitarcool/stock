package com.efinancialpal.swagger.model;

import java.math.BigDecimal;
import java.util.Date;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.DbName;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

import static com.efinancialpal.swagger.rest.Constants.EFINANCIALPAL_DB_NAME;

@DbName(EFINANCIALPAL_DB_NAME)
@Table("daily_transaction_data")
@IdName("id")
public class DailyTransactionData extends Model {
    public void setId(Long id) {
        set("id", id);
    }

    public void setTicker(String ticker) {
        set("ticker", ticker);
    }

    public void setDate(Date date) {
        set("date", date);
    }

    public void setOpen(BigDecimal open) {
        set("open", open);
    }

    public void setClose(BigDecimal close) {
        set("close", close);
    }

    public void setAdjClose(BigDecimal adjClose) {
        set("adj_close", adjClose);
    }

    public void setHigh(BigDecimal high) {
        set("high", high);
    }

    public void setLow(BigDecimal low) {
        set("low", low);
    }

    public void setVolume(Long volume) {
        set("volume", volume);
    }

    public void setAuthor(String author) {
        set("author", author);
    }

    public String getAuthor() {
        if (get("author") != null) {
            return getString("author");
        }
        return null;
    }

    public Long getId() {
        if (get("id") != null) {
            return getLong("id");
        }
        return null;
    }

    public String getTicker() {
        if (get("ticker") != null) {
            return getString("ticker");
        }
        return null;
    }

    public Date getDate() {
        if (get("date") != null) {
            return getDate("date");
        }
        return null;
    }

    public BigDecimal getHigh() {
        if (get("high") != null) {
            return getBigDecimal("high");
        }
        return new BigDecimal(0);
    }

    public BigDecimal getLow() {
        if (get("low") != null) {
            return getBigDecimal("low");
        }
        return new BigDecimal(0);
    }


    public BigDecimal getOpen() {
        if (get("open") != null) {
            return getBigDecimal("open");
        }
        return new BigDecimal(0);
    }

    public BigDecimal getClose() {
        if (get("close") != null) {
            return getBigDecimal("close");
        }
        return new BigDecimal(0);
    }


    public Long getLong() {
        if (get("volume") != null) {
            return getLong("volume");
        }
        return 0L;
    }


    public void setActive(Boolean active) {
        set("active", active);
    }

    public Boolean getActive() {
        if (get("active") != null) {
            return getBoolean("active");
        }
        return false;
    }

    public void setVersion(Long version) {
        set("version", version);
    }

    public Long getVersion() {
        if (get("version") != null) {
            return getLong("version");
        }
        return 0L;
    }

    public void setVersionUpdatedAt(Date versionUpdatedAt) {
        set("version_updated_at", versionUpdatedAt);
    }

    public Date getVersionUpdatedAt() {
        if (get("version_updated_at") != null) {
            return getDate("version_updated_at");
        }
        return null;
    }

}
