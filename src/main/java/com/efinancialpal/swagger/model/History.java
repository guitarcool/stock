package com.efinancialpal.swagger.model;

import java.util.Date;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.DbName;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

import static com.efinancialpal.swagger.rest.Constants.STOCK;

@DbName(STOCK)
@Table("history")
@IdName("id")
public class History extends Model {
    public void setId(Long id) {
        set("id", id);
    }

    public void setPersonId(String personId) {
        set("person_id", personId);
    }

    public void setInOut(String inOut) {
        set("in_out", inOut);
    }

    public void setTimestamp(Date timestamp) {
        set("timestamp", timestamp);
    }

    public Long getId() {
        if (get("id") != null) {
            return getLong("id");
        }
        return null;
    }

    public String getPersonId() {
        if (get("person_id") != null) {
            return getString("person_id");
        }
        return null;
    }

    public String getInOut() {
        if (get("in_out") != null) {
            return getString("in_out");
        }
        return null;
    }

    public Date getTimestamp() {
        if (get("timestamp") != null) {
            return getDate("timestamp");
        }
        return null;
    }

    public void setActive(Boolean active) {
        set("active", active);
    }

    public Boolean getActive() {
        if (get("active") != null) {
            return getBoolean("active");
        }
        return false;
    }


    public void setVersionUpdatedAt(Date versionUpdatedAt) {
        set("version_updated_at", versionUpdatedAt);
    }

    public Date getVersionUpdatedAt() {
        if (get("version_updated_at") != null) {
            return getDate("version_updated_at");
        }
        return null;
    }


}
