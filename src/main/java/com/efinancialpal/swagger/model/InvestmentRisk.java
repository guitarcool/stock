package com.efinancialpal.swagger.model;

import java.util.Date;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.DbName;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

import static com.efinancialpal.swagger.rest.Constants.EFINANCIALPAL_DB_NAME;

@DbName(EFINANCIALPAL_DB_NAME)
@Table("investment_risk")
@IdName("id")
public class InvestmentRisk extends Model {
    public void setId(Long id) {
        set("id", id);
    }

    public void setTicker(String ticker) {
        set("ticker", ticker);
    }

    public void setRiskCategory(String cat) {
        set("risk_category", cat);
    }

    public void setRisk(String risk) {
        set("risk", risk);
    }

    public void setAuthor(String author) {
        set("author", author);
    }

    public String getAuthor() {
        if (get("author") != null) {
            return getString("author");
        }
        return null;
    }

    public Long getId() {
        if (get("id") != null) {
            return getLong("id");
        }
        return null;
    }

    public String getRiskCategory() {
        if (get("risk_category") != null) {
            return getString("risk_category");
        }
        return null;
    }

    public String getRisk() {
        if (get("risk") != null) {
            return getString("risk");
        }
        return null;
    }

    public String getTicker() {
        if (get("ticker") != null) {
            return getString("ticker");
        }
        return null;
    }

    public void setActive(Boolean active) {
        set("active", active);
    }

    public Boolean getActive() {
        if (get("active") != null) {
            return getBoolean("active");
        }
        return false;
    }

    public void setVersion(Long version) {
        set("version", version);
    }

    public Long getVersion() {
        if (get("version") != null) {
            return getLong("version");
        }
        return 0L;
    }

    public void setVersionUpdatedAt(Date versionUpdatedAt) {
        set("version_updated_at", versionUpdatedAt);
    }

    public Date getVersionUpdatedAt() {
        if (get("version_updated_at") != null) {
            return getDate("version_updated_at");
        }
        return null;
    }


}
