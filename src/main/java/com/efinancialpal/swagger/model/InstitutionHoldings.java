package com.efinancialpal.swagger.model;

/**
 * Created by haiyanluo on 10/22/16.
 */

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.DbName;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

import java.util.Date;

@DbName("stock")
@Table("institution_holdings")
@IdName("id")
public class InstitutionHoldings extends Model {
    public void setId(Long id) {
        set("id", id);
    }

    public Long getId() {
        if (get("id") != null) {
            return getLong("id");
        }
        return null;
    }

    public void setTicker(String ticker) {
        set("ticker", ticker);
    }

    public Long getInstitutionId() {
        if (get("institution_id") != null) {
            return getLong("institution_id");
        }
        return null;
    }

    public void setInstitutionId(Long institutionId) {
        set("institution_id", institutionId);
    }

    public String getTicker() {
        if (get("ticker") != null) {
            return getString("ticker");
        }
        return null;
    }

    public void setStockName(String stockName) {
        set("stock_name", stockName);
    }

    public String getStockName() {
        if (get("stock_name") != null) {
            return getString("stock_name");
        }
        return null;
    }


    public void setPercentage(Float percentage) {
        set("percentage", percentage);
    }

    public Float getPercentage() {
        if (get("percentage") != null) {
            return getFloat("percentage");
        }
        return null;
    }

    public void setShares(Long shares) {
        set("shares", shares);
    }

    public Long getShares() {
        if (get("shares") != null) {
            return getLong("shares");
        }
        return null;
    }

    public void setRecentActivity(String recentActivity) {
        set("recent_activity", recentActivity);
    }

    public String getRecentActivity() {
        if (get("recent_activity") != null) {
            return getString("recent_activity");
        }
        return null;
    }

    public void setActivityPercentage(Float activityPercentage) {
        set("activity_percentage", activityPercentage);
    }

    public Float getActivityPercentage() {
        if (get("activity_percentage") != null) {
            return getFloat("activity_percentage");
        }
        return null;
    }

    public void setReportedPrice(Float reportedPrice) {
        set("reported_price", reportedPrice);
    }

    public Float getReportedPrice() {
        if (get("reported_price") != null) {
            return getFloat("reported_price");
        }
        return null;
    }


    public void setActive(Boolean active) {
        set("active", active);
    }

    public Boolean getActive() {
        if (get("active") != null) {
            return getBoolean("active");
        }
        return false;
    }

    public void setVersion(Long version) {
        set("version", version);
    }

    public Long getVersion() {
        if (get("version") != null) {
            return getLong("version");
        }
        return 0L;
    }

    public void setVersionUpdatedAt(Date versionUpdatedAt) {
        set("version_updated_at", versionUpdatedAt);
    }

    public Date getVersionUpdatedAt() {
        if (get("version_updated_at") != null) {
            return getDate("version_updated_at");
        }
        return null;
    }


}


