package com.efinancialpal.swagger.model;


import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.DbName;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

import java.util.Date;

/**
 * Created by haiyanluo on 9/25/16.
 */
@DbName("stock")
@Table("ticker")
@IdName("id")
public class Ticker extends Model {
    public void setId(Long id) {
        set("id", id);
    }
    public Long getId() {
        if (get("id") != null) {
            return getLong("id");
        }
        return null;
    }
    public void setSymbol(String symbol) {
        set("symbol", symbol);
    }
    public String getSymbol() {
        if (get("symbol") != null) {
            return getString("symbol");
        }
        return null;
    }
    public void setName(String name) {
        set("name", name);
    }
    public String getName() {
        if (get("name") != null) {
            return getString("name");
        }
        return null;
    }
    public void setLastSale(Float lastSale) {
        set("last_sale", lastSale);
    }
    public Float getLastSale() {
        if (get("last_sale") != null) {
            return getFloat("last_sale");
        }
        return null;
    }
    public void setMaketCap(Float marketCap) {
        set("market_cap", marketCap);
    }
    public Float getMarketCap() {
        if (get("market_cap") != null) {
            return getFloat("market_cap");
        }
        return null;
    }
    public void setIpoYear(String ipoYear) {
        set("ipo_year", ipoYear);
    }
    public String getIpoYear() {
        if (get("ipo_year") != null) {
            return getString("ipo_year");
        }
        return null;
    }
    public void setSector(String sector) {
        set("sector", sector);
    }
    public String getSector() {
        if (get("sector") != null) {
            return getString("sector");
        }
        return null;
    }
    public void setIndustry(String industry) {
        set("industry", industry);
    }
    public String getIndustry() {
        if (get("industry") != null) {
            return getString("industry");
        }
        return null;
    }
    public void setSummaryQuote(String summaryQuote) {
        set("summary_quote", summaryQuote);
    }
    public String getSummaryQuote() {
        if (get("summary_quote") != null) {
            return getString("summary_quote");
        }
        return null;
    }
    public void setExchange(String exchange) {
        set("exchange", exchange);
    }
    public String getExchange() {
        if (get("exchange") != null) {
            return getString("exchange");
        }
        return null;
    }
    public void setActive(Boolean active) {
        set("active", active);
    }

    public Boolean getActive() {
        if (get("active") != null) {
            return getBoolean("active");
        }
        return false;
    }

    public void setVersion(Long version) {
        set("version", version);
    }

    public Long getVersion() {
        if (get("version") != null) {
            return getLong("version");
        }
        return 0L;
    }

    public void setVersionUpdatedAt(Date versionUpdatedAt) {
        set("version_updated_at", versionUpdatedAt);
    }

    public Date getVersionUpdatedAt() {
        if (get("version_updated_at") != null) {
            return getDate("version_updated_at");
        }
        return null;
    }

}
