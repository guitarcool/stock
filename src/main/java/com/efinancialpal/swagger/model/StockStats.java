package com.efinancialpal.swagger.model;

import java.math.BigDecimal;
import java.util.Date;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.DbName;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

@DbName("stock")
@Table("stock_stats")
@IdName("id")
public class StockStats extends Model {

    public void setId(Long id) {
        set("id", id);
    }

    public void setTicker(String ticker) {
        set("ticker", ticker);
    }

    public void setMarketCap(BigDecimal marketCap) {
        set("market_cap", marketCap);
    }

    public void setSharesFloat(Long sharesFloat) {
        set("shares_float", sharesFloat);
    }

    public void setSharesOutstanding(Long sharesOutstanding) {
        set("shares_outstanding", sharesOutstanding);
    }

    public void setSharesOwned(Long sharesOwned) {
        set("shares_owned", sharesOwned);
    }

    public void setEps(BigDecimal eps) {
        set("eps", eps);
    }

    public void setPe(BigDecimal pe) {
        set("pe", pe);
    }

    public void setPeg(BigDecimal peg) {
        set("peg", peg);
    }

    public void setEpsEstimateCurrentYear(BigDecimal epsEstimateCurrentYear) {
        set("eps_estimate_current_year", epsEstimateCurrentYear);
    }

    public void setEpsEstimateNextQuarter(BigDecimal epsEstimateNextQuarter) {
        set("eps_estimate_next_quarter", epsEstimateNextQuarter);
    }

    public void setEpsEstimateNextYear(BigDecimal epsEstimateNextYear) {
        set("eps_estimate_next_year", epsEstimateNextYear);
    }

    public void setPriceBook(BigDecimal priceBook) {
        set("price_book", priceBook);
    }

    public void setPriceSales(BigDecimal priceSales) {
        set("price_sales", priceSales);
    }

    public void setBookValuePerShare(BigDecimal bookValuePerShare) {
        set("book_value_per_share", bookValuePerShare);
    }

    public void setRevenue(BigDecimal revenue) {
        set("revenue", revenue);
    }

    public void setEbitda(BigDecimal ebitda) {
        set("ebitda", ebitda);
    }

    public void setOneYearTargetPrice(BigDecimal oneYearTargetPrice) {
        set("one_year_target_price", oneYearTargetPrice);
    }

    public void setShortRatio(BigDecimal shortRatio) {
        set("short_ratio", shortRatio);
    }

    public void setAuthor(BigDecimal author) {
        set("author", author);
    }

    public Long getId() {
        if (get("id") != null) {
            return getLong("id");
        }
        return null;
    }

    public String getAuthor() {
        if (get("author") != null) {
            return getString("author");
        }
        return null;
    }

    public String getTicker() {
        if (get("ticker") != null) {
            return getString("ticker");
        }
        return null;
    }

    public BigDecimal getMarketCap() {
        if (get("market_cap") != null) {
            return getBigDecimal("market_cap");
        }
        return null;
    }

    public Long getSharesFloat() {
        if (get("shares_float") != null) {
            return getLong("shares_float");
        }
        return null;
    }

    public Long getSharesOustanding() {
        if (get("shares_outstanding") != null) {
            return getLong("shares_outstanding");
        }
        return null;
    }

    public Long getSharesOwned() {
        if (get("shares_owned") != null) {
            return getLong("shares_owned");
        }
        return null;
    }

    public BigDecimal getEps() {
        if (get("eps") != null) {
            return getBigDecimal("eps");
        }
        return null;
    }

    public BigDecimal getPe() {
        if (get("pe") != null) {
            return getBigDecimal("pe");
        }
        return null;
    }

    public BigDecimal getPeg() {
        if (get("peg") != null) {
            return getBigDecimal("peg");
        }
        return null;
    }

    public BigDecimal getEpsEstimateCurrentYear() {
        if (get("eps_estimate_current_year") != null) {
            return getBigDecimal("eps_estimate_current_year");
        }
        return null;
    }

    public BigDecimal getEpsEstimateNextQuarter() {
        if (get("eps_estimate_next_quarter") != null) {
            return getBigDecimal("eps_estimate_next_quarter");
        }
        return null;
    }

    public BigDecimal getEpsEstimateNextYear() {
        if (get("eps_estimate_next_year") != null) {
            return getBigDecimal("eps_estimate_next_year");
        }
        return null;
    }

    public BigDecimal getPriceBook() {
        if (get("price_book") != null) {
            return getBigDecimal("price_book");
        }
        return null;
    }

    public BigDecimal getPriceSales() {
        if (get("price_sales") != null) {
            return getBigDecimal("price_sales");
        }
        return null;
    }

    public BigDecimal getRevenue() {
        if (get("revenue") != null) {
            return getBigDecimal("revenue");
        }
        return null;
    }

    public BigDecimal getEbitda() {
        if (get("ebitda") != null) {
            return getBigDecimal("ebitda");
        }
        return null;
    }

    public BigDecimal getOneYearTargetPrice() {
        if (get("one_year_target_price") != null) {
            return getBigDecimal("one_year_target_price");
        }
        return null;
    }

    public BigDecimal getShortRatio() {
        if (get("short_ratio") != null) {
            return getBigDecimal("short_ratio");
        }
        return null;
    }

    public void setActive(Boolean active) {
        set("active", active);
    }

    public Boolean getActive() {
        if (get("active") != null) {
            return getBoolean("active");
        }
        return false;
    }

    public void setVersion(Long version) {
        set("version", version);
    }

    public Long getVersion() {
        if (get("version") != null) {
            return getLong("version");
        }
        return 0L;
    }

    public void setVersionUpdatedAt(Date versionUpdatedAt) {
        set("version_updated_at", versionUpdatedAt);
    }

    public Date getVersionUpdatedAt() {
        if (get("version_updated_at") != null) {
            return getDate("version_updated_at");
        }
        return null;
    }


}
