package com.myprogram;

/**
 * Created by haiyanluo on 9/4/17.
 */
public class ConstructTree {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder == null || inorder == null || preorder.length == 0 || preorder.length != inorder.length) {
            return null;
        }
        return helper(preorder, inorder, new int[]{0}, 0, inorder.length - 1);
    }
    private TreeNode helper(int[] preorder, int[] inorder, int[] index, int first, int last) {
        if (first > last) {
            return null;
        }
        int val = preorder[index[0]];
        TreeNode root = new TreeNode(val);
        int idx = findIndex(inorder, val);
        ++index[0];
        root.left = helper(preorder, inorder, index, first, idx - 1);
        root.right = helper(preorder, inorder, index, idx + 1, last);
        return root;
    }
    private int findIndex(int[] inorder, int val) {
        int low = 0;
        int high = inorder.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (inorder[mid] == val) {
                return mid;
            } else if (inorder[mid] < val) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        ConstructTree constructTree = new ConstructTree();
        constructTree.buildTree(new int[]{1, 2}, new int[]{2, 1});
    }
}
