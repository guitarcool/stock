package com.myprogram;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class amazon {

    public static List<String> reorder(List<String> input) {

        List<String> tmpCharList = new ArrayList<>(), tmpIntList = new ArrayList<>();
        for (String s : input) {
            if (isStringList(s)) {
                tmpCharList.add(s);
            } else {
                tmpIntList.add(s);
            }
        }
        Collections.sort(tmpCharList, new Comparator<String>() {
            @Override
            public int compare(String a, String b) {
                String[] split1 = a.split(" ");
                String[] split2 = b.split(" ");
                int i = split1[0].length(), j = split2[0].length();
                while (i < a.length() && j < b.length()) {
                    if (Character.toLowerCase(a.charAt(i)) != Character.toLowerCase(b.charAt(j))) {
                        return Character.compare(Character.toLowerCase(a.charAt(i)), Character.toLowerCase(b.charAt(j)));
                    }
                    ++i;
                    ++j;
                }
                if (i < a.length()) {
                    return -1;
                }
                if (j < b.length()) {
                    return 1;
                }
                return split1[0].compareTo(split2[0]);
            }
        });

        tmpCharList.addAll(tmpIntList);
        return tmpCharList;

    }

    public static boolean isStringList(String s) {
        String split[] = s.split(" ");
        for (int i = 1; i < split.length; ++i) {
            for (int j = 0; j < split[i].length(); ++j) {
                if (!Character.isLetter(split[i].charAt(j))) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        List<String> input = new ArrayList<>();
        input.add("a1 9 2 3 1");
        input.add("g1 Act car");
        input.add("zo4 4 7");
        input.add("ab1 off KEY dog");
        input.add("a8 act zoo");
        System.out.println(reorder(input));
    }
}
