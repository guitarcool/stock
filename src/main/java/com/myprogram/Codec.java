package com.myprogram;

import java.util.HashMap;
import java.util.Map;

public class Codec {
    Map<Long, Character> map = new HashMap<>();
    Map<Character, Long> reverseMap = new HashMap<>();
    Map<Long, String> result = new HashMap<>();
    long index = 1;
    public Codec() {
        long i = 0;
        for (char c = 'a'; c <= 'z'; ++c) {
            map.put(i, c);
            reverseMap.put(c, i);
            ++i;
        }
        for (char c = 'A'; c <= 'Z'; ++c) {
            map.put(i, c);
            reverseMap.put(c, i);
            ++i;
        }
        for (char c = '0'; c <= '9'; ++c) {
            map.put(i, c);
            reverseMap.put(c, i);
            ++i;
        }
    }

    // Encodes a URL to a shortened URL.
    public String encode(String longUrl) {
        StringBuilder builder = new StringBuilder();
        long id = index;
        while (id > 0) {
            builder.append(map.get(id % map.size()));
            id /= map.size();
        }
        while (builder.length() < 6) {
            builder.append('+');
        }
        result.put(index++, longUrl);
        return builder.reverse().toString();
    }

    // Decodes a shortened URL to its original URL.
    public String decode(String shortUrl) {
        long id = 0;
        for (int i = 0; i < shortUrl.length(); ++i) {
            char c = shortUrl.charAt(i);
            if (reverseMap.containsKey(c)) {
                id = id * map.size() + reverseMap.get(c);
            }
        }
        return result.get(id);
    }

    public static void main(String[] args) {
        Codec codec = new Codec();
        String encoded = codec.encode("https://leetcode.com/problems/design-tinyurl");
        System.out.println(codec.decode(encoded));

    }
}
