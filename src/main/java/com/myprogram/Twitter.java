package com.myprogram;

import java.util.*;

/**
 * Created by haiyanluo on 8/24/17.
 */
public class Twitter {
    private Map<Integer, Node> tweetMap = new HashMap<>();
    private Map<Integer, Set<Integer>> followMap = new HashMap<>();
    /** Initialize your data structure here. */
    public Twitter() {
    }

    /** Compose a new tweet. */
    public void postTweet(int userId, int tweetId) {
        if (!tweetMap.containsKey(userId)) {
            tweetMap.put(userId, null);
        }
        Node node = new Node(new Tweet(tweetId, System.currentTimeMillis()));
        node.next = tweetMap.get(userId);
        tweetMap.put(userId, node);
    }

    /** Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent. */
    public List<Integer> getNewsFeed(int userId) {
        PriorityQueue<Node> pq = new PriorityQueue<>(new Comparator<Node>() {
            @Override
            public int compare(Node a, Node b) {
                return Long.compare(b.tweet.timestamp, a.tweet.timestamp);
            }
        });
        List<Integer> result = new ArrayList<>();
        if (!followMap.containsKey(userId)) {
            return result;
        }
        for (Integer i : followMap.get(userId)) {
            if (tweetMap.containsKey(i)) {
                pq.offer(tweetMap.get(i));
            }
        }
        while (!pq.isEmpty() && result.size() < 10) {
            Node node = pq.poll();
            result.add(node.tweet.tweetId);
            if (node.next != null) {
                pq.offer(node.next);
            }
        }
        return result;
    }

    /** Follower follows a followee. If the operation is invalid, it should be a no-op. */
    public void follow(int followerId, int followeeId) {
        if (followMap.containsKey(followerId)) {
            followMap.put(followerId, new HashSet<Integer>());
            followMap.get(followerId).add(followerId);
        }
        followMap.get(followerId).add(followeeId);
    }

    /** Follower unfollows a followee. If the operation is invalid, it should be a no-op. */
    public void unfollow(int followerId, int followeeId) {
        if (followMap.containsKey(followerId)) {
            followMap.get(followerId).remove(followeeId);
        }
    }

    public class Tweet {
        public Tweet(int tweetId, long timestamp) {
            this.tweetId = tweetId;
            this.timestamp = timestamp;
        }
        public int tweetId;
        public long timestamp;
    }

    public class Node {
        public Tweet tweet;
        public Node next = null;
        public Node (Tweet tweet) {
            this.tweet = tweet;
        }
    }
}
