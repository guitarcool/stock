package com.myprogram;

import com.apple.concurrent.Dispatch;
import org.apache.commons.collections.*;

import java.util.*;
import java.util.PriorityQueue;

/**
 * Created by haiyanluo on 8/24/17.
 */
public class MyProgram {
    private String ipv4(String IP) {
        String[] splits = IP.split("\\.");
        if (splits.length != 4) {
            return "Neither";
        }
        for (int i = 0; i < splits.length; ++i) {
            if (splits[i] == null || splits[i].length() == 0) {
                return "Neither";
            }
            Integer num = null;
            try {
                num = Integer.valueOf(splits[i]);
            } catch (Exception e) {
                return "Neither";
            }
            if (num < 0 || num > 255) {
                return "Neither";
            }
            if (splits[i].length() > 1 && splits[i].charAt(0) == '0') {
                return "Neither";
            }
        }
        return "IPv4";
    }
    private String ipv6(String IP) {
        String[] splits = IP.split(":");
        if (splits.length != 8) {
            return "Neither";
        }
        for (int i = 0; i < splits.length; ++i) {
            String s = splits[i];
            if (s == null || s.length() == 0) {
                return "Neither";
            }
            if (s.length() > 4) {
                return "Neither";
            }
            for (int j = 0; j < s.length(); ++j) {
                char c = s.charAt(j);
                if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))) {
                    return "Neither";
                }
            }
        }
        return "Neither";
    }
    public int findComplement(int num) {
        boolean started = false;
        int result = 0;
        for (int i = 31; i >= 0; --i) {
            if (((1 << i) & num) != 0) {
                started = true;
            }
            else if (started) {
                result |= (1 << i);
            }
        }
        return result;
    }

    public boolean isPerfectSquare(int num) {
        int low = 1;
        int high = num;
        while (low <= high) {
            long mid = low + (high - low) / 2;
            long mul = mid * mid;
            if (mul == num) {
                return true;
            } else if (mul < num) {
                low = (int) mid + 1;
            } else {
                high = (int) mid - 1;
            }
        }
        return false;
    }


    public String solveEquation(String equation) {
        String[] splits = equation.split("=");
        List<String> left = breakIt(splits[0]);
        List<String> right = breakIt(splits[1]);
        int lh = 0;
        int rh = 0;
        for (String v : left) {
            if (v.indexOf("x") >= 0) {
                if (v.equals("x") || v.equals("+x")) {
                    lh += 1;
                } else if (v.equals("-x")) {
                    lh -= 1;
                } else {
                    lh += Integer.valueOf(v.substring(0, v.indexOf("x")));
                }
            } else {
                rh -= Integer.valueOf(v);
            }
        }
        for (String v : right) {
            if (v.indexOf("x") >= 0) {
                if (v.equals("x") || v.equals("+x")) {
                    lh -= 1;
                } else if (v.equals("-x")) {
                    lh += 1;
                } else {
                    lh -= Integer.valueOf(v.substring(0, v.indexOf("x")));
                }
            } else {
                rh += Integer.valueOf(v);
            }
        }
        if (lh == 0) {
            if (rh == 0) {
                return "Infinite solutions";
            }
            return "No solution";
        }
        return "x=" + lh / rh;
    }
    List<String> breakIt(String s) {
        List<String> result = new ArrayList<>();
        int i = 0;
        String r = "";
        while (i < s.length()) {
            if (s.charAt(i) == '+' || s.charAt(i) == '-') {
                if (r.length() > 0) {
                    result.add(r);
                }
                r = "" + s.charAt(i);
            } else {
                r += s.charAt(i);
            }
            ++i;
        }
        result.add(r);
        return result;
    }

    TreeNode buildTree() {
        TreeNode root = new TreeNode(20);
        root.left = new TreeNode(5);
        root.right = new TreeNode(70);
        root.left.left = new TreeNode(2);
        root.left.right = new TreeNode(10);
        return root;
    }

    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        if (root == null)
            return null;

        if (root.val <= p.val) {
            return inorderSuccessor(root.right, p);
        } else {
            TreeNode left = inorderSuccessor(root.left, p);
            return (left != null) ? left : root;
        }
    }

    void permute(List<List<Integer>> result, int[] sum, int target, List<Integer> partial) {
        for (int i = 0; i < sum.length; ++i) {
            partial.add(sum[i]);
            if (mySum(partial) == target) {
                result.add(new ArrayList<>(partial));
            } else if (mySum(partial) < target) {
                permute(result, sum, target, partial);
            }
            partial.remove(partial.size() - 1);
        }
    }

    int mySum(List<Integer> list) {
        int sum = 0;
        for (int v : list) {
            sum += v;
        }
        return sum;
    }

    public List<String> generatePalindromes(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        List<Character> chs = new ArrayList<>();
        List<String> result = new ArrayList<>();
        String mid = "";
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            char c = entry.getKey();
            int count = entry.getValue();
            if (count % 2 == 1) {
                mid += c;
                if (++count > 1) {
                    return result;
                }
            }
            for (int i = 0; i < count / 2; ++i) {
                chs.add(c);
            }
        }
        boolean[] visited = new boolean[chs.size()];
        helper(result, chs, new ArrayList<Character>(), mid, visited);
        return result;
    }
    void helper(List<String> result, List<Character> chs, List<Character> partial, String mid, boolean[] visited) {
        if (partial.size() == chs.size()) {
            StringBuilder builder = new StringBuilder();
            for (char c : partial) {
                builder.append(c);
            }
            result.add(builder.toString() + mid + builder.reverse().toString());
            return;
        }
        for (int i = 0; i < chs.size(); ++i) {
            if (visited[i]) {
                continue;
            }
            partial.add(chs.get(i));
            visited[i] = true;
            helper(result, chs, partial, mid, visited);
            visited[i] = false;
            partial.remove(partial.size() - 1);
        }
    }

    public List<String> generateAbbreviations(String word) {
        List<String> res = new ArrayList<String>();
        int len = word.length();
        res.add(len==0 ? "" : String.valueOf(len));
        for(int i = 0 ; i < len ; i++) {
            List<String> children = generateAbbreviations(word.substring(i + 1));
            for (String right : children) {
                String leftNum = i > 0 ? String.valueOf(i) : "";
                res.add(leftNum + word.substring(i, i + 1) + right);
            }
        }
        return res;
    }

    public double[] medianSlidingWindow(int[] nums, int k) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Collections.reverseOrder());
        int n = nums.length;
        double[] result = new double[n - k + 1];
        int index = 0;
        for (int i = 0; i < n; ++i) {
            maxHeap.offer(nums[i]);
            minHeap.offer(maxHeap.poll());
            if (maxHeap.size() < minHeap.size()) {
                maxHeap.offer(minHeap.poll());
            }
            if (i >= k - 1) {
                if (maxHeap.size() == minHeap.size()) {
                    result[index++] = (minHeap.peek() + maxHeap.peek()) / 2.0;
                } else {
                    result[index++] = maxHeap.peek();
                }
            }
            if (i - k >= 0) {
                int val = nums[i - k];
                if (val > maxHeap.peek()) {
                    minHeap.remove(val);
                } else {
                    maxHeap.remove(val);
                }
                if (maxHeap.size() < minHeap.size()) {
                    maxHeap.offer(minHeap.poll());
                } else if (maxHeap.size() > 1 + minHeap.size()) {
                    minHeap.offer(maxHeap.poll());
                }
            }
        }
        return result;
    }

    public boolean isNumber(String s) {
        if (s == null || s.length() == 0) {
            return false;
        }
        s = s.trim();
        if (s.length() == 0) {
            return false;
        }
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (c == 'e' || c == 'E') {
                String second = null;
                if (i + 1 < s.length()) {
                    second = s.substring(i + 1);
                }
                return isFloat(s.substring(0, i)) && isInteger(second);
            }
        }
        return isFloat(s);
    }
    boolean isInteger(String s) {
        if (s == null || s.length() == 0) {
            return false;
        }
        int i = 0;
        if (i < s.length() && (s.charAt(0) == '+' || s.charAt(0) == '-')) {
            ++i;
        }
        for (i = 0; i < s.length(); ++i) {
            if (!Character.isDigit(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    boolean isFloat(String s) {
        boolean hasDot = false;
        boolean hasDigit = false;
        int i = 0;
        if (i < s.length() && (s.charAt(0) == '+' || s.charAt(0) == '-')) {
            ++i;
        }
        for (; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (!Character.isDigit(c)) {
                if (c == '.') {
                    if (hasDot) {
                        return false;
                    } else {
                        hasDot = true;
                    }
                } else {
                    return false;
                }
            } else {
                hasDigit = true;
            }
        }
        return hasDigit;
    }

    public int[] smallestRange(List<List<Integer>> nums) {
        int minLen = Integer.MAX_VALUE;
        int[] result = new int[2];
        PriorityQueue<Node> q = new PriorityQueue<>(new Comparator<Node>() {
            @Override
            public int compare(Node a, Node b) {
                return Integer.compare(a.val, b.val);
            }
        });
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < nums.size(); ++i) {
            Node node = new Node(i, 0, nums.get(i).get(0));
            q.offer(node);
            max = Math.max(node.val, max);
        }
        while (!q.isEmpty()) {
            Node node = q.poll();
            if (max - node.val < minLen) {
                minLen = max - node.val;
                result[0] = node.val;
                result[1] = max;
            }
            if (node.offset == nums.get(node.index).size() - 1) {
                break;
            } else {
                Node newNode = new Node(node.index, node.offset + 1, nums.get(node.index).get(node.offset + 1));
                q.offer(newNode);
                max = Math.max(newNode.val, max);
            }
        }
        return result;
    }
    class Node {
        int index;
        int offset;
        int val;
        public Node(int index, int offset, int val) {
            this.index = index;
            this.offset = offset;
            this.val = val;
        }
    }

    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> result = new ArrayList<>();
        Set<String> set = new HashSet<>(wordList);
        Queue<String> q = new LinkedList<>();
        Set<String> visited = new HashSet<>();
        q.offer(beginWord);
        visited.add(beginWord);
        Map<String, Set<String>> reverseMap = new HashMap<>();
        reverseMap.put(beginWord, new HashSet<>());
        boolean hasFound = false;
        while (!q.isEmpty()) {
            if (hasFound) {
                break;
            }
            int size = q.size();
            for (int i = 0; i < size; ++i) {
                String oldWord = q.poll();
                char[] word = oldWord.toCharArray();
                for (int k = 0; k < word.length; ++k) {
                    char originChar = word[k];
                    for (char c = 'a'; c <= 'z'; ++c) {
                        word[k] = c;
                        String newWord = new String(word);
                        if (newWord.equals(endWord)) {
                            hasFound = true;
                        }
                        if (set.contains(newWord) && !visited.contains(newWord)) {
                            if (!reverseMap.containsKey(newWord)) {
                                reverseMap.put(newWord, new HashSet<>());
                            }
                            reverseMap.get(newWord).add(oldWord);
                            visited.add(newWord);
                        }

                    }
                    word[k] = originChar;
                }
            }
        }
        if (!hasFound) {
            return result;
        }
        List<String> list = new ArrayList<>();
        list.add(endWord);
        dfs(beginWord, endWord, reverseMap, result, list);
        return result;
    }
    void dfs(String beginWord, String word, Map<String, Set<String>> reverseMap, List<List<String>> result, List<String> cur) {
        if (word.equals(beginWord)) {
            List<String> sorted = new ArrayList<>(cur);
            Collections.reverse(sorted);
            result.add(sorted);
            return;
        }
        for (String child : reverseMap.get(word)) {
            cur.add(child);
            dfs(beginWord, child, reverseMap, result, cur);
            cur.remove(cur.size() - 1);
        }
    }

    public int divide(int dividend, int divisor) {
        long sign = 1;
        if (dividend < 0) {
            sign *= -1;
        }
        if (divisor < 0) {
            sign *= -1;
        }
        long dividen = Math.abs((long) dividend);
        long diviso = Math.abs((long) divisor);
        long result = 0;
        while (dividen >= diviso) {
            long r = 1;
            long d = diviso;
            while ((d << 1) <= dividen) {
                r <<= 1;
                d <<= 1;
            }
            result += r;
            dividen -= d;
        }
        if (result > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        }
        return (int) (sign * result);
    }

    public TreeNode str2tree(String s) {
        if (s == null || s.length() == 0) {
            return null;
        }
        int firstParen = s.indexOf("(");
        int val = firstParen == -1 ? Integer.valueOf(s) : Integer.valueOf(s.substring(0, firstParen));
        TreeNode node = new TreeNode(val);
        int index = findIndex(s);
        if (firstParen != -1) {
            String left = s.substring(firstParen + 1, index);
            String right = index + 2 < s.length() - 1 ? s.substring(index + 2, s.length() - 1) : null;
            node.left = str2tree(left);
            node.right = str2tree(right);
        }
        return node;
    }
    int findIndex(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (c == '(') {
                ++count;
            } else if (c == ')') {
                if (--count == 0) {
                    return i;
                }
            }
        }
        return -1;
    }

    public List<String> fullJustify(String[] words, int maxWidth) {
        List<String> result = new ArrayList<>();
        int index = 0;
        while (index < words.length) {
            int cnt = words[index].length();
            int j = index + 1;
            while (j < words.length) {
                if (cnt + words[j].length() + 1 > maxWidth) {
                    break;
                }
                cnt += words[j].length() + 1;
                ++j;
            }
            StringBuilder builder = new StringBuilder();
            int diff = j - index - 1;
            if (j == words.length || j - index == 1) {
                // last line or only one word
                for (int i = index; i < j; ++i) {
                    builder.append(words[i]);
                    if (i != j - 1) {
                        builder.append(" ");
                    }
                }
                for (int i = builder.length(); i < maxWidth; ++i) {
                    builder.append(" ");
                }
            } else {
                int spaces = (maxWidth - cnt) / diff;
                int mod = (maxWidth - cnt) % diff;
                for (int i = index; i < j; ++i) {
                    builder.append(words[i]);
                    if (i != j - 1) {
                        for (int k = 0; k <= spaces; ++k) {
                            builder.append(" ");
                        }
                        if (i - index < mod) {
                            builder.append(" ");
                        }
                    }
                }
            }
            result.add(builder.toString());
            index = j;
        }
        return result;
    }
        public List<String> findItinerary(String[][] tickets) {
            Map<String, PriorityQueue<String>> targets = new HashMap<>();
            List<String> route = new LinkedList();
            for (String[] ticket : tickets)
                targets.computeIfAbsent(ticket[0], k -> new PriorityQueue()).add(ticket[1]);
            visit("JFK", targets, route);
            return route;
        }
        void visit(String airport, Map<String, PriorityQueue<String>> targets, List<String> route) {
            while(targets.containsKey(airport) && !targets.get(airport).isEmpty())
                visit(targets.get(airport).poll(), targets, route);
            route.add(0, airport);
        }

    public int maximalRectangle(char[][] matrix) {
        if(matrix.length == 0 || matrix[0].length == 0) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int[] left = new int[n], right = new int[n], height = new int[n];
        Arrays.fill(right, n);
        int maxA = 0;
        for(int i = 0; i < m; i++) {
            int curLeft = 0, curRight = n;
            for(int j = 0; j < n; j++) { // compute left (from left to right)
                if(matrix[i][j] == '1') {
                    ++height[j]; left[j] = Math.max(left[j], curLeft);
                } else {
                    left[j] = 0; curLeft = j + 1; height[j] = 0;
                }
            }
            // compute right (from right to left)
            for(int j = n - 1; j >= 0; j--) {
                if(matrix[i][j]=='1') {
                    right[j] = Math.min(right[j], curRight);
                } else {
                    right[j] = n; curRight = j;
                }
                // compute the area of rectangle (can do this from either side)
                maxA = Math.max(maxA, (right[j] - left[j]) * height[j]);
            }
            System.out.println("");
        }
        return maxA;
    }

    public String shortestPalindrome(String s) {
        int j = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == s.charAt(j)) {
                ++j;
            }
        }
        if (j == s.length()) { return s; }
        String suffix = s.substring(j);
        return new StringBuilder(suffix).reverse().toString() + shortestPalindrome(s.substring(0, j)) + suffix;
    }

    public String removeDuplicateLetters(String s) {
        int[] cnt = new int[26];
        for (int i = 0; i < s.length(); ++i) {
            ++cnt[s.charAt(i) - 'a'];
        }
        int pos = 0;
        for (int i = 0; i < s.length(); ++i) {
            if (s.charAt(i) < s.charAt(pos)) {
                pos = i;
            }
            if (--cnt[s.charAt(i) - 'a'] == 0) {
                break;
            }
        }
        return s.length() == 0 ? "" : s.charAt(pos) + removeDuplicateLetters(s.substring(pos + 1).replaceAll("" + s.charAt(pos), ""));
    }
    public String countOfAtoms(String formula) {
        Stack<Map<String,Integer>> stack= new Stack<>();
        Map<String,Integer> map= new HashMap<>();
        int i=0,n=formula.length();
        while(i<n){
            char c=formula.charAt(i);i++;
            if(c=='('){
                stack.push(map);
                map=new HashMap<>();
            }
            else if(c==')'){
                int val=0;
                while(i<n && Character.isDigit(formula.charAt(i)))
                    val=val*10+formula.charAt(i++)-'0';

                if(val==0) val=1;
                if(!stack.isEmpty()){
                    Map<String,Integer> temp= map;
                    map=stack.pop();
                    for(String key: temp.keySet())
                        map.put(key,map.getOrDefault(key,0)+temp.get(key)*val);
                }
            }
            else{
                int start=i-1;
                while(i<n && Character.isLowerCase(formula.charAt(i))){
                    i++;
                }
                String s= formula.substring(start,i);
                int val=0;
                while(i<n && Character.isDigit(formula.charAt(i))) val=val*10+ formula.charAt(i++)-'0';
                if(val==0) val=1;
                map.put(s,map.getOrDefault(s,0)+val);
            }
        }
        StringBuilder sb= new StringBuilder();
        List<String> list= new ArrayList<>(map.keySet());
        Collections.sort(list);
        for(String key: list){
            sb.append(key);
            if(map.get(key)>1) sb.append(map.get(key));
        }
        return sb.toString();
    }

    public String crackSafe(int n, int k) {
        if (n == 1 && k == 1) return "0";
        Set<String> seen = new HashSet();
        StringBuilder ans = new StringBuilder();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n-1; ++i)
            sb.append("0");
        String start = sb.toString();

        dfs(start, k, seen, ans);
        ans.append(start);
        return new String(ans);
    }

    public void dfs(String node, int k, Set<String> seen, StringBuilder ans) {
        for (int x = 0; x < k; ++x) {
            String nei = node + x;
            if (!seen.contains(nei)) {
                seen.add(nei);
                dfs(nei.substring(1), k, seen, ans);
                ans.append(x);
            }
        }
    }

    public String minWindow(String S, String T) {
        int m = S.length(), n = T.length();
        int[] f = new int[m]; Arrays.fill(f, -1);
        String result = "";
        for (int j = 0; j < n; ++j) {
            int k = -1;
            for (int i = 0; i < m; ++i) {
                if (S.charAt(i) == T.charAt(j)) {
                    if (j == 0) {
                        f[i] = i;
                    } else {
                        if (k >= 0) {
                            f[i] = k;
                        }
                    }
                    if (j == n - 1) {
                        if (f[i] >= 0 && (result.length() == 0 || i - f[i] + 1 < result.length())) {
                            result = S.substring(f[i], i + 1);
                        }
                    }
                }
                if (f[i] >= 0) {
                    k = f[i];
                }
            }
        }
        return result;
    }

    public int mySqrt(int x) {
        if (x == 0)
            return 0;
        int left = 1, right = x;
        while (left < right) {
            int mid = left + (right - left)/2;
            if (mid > x/mid) {
                right = mid - 1;
            } else if (mid == x / mid) {
                return mid;
            } else {
                //if (mid + 1 > x/(mid + 1)) // using division is to avoid overflow of mid * mid
                //   return mid;
                left = mid;
            }
        }
        return left;
    }


    public String addBoldTag(String s, String[] dict) {
        List<int[]> intervals = new ArrayList<>();
        for (String word : dict) {
            int index = 0;
            while (index >= 0) {
                index = s.indexOf(word, index);
                if (index >= 0) {
                    intervals.add(new int[]{index, index + word.length()});
                } else {
                    break;
                }
                index += word.length();
            }
        }
        Collections.sort(intervals, (a, b) -> {
            if (a[0] != b[0]) {
                return a[0] - b[0];
            } else {
                return a[1] - b[1];
            }
        });
        List<int[]> result = new ArrayList<>();
        if (intervals.size() > 0) {
            result.add(intervals.get(0));
        }
        for (int i = 1; i < intervals.size(); ++i) {
            int[] interval = intervals.get(i);
            if (interval[0] <= result.get(result.size() - 1)[1] + 1) {
                result.get(result.size() - 1)[1] = Math.max(interval[1], result.get(result.size() - 1)[1]);
            } else {
                result.add(interval);
            }
        }
        StringBuilder builder = new StringBuilder();
        int prev = 0;
        for (int i = 0; i < result.size(); ++i) {
            builder.append(s.substring(prev, result.get(i)[0]));
            builder.append("<b>");
            builder.append(s.substring(result.get(i)[0], result.get(i)[1]));
            builder.append("</b>");
            prev = result.get(i)[1];
        }
        builder.append(prev);
        return builder.toString();
    }

    public String boldWords(String[] words, String S) {
        int n = S.length();
        boolean[] bold = new boolean[n];
        for (String word : words) {
            int index = 0;
            while (index >= 0) {
                index = S.indexOf(word, index);
                for (int i = index; i < index + word.length(); ++i) {
                    bold[i] = true;
                }
                if (index < 0) {
                    break;
                }
                ++index;
            }
        }
        StringBuilder builder = new StringBuilder();
        int i = 0;
        while (i < n) {
            if (!bold[i]) {
                builder.append(S.charAt(i++));
            } else {
                int j = i;
                builder.append("<b>");
                while (j < n && bold[j]) {
                    builder.append(S.charAt(j++));
                }
                builder.append("</b>");
                i = j;
            }
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        MyProgram myProgram = new MyProgram();
    //    myProgram.ipv6("2001:0db8:85a3:0:0:8A2E:0370:7334:");
        // myProgram.findComplement(5);
       // myProgram.isPerfectSquare(2147483647);
       // myProgram.solveEquation("x+5-3+x=6+x-2");
        // TreeNode root = new TreeNode(20);
        // TreeNode p = new TreeNode(10);
        // System.out.println(myProgram.inorderSuccessor(myProgram.buildTree(), p));
        // System.out.println(myProgram.inorderSuccessor(p, p));

        // List<List<Integer>> result = new ArrayList<>();
        // myProgram.permute(result, new int[]{1, 2, 3}, 6, new ArrayList<>());
        // System.out.println(result);
        // myProgram.generatePalindromes("a");
        // myProgram.generateAbbreviations("word");
        // myProgram.medianSlidingWindow(new int[]{1, 3,  -1,  -3,  5, 3,  6, 7}, 3);
        // myProgram.isNumber(" 005047e+6");

        //List<List<Integer>> lists = new ArrayList<>();
        //lists.add(Arrays.asList(4,10,15,24,26));
        //lists.add(Arrays.asList(0,9,12,20));
        //lists.add(Arrays.asList(5,18,22,30));
        //myProgram.smallestRange(lists);
        // myProgram.findLadders("hit", "cog", Arrays.asList("hot","dot","dog","lot","log","cog"));
        // myProgram.divide(-2147483648, -1);
        // myProgram.str2tree("4(2(3)(1))(6(5))");
        // myProgram.fullJustify(new String[]{"a","b","c","d","e"}, 3);
        // List<String> route = myProgram.findItinerary(new String[][]{{"JFK", "DEF"}, {"JFK", "ABC"}});
       // System.out.println(route);
        // myProgram.maximalRectangle(new char[][]{{'1', '0', '1', '0', '0'}, {'1', '0', '1', '1', '1'}, {'1', '0', '1', '1', '1'}, {'1', '0', '0', '1', '0'}});
        // myProgram.shortestPalindrome("aacecaaa");
        // myProgram.removeDuplicateLetters("bcabc");
        // myProgram.countOfAtoms("K4(ON(SO3)2)2");
        // myProgram.crackSafe(2, 2);
        // myProgram.minWindow("cnhczmccqouqadqtmjjzl", "mm");
        // myProgram.mySqrt(8);
        // myProgram.addBoldTag("aaabbcc", new String[]{"aaa","aab", "bc"});
        myProgram.boldWords(new String[]{"ab", "bc"}, "aabcd");
    }
}
