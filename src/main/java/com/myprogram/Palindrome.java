package com.myprogram;

/**
 * Created by haiyanluo on 10/5/17.
 */
public class Palindrome {
    public boolean isPalindrome(String s) {
        int i = 0, j = s.length() - 1;
        while (i < j) {
            while (i < j && !Character.isLetterOrDigit(i)) {
                ++i;
            }
            while (i < j && !Character.isLetterOrDigit(j)) {
                --j;
            }
            char a = s.charAt(i++);
            char b = s.charAt(j--);
            if (Character.toUpperCase(a) != Character.toUpperCase(b)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Palindrome palindrome = new Palindrome();
        palindrome.isPalindrome("ab");
    }
}
