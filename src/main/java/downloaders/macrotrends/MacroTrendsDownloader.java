package downloaders.macrotrends;

/**
 * Created by haiyanluo on 8/19/18.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by haiyanluo on 8/9/18.
 */
public class MacroTrendsDownloader {
    private static final Logger logger = LoggerFactory.getLogger(MacroTrendsDownloader.class);

    private static final String BASE_URL = "http://download.macrotrends.net/assets/php/stock_data_export.php?t=";

    private static MacroTrendsDownloader instance = new MacroTrendsDownloader();
    private MacroTrendsDownloader() {}

    public static MacroTrendsDownloader getInstance() {
        return instance;
    }

    public List<HistoricalQuote> download(String symbol) {
        List<HistoricalQuote> quotes = new ArrayList<>();
        try {
            URL url = new URL(buildUrl(symbol));
            BufferedReader in;
            in = new BufferedReader(
                    new InputStreamReader(url.openStream()));

            String inputLine;
            int lineNo = 0;
            while ((inputLine = in.readLine()) != null) {
                // skip the first 11 lines
                if (lineNo++ <= 11) {
                    continue;
                }
                String[] splits = inputLine.split(",");
                HistoricalQuote quote = new HistoricalQuote();

                Calendar calendar = Calendar.getInstance();
                String pattern = "yyy-MM-dd";
                calendar.setTime(new SimpleDateFormat(pattern).parse(splits[0]));

                quote.setSymbol(symbol);
                quote.setDate(calendar);
                quote.setOpen(new BigDecimal(splits[1]));
                quote.setHigh(new BigDecimal(splits[2]));
                quote.setLow(new BigDecimal(splits[3]));
                quote.setClose(new BigDecimal(splits[4]));
                quote.setVolume(Long.valueOf(splits[5]));

                quotes.add(quote);
            }
            in.close();

            Collections.sort(quotes, new Comparator<HistoricalQuote>() {
                @Override
                public int compare(HistoricalQuote o1, HistoricalQuote o2) {
                    return o2.getDate().getTime().compareTo(o1.getDate().getTime());
                }
            });

        } catch (Exception e) {
            logger.error("Failed to download {} ", symbol, e);
            return new ArrayList<>();
        }
        return quotes;
    }

    /*
    CsvReader products = new CsvReader("products.csv");

			products.readHeaders();

			while (products.readRecord())
    {
        String productID = products.get("ProductID");
        String productName = products.get("ProductName");
        String supplierID = products.get("SupplierID");
        String categoryID = products.get("CategoryID");
        String quantityPerUnit = products.get("QuantityPerUnit");
        String unitPrice = products.get("UnitPrice");
        String unitsInStock = products.get("UnitsInStock");
        String unitsOnOrder = products.get("UnitsOnOrder");
        String reorderLevel = products.get("ReorderLevel");
        String discontinued = products.get("Discontinued");

        // perform program logic here
        System.out.println(productID + ":" + productName);
    }

			products.close();

} catch (FileNotFoundException e) {
        e.printStackTrace();
        } catch (IOException e) {
        e.printStackTrace();
        }

*/

    private String buildUrl(String symbol) {
        StringBuilder builder = new StringBuilder();
        builder.append(BASE_URL);
        builder.append(symbol);
        return builder.toString();
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; ++i) {
            List<HistoricalQuote> quotes = MacroTrendsDownloader.getInstance().download("MSFT");
            System.out.println(quotes);
        }
    }
}

