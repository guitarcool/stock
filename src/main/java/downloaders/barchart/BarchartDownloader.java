package downloaders.barchart;

/**
 * Created by haiyanluo on 11/20/17.
 */

import com.barchart.ondemand.BarchartOnDemandClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.util.MyHttpClient;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;


public class BarchartDownloader {
    private static final Logger logger = LoggerFactory.getLogger(BarchartDownloader.class);

    private static final String BASE_URL = "https://marketdata.websol.barchart.com/getHistory.json?apikey=";
    private final static String API_KEY = "539ee43ee7842fb6839fea0171ee9355";
    private final static BarchartOnDemandClient onDemand = new BarchartOnDemandClient.Builder().apiKey(API_KEY).build();


    private static BarchartDownloader instance = new BarchartDownloader();
    private BarchartDownloader() {}

    public static BarchartDownloader getInstance() {
        return instance;
    }


    public List<HistoricalQuote> download(String symbol) {
        List<HistoricalQuote> quotes = new ArrayList<>();
        try {
            String url = buildUrl(symbol);
            String result = MyHttpClient.getInstance().sendGetRequest(url);
            JSONObject jsonObject = new JSONObject(result);
            JSONArray array = (JSONArray) jsonObject.get("results");
            for (int i = 0; i < array.length(); ++i) {
                HistoricalQuote quote = new HistoricalQuote();
                JSONObject object = (JSONObject)  array.get(i);
                double open = (double) object.get("open");
                double high = (double) object.get("high");
                double low = (double) object.get("low");
                double close = (double) object.get("close");
                long volume = (int) object.get("volume");
                quote.setSymbol(symbol);
                quote.setOpen(new BigDecimal(open));
                quote.setHigh(new BigDecimal(high));
                quote.setLow(new BigDecimal(low));
                quote.setClose(new BigDecimal(close));
                quote.setVolume(volume);
                Calendar calendar = Calendar.getInstance();
                quote.setDate(calendar);
                String pattern = "yyy-MM-dd";
                String date = (String) object.get("tradingDay");
                calendar.setTime(new SimpleDateFormat(pattern).parse(date));
                quotes.add(quote);
            }

            Collections.sort(quotes, new Comparator<HistoricalQuote>() {
                @Override
                public int compare(HistoricalQuote o1, HistoricalQuote o2) {
                    return o2.getDate().getTime().compareTo(o1.getDate().getTime());
                }
            });

        } catch (Exception e) {
            logger.error("Failed to download {} ", symbol, e);
            return new ArrayList<>();
        }
        return quotes;
    }


    private String buildUrl(String symbol) {
        StringBuilder builder = new StringBuilder();
        builder.append(BASE_URL);
        builder.append(API_KEY);
        builder.append("&");
        builder.append("symbol=");
        builder.append(symbol);
        builder.append("&");
        builder.append("type=daily");
        builder.append("&");
        builder.append("startDate=");
        builder.append("20100101");
        builder.append("&");
        builder.append("endDate=");
        builder.append(new SimpleDateFormat("yyMMdd").format(new Date()).toString());
        builder.append("&maxRecords=500&interval=60&order=asc&sessionFilter=EFK&splits=true");
        builder.append("&dividends=true&volume=sum&nearby=1&jerq=true&exchange=NYSE%2CAMEX%2CNASDAQ&backAdjust=false&daysToExpiration=1&contractRoll=expiration");
        return builder.toString();
    }


    public static void main(String[] args) throws Exception {
        BarchartDownloader downloader = new BarchartDownloader();
        String url = downloader.buildUrl("AAPL");
        List<HistoricalQuote> quotes = downloader.download("AAPL");
        System.out.println(quotes);

    }
}


