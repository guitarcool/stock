package downloaders.morningstar;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by haiyanluo on 5/26/19.
 */
public class MorningstarDownloader {
    private static final Logger logger = LoggerFactory.getLogger(MorningstarDownloader.class);
    private static final String BASE_PATH = "/Users/haiyanluo/workspace/stock/crawler/panda/data/";

    private static MorningstarDownloader instance = new MorningstarDownloader();
    private MorningstarDownloader() {}

    public static MorningstarDownloader getInstance()
    {
        return instance;
    }

    public static List<HistoricalQuote> getHistoricQuotes(String filePath) throws  Exception {
        List<HistoricalQuote> quotes = MorningstarDownloader.getInstance().download(filePath);
        // Collections.reverse(quotes); // already reversed
        return quotes;
    }

    public static String getFilePathFromSymbol(String symbol) {
        return BASE_PATH + symbol + ".csv";
        // Path path = Paths.get(fulFilePath);
        // return path.getFileName().toString().split("\\.")[0].toUpperCase();
    }

    // This is really just read the historic data from the file, with the input the full path name
    public List<HistoricalQuote> download(String symbol) {
        String fullFilePath = getFilePathFromSymbol(symbol);

        List<HistoricalQuote> quotes = new ArrayList<>();
        try {
            final Reader reader = new BufferedReader(new FileReader(fullFilePath));
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader());
            for (final CSVRecord record : parser) {
                HistoricalQuote quote = new HistoricalQuote();
                Iterator<String> iterator = record.iterator();

                String date = iterator.next();
                double high = Double.valueOf(iterator.next());
                double low = Double.valueOf(iterator.next());
                double open = Double.valueOf(iterator.next());
                double close = Double.valueOf(iterator.next());
                double volume = Double.valueOf(iterator.next());
                double adjClose = Double.valueOf(iterator.next());
                // ignore OpenInt

                quote.setOpen(new BigDecimal(open));
                quote.setHigh(new BigDecimal(high));
                quote.setLow(new BigDecimal(low));
                quote.setClose(new BigDecimal(close));
                quote.setVolume((long) volume);
                quote.setAdjClose(new BigDecimal(adjClose));
                quote.setSymbol(symbol);
                Calendar calendar = Calendar.getInstance();
                quote.setDate(calendar);
                String pattern = "yyyy-MM-dd";
                calendar.setTime(new SimpleDateFormat(pattern).parse(date));
                quotes.add(quote);
            }

        } catch (Exception e) {
            logger.error("Failed to download {} ", symbol, e);
            return new ArrayList<>();
        }
        Collections.sort(quotes, new Comparator<HistoricalQuote>() {
            @Override
            public int compare(HistoricalQuote o1, HistoricalQuote o2) {
                return o2.getDate().getTime().compareTo(o1.getDate().getTime());
            }
        });
        return quotes;
    }

    public static void main(String[] args) {
        List<HistoricalQuote> result = MorningstarDownloader.getInstance().download("AAPL");
        System.out.println(result);
    }


}
