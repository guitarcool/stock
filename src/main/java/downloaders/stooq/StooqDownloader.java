package downloaders.stooq;

/**
 * Created by haiyanluo on 11/10/17.
 */

import com.efinancialpal.couchbase.CouchbaseKeyValueCache;
import com.efinancialpal.couchbase.StockRecordData;
import com.efinancialpal.strategy.EvaluationContext;
import com.efinancialpal.strategy.Strategy;
import com.efinancialpal.strategy.sell.EmbraceShortStrategy;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class StooqDownloader {
    private static final Logger logger = LoggerFactory.getLogger(StooqDownloader.class);

    public static final String BASE_PATH = "/Volumes/Transcend/Downloads/data/daily/us";
    private static final String STOCK_HISTORIC_DATA_REFRESHED_TIMESTAMP_KEY = "stock_historic_data_refreshed_timestamp";
    private final static long ONE_DAY_IN_MS = (24 - 10) * 60 * 60 * 1000; // sometimes I run this less than 24 hours away

    private static StooqDownloader instance = new StooqDownloader();
    private StooqDownloader() {}

    public static StooqDownloader getInstance()
    {
        return instance;
    }
    public static List<String> getFileNames(List<String> fileNames, Path dir) {
        try(DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path path : stream) {
                if(path.toFile().isDirectory()) {
                    getFileNames(fileNames, path);
                } else {
                    if (!path.getFileName().toString().contains("._")) {
                        // skip hidden files
                        fileNames.add(path.toAbsolutePath().toString());
                    }
                    // System.out.println(path.getFileName());
                }
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
        return fileNames;
    }

    public static List<HistoricalQuote> getHistoricQuotes(String filePath) throws  Exception {
        List<HistoricalQuote> quotes = StooqDownloader.getInstance().download(filePath);
        // Collections.reverse(quotes); // already reversed
        return quotes;
    }

    public static List<String> loadAndStoreFullStockData() throws  Exception {
        List<String> candidateStocks = new ArrayList<>();
        boolean needRefresh = needToRefreshStockData();
        List<String> stockFiles = new ArrayList<>();
        getFileNames(stockFiles, Paths.get(BASE_PATH));
        if (needRefresh) {
            logger.error("Loading {} stocks historic data", stockFiles.size());
        } else {
            logger.info("No need to load stocks historic data");
        }
        for (String file : stockFiles) {
            String symbol = getSymbolFromFullPath(file);
            if (needRefresh) {
                List<HistoricalQuote> quotes = StooqDownloader.getInstance().download(file);
                StockRecordData.getInstance().set(symbol, quotes);
            }
            candidateStocks.add(symbol);
        }
        logger.info("found {} candidate stocks", candidateStocks.size());
        return candidateStocks;
    }

    public static boolean needToRefreshStockData() {
        try {
            String lastTickerUpdatedTimestamp = CouchbaseKeyValueCache.getInstance().get(STOCK_HISTORIC_DATA_REFRESHED_TIMESTAMP_KEY);
            if (lastTickerUpdatedTimestamp != null) {
                final Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(Long.valueOf(lastTickerUpdatedTimestamp));
                final String timeString =
                        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(cal.getTime());
                logger.info("Historic data were updated last time at {}", timeString);
            } else {
                logger.info("Historic data were not updated before, first time this is being run?");
            }
            if (lastTickerUpdatedTimestamp == null
                    || (System.currentTimeMillis() - Long.valueOf(lastTickerUpdatedTimestamp) > ONE_DAY_IN_MS)) {
                return true;
            }
        } catch (Exception e) {
            logger.error("Failed to read and refresh tickers ", e);
            throw  e;
        }
        return false;
    }

    public static String getSymbolFromFullPath(String fulFilePath) {
        Path path = Paths.get(fulFilePath);
        return path.getFileName().toString().split("\\.")[0].toUpperCase();
    }

    // This is really just read the historic data from the file, with the input the full path name
    public List<HistoricalQuote> download(String fulFilePath) {
        String symbol = getSymbolFromFullPath(fulFilePath);

        List<HistoricalQuote> quotes = new ArrayList<>();
        try {
            final Reader reader = new BufferedReader(new FileReader(fulFilePath));
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader());
            for (final CSVRecord record : parser) {
                HistoricalQuote quote = new HistoricalQuote();
                Iterator<String> iterator = record.iterator();

                String date = iterator.next();
                double open = Double.valueOf(iterator.next());
                double high = Double.valueOf(iterator.next());
                double low = Double.valueOf(iterator.next());
                double close = Double.valueOf(iterator.next());
                double volume = Double.valueOf(iterator.next());
                // ignore OpenInt

                quote.setOpen(new BigDecimal(open));
                quote.setHigh(new BigDecimal(high));
                quote.setLow(new BigDecimal(low));
                quote.setClose(new BigDecimal(close));
                quote.setVolume((long) volume);
                quote.setSymbol(symbol);
                Calendar calendar = Calendar.getInstance();
                quote.setDate(calendar);
                String pattern = "yyyy-MM-dd";
                calendar.setTime(new SimpleDateFormat(pattern).parse(date));
                quotes.add(quote);
            }

        } catch (Exception e) {
            logger.error("Failed to download {} ", symbol, e);
            return new ArrayList<>();
        }
        Collections.sort(quotes, new Comparator<HistoricalQuote>() {
            @Override
            public int compare(HistoricalQuote o1, HistoricalQuote o2) {
                return o2.getDate().getTime().compareTo(o1.getDate().getTime());
            }
        });
        return quotes;
    }

    /*
    CsvReader products = new CsvReader("products.csv");

			products.readHeaders();

			while (products.readRecord())
    {
        String productID = products.get("ProductID");
        String productName = products.get("ProductName");
        String supplierID = products.get("SupplierID");
        String categoryID = products.get("CategoryID");
        String quantityPerUnit = products.get("QuantityPerUnit");
        String unitPrice = products.get("UnitPrice");
        String unitsInStock = products.get("UnitsInStock");
        String unitsOnOrder = products.get("UnitsOnOrder");
        String reorderLevel = products.get("ReorderLevel");
        String discontinued = products.get("Discontinued");

        // perform program logic here
        System.out.println(productID + ":" + productName);
    }

			products.close();

} catch (FileNotFoundException e) {
        e.printStackTrace();
        } catch (IOException e) {
        e.printStackTrace();
        }

*/

    public static void main(String[] args) throws Exception {
        // loadFullStockData();

        // List<HistoricalQuote> quotes = quandl.QuandlDownloader.getInstance().download("MSFT");
        String filePath = "/Volumes/Transcend/Downloads/data/daily/us/nyse etfs/ura.us.txt";
        List<HistoricalQuote> quotes = StooqDownloader.getInstance().download(filePath);
        List<HistoricalQuote> newQuotes = new ArrayList<>();
        String dateStr = "02/15/2017";
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date startDate = (Date)formatter.parse(dateStr);

        for (HistoricalQuote quote : quotes) {
            if (quote.getDate().getTime().compareTo(startDate) <= 0) {
               newQuotes.add(quote);
            }
        }
        Strategy strategy = new EmbraceShortStrategy();
        EvaluationContext evaluationContext = new EvaluationContext();
        System.out.println(strategy.meet("URA", newQuotes, evaluationContext));
    }
}


