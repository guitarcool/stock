package downloaders.alphavantage;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.util.MyInsecureHttpClient;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by haiyanluo on 8/9/18.
 */
public class AlphaVantageDownloader {
    private static final Logger logger = LoggerFactory.getLogger(AlphaVantageDownloader.class);

    private static final String API_KEY = "I5COPG4JK7FJZ2HP";
    private static final String BASE_URL = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&apikey=I5COPG4JK7FJZ2HP&outputsize=full&symbol=";

    private static AlphaVantageDownloader instance = new AlphaVantageDownloader();
    private AlphaVantageDownloader() {}

    public static AlphaVantageDownloader getInstance() {
        return instance;
    }

    public List<HistoricalQuote> download(String symbol) {
        List<HistoricalQuote> quotes = new ArrayList<>();
        try {
            String url = buildUrl(symbol);
            String result = MyInsecureHttpClient.getInstance().sendGetRequest(url);
            JSONObject jsonObject = new JSONObject(result);

            JSONObject timeseries = (JSONObject) jsonObject.get("Time Series (Daily)");
            Iterator<?> keys = timeseries.keys();

            while( keys.hasNext() ) {
                HistoricalQuote quote = new HistoricalQuote();
                String date = (String) keys.next();
                JSONObject valueObj = (JSONObject) timeseries.get(date);
                // System.out.print(valueObj);

                Iterator<?> itemIterator = valueObj.keys();
                while ( itemIterator.hasNext() ) {
                    String item = (String) itemIterator.next();
                    String value = valueObj.getString(item);

                    if (item.contains("open")) {
                        quote.setOpen(new BigDecimal(value));
                    } else if (item.contains("close")) {
                        quote.setClose(new BigDecimal(value));
                    } else if (item.contains("high")) {
                        quote.setHigh(new BigDecimal(value));
                    } else if (item.contains("low")) {
                        quote.setLow(new BigDecimal(value));
                    } else if (item.contains("volume")) {
                        quote.setVolume(Long.valueOf(value));
                    }
                }
                Calendar calendar = Calendar.getInstance();
                quote.setDate(calendar);
                String pattern = "yyy-MM-dd";
                calendar.setTime(new SimpleDateFormat(pattern).parse(date));
                quotes.add(quote);
            }


            Collections.sort(quotes, new Comparator<HistoricalQuote>() {
                @Override
                public int compare(HistoricalQuote o1, HistoricalQuote o2) {
                    return o2.getDate().getTime().compareTo(o1.getDate().getTime());
                }
            });

        } catch (Exception e) {
            logger.error("Failed to download {} ", symbol, e);
            return new ArrayList<>();
        }
        return quotes;
    }

    /*
    CsvReader products = new CsvReader("products.csv");

			products.readHeaders();

			while (products.readRecord())
    {
        String productID = products.get("ProductID");
        String productName = products.get("ProductName");
        String supplierID = products.get("SupplierID");
        String categoryID = products.get("CategoryID");
        String quantityPerUnit = products.get("QuantityPerUnit");
        String unitPrice = products.get("UnitPrice");
        String unitsInStock = products.get("UnitsInStock");
        String unitsOnOrder = products.get("UnitsOnOrder");
        String reorderLevel = products.get("ReorderLevel");
        String discontinued = products.get("Discontinued");

        // perform program logic here
        System.out.println(productID + ":" + productName);
    }

			products.close();

} catch (FileNotFoundException e) {
        e.printStackTrace();
        } catch (IOException e) {
        e.printStackTrace();
        }

*/

    private String buildUrl(String symbol) {
        StringBuilder builder = new StringBuilder();
        builder.append(BASE_URL);
        builder.append(symbol);
        return builder.toString();
    }


    public static void main(String[] args) {
        System.setProperty("https.protocols", "TLSv1");
        List<HistoricalQuote> quotes = AlphaVantageDownloader.getInstance().download("MSFT");
        System.out.println(quotes);
    }
}
