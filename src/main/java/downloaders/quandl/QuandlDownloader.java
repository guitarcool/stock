package downloaders.quandl;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by haiyanluo on 10/7/17.
 */
public class QuandlDownloader {
    private static final Logger logger = LoggerFactory.getLogger(QuandlDownloader.class);

    private static final String API_KEY = "WFmaZ8wUHTN9HLADgA7g";
    private static final String BASE_URL = "https://www.quandl.com/api/v3/datasets/EOD/";

    private static QuandlDownloader instance = new QuandlDownloader();
    private QuandlDownloader() {}

    public static QuandlDownloader getInstance() {
        return instance;
    }

    public List<HistoricalQuote> download(String symbol) {
        List<HistoricalQuote> quotes = new ArrayList<>();
        try {
            String url = buildUrl(symbol);
            final Reader reader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader());
            for (final CSVRecord record : parser) {
                HistoricalQuote quote = new HistoricalQuote();
                String date = record.get("Date");
                double open = Double.valueOf(record.get("Open"));
                double high = Double.valueOf(record.get("High"));
                double low = Double.valueOf(record.get("Low"));
                double close = Double.valueOf(record.get("Close"));
                double volume = Double.valueOf(record.get("Volume"));
                double dividend = Double.valueOf(record.get("Dividend"));
                double split = Double.valueOf(record.get("Split"));
                double adjOpen = Double.valueOf(record.get("Adj_Open"));
                double adjHigh = Double.valueOf(record.get("Adj_High"));
                double adjLow = Double.valueOf(record.get("Adj_Low"));
                double adjClose = Double.valueOf(record.get("Adj_Close"));
                double adjVolume = Double.valueOf(record.get("Adj_Volume"));
                quote.setOpen(new BigDecimal(open));
                quote.setHigh(new BigDecimal(high));
                quote.setLow(new BigDecimal(low));
                quote.setClose(new BigDecimal(close));
                quote.setVolume((long) volume);
                quote.setAdjClose(new BigDecimal(adjClose));
                quote.setSymbol(symbol);
                Calendar calendar = Calendar.getInstance();
                quote.setDate(calendar);
                String pattern = "yyyy-MM-dd";
                calendar.setTime(new SimpleDateFormat(pattern).parse(date));
                quotes.add(quote);
            }

        } catch (Exception e) {
            logger.error("Failed to download {} ", symbol, e);
            return new ArrayList<>();
        }
        return quotes;
    }

    /*
    CsvReader products = new CsvReader("products.csv");

			products.readHeaders();

			while (products.readRecord())
    {
        String productID = products.get("ProductID");
        String productName = products.get("ProductName");
        String supplierID = products.get("SupplierID");
        String categoryID = products.get("CategoryID");
        String quantityPerUnit = products.get("QuantityPerUnit");
        String unitPrice = products.get("UnitPrice");
        String unitsInStock = products.get("UnitsInStock");
        String unitsOnOrder = products.get("UnitsOnOrder");
        String reorderLevel = products.get("ReorderLevel");
        String discontinued = products.get("Discontinued");

        // perform program logic here
        System.out.println(productID + ":" + productName);
    }

			products.close();

} catch (FileNotFoundException e) {
        e.printStackTrace();
        } catch (IOException e) {
        e.printStackTrace();
        }

*/

private String buildUrl(String symbol) {
        StringBuilder builder = new StringBuilder();
        builder.append(BASE_URL);
        builder.append(symbol);
        builder.append(".csv?api_key=");
        builder.append(API_KEY);
        return builder.toString();
    }


    public static void main(String[] args) {
        List<HistoricalQuote> quotes = QuandlDownloader.getInstance().download("MSFT");
        System.out.println(quotes);
    }
}
