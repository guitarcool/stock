package downloaders.googlefinance;

/**
 * Created by haiyanluo on 11/3/17.
 */

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.histquotes.HistoricalQuote;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class GoogleDownloader {
    private static final Logger logger = LoggerFactory.getLogger(GoogleDownloader.class);

    private static final String BASE_URL = "https://finance.google.com/finance/historical?q=";

    private static GoogleDownloader instance = new GoogleDownloader();
    private GoogleDownloader() {}

    public static GoogleDownloader getInstance() {
        return instance;
    }

    public List<HistoricalQuote> download(String symbol) {
        List<HistoricalQuote> quotes = new ArrayList<>();
        try {
            String url = buildUrl(symbol);
            final Reader reader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader());
            for (final CSVRecord record : parser) {
                HistoricalQuote quote = new HistoricalQuote();
                Iterator<String> iterator = record.iterator();
                /* TODO: there is an issue the title of the Downloaded CSV, that can't match with 'Date', weird
                String date = record.get("Date");
                double open = Double.valueOf(record.get("Open"));
                double high = Double.valueOf(record.get("High"));
                double low = Double.valueOf(record.get("Low"));
                double close = Double.valueOf(record.get("Close"));
                double volume = Double.valueOf(record.get("Volume"));
                */
                String date = iterator.next();
                double open = Double.valueOf(iterator.next());
                double high = Double.valueOf(iterator.next());
                double low = Double.valueOf(iterator.next());
                double close = Double.valueOf(iterator.next());
                double volume = Double.valueOf(iterator.next());

                quote.setOpen(new BigDecimal(open));
                quote.setHigh(new BigDecimal(high));
                quote.setLow(new BigDecimal(low));
                quote.setClose(new BigDecimal(close));
                quote.setVolume((long) volume);
                quote.setSymbol(symbol);
                Calendar calendar = Calendar.getInstance();
                quote.setDate(calendar);
                String pattern = "dd-MMM-yy";
                calendar.setTime(new SimpleDateFormat(pattern).parse(date));
                quotes.add(quote);
            }

        } catch (Exception e) {
            logger.error("Failed to download {} ", symbol, e);
            return new ArrayList<>();
        }
        return quotes;
    }

    /*
    CsvReader products = new CsvReader("products.csv");

			products.readHeaders();

			while (products.readRecord())
    {
        String productID = products.get("ProductID");
        String productName = products.get("ProductName");
        String supplierID = products.get("SupplierID");
        String categoryID = products.get("CategoryID");
        String quantityPerUnit = products.get("QuantityPerUnit");
        String unitPrice = products.get("UnitPrice");
        String unitsInStock = products.get("UnitsInStock");
        String unitsOnOrder = products.get("UnitsOnOrder");
        String reorderLevel = products.get("ReorderLevel");
        String discontinued = products.get("Discontinued");

        // perform program logic here
        System.out.println(productID + ":" + productName);
    }

			products.close();

} catch (FileNotFoundException e) {
        e.printStackTrace();
        } catch (IOException e) {
        e.printStackTrace();
        }

*/

    private String buildUrl(String symbol) {
        StringBuilder builder = new StringBuilder();
        builder.append(BASE_URL);
        builder.append(symbol);
        builder.append("&output=csv");
        return builder.toString();
    }


    public static void main(String[] args) {
        // List<HistoricalQuote> quotes = quandl.QuandlDownloader.getInstance().download("MSFT");
        List<HistoricalQuote> quotes = GoogleDownloader.getInstance().download("MSFT");
        System.out.println(quotes);
    }
}

