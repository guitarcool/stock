import urllib2
import sys
from bs4 import BeautifulSoup

BASE_URL = "https://finviz.com/quote.ashx?t="

def scrub_fa_data(symbol):
    url = BASE_URL + symbol
    response = urllib2.urlopen(url)
    html=response.read()
    soup = BeautifulSoup(html, "html.parser")
    table = soup.find("table", {"class": "snapshot-table2"})
    rows = table.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        index = 0
        while index < len(cols):
            print cols[index] + ":" + cols[index + 1]
            index = index + 2
        

def main():
    scrub_fa_data("FB")

if __name__ == '__main__':
    main()
