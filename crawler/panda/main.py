from pandas_datareader import data
import csv
import time

start_date_str = ('2015-1-1')
end_date_str = time.strftime("%Y-%m-%d") 
DATA_SOURCE = 'yahoo'
BASE_PATH = '/Users/haiyanluo/workspace/stock/tickers/'

symbol_files = ['nasdaq.csv', 'nyse.csv', 'ETFList.csv']


def load_symbols(symbol_files):
    stocks = []
    for filename in symbol_files:
        with open(BASE_PATH + filename) as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                stocks.append(row[0].strip())
    return stocks

def main():
    stocks = load_symbols(symbol_files)
    print ("total {0} symbol".format(len(stocks)))
    for i in stocks:
        try: 
            print ("Downloading {0}".format(i))
            data.DataReader(i,start=start_date_str,end=end_date_str,data_source=DATA_SOURCE).to_csv(i+'.csv')
        except:
            pass
    """
    for stock in stocks:
        result = data.DataReader(stock, 
                       start=start_date_str
                       end=end_data_str
                       data_source=DATA_SOURCE)
    """

if __name__ == '__main__':
    main()
