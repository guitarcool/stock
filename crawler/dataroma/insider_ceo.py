import urllib2
import sys
from bs4 import BeautifulSoup
import json
from yahoo_finance import Share 
from DataromaEntry import DataromaEntry
import time
import os
from datetime import datetime, timedelta

# find candidate stock during the period, format: 2017-01-01
FROM_DATE = os.getenv("FROM_DATE")
TO_DATE = os.getenv("TO_DATE")
if (FROM_DATE is None or TO_DATE is None):
    print "FROM_DATE or TO_DATE is not set, setting it to default"
    from_time = datetime.now() - timedelta(days=5)
    FROM_DATE = from_time.strftime("%Y-%m-%d")
    TO_DATE = time.strftime("%Y-%m-%d") 

print "FROM_DATE: " + FROM_DATE
print "TO_DATE: " + TO_DATE

# historic details for given stock
HISTORIC_FROM_DATE = '2017-10-01'

SALE = "Sale"
PURCHASE = "Purchase"
DIRECT = "D"
INDIRECT = "I"
LOW_PRICE_STOCK = 3
HIGH_PRICE_STOCK = 50
AMOUNT_THRESHOLD = 100000

direct_buy_map = {}
final_result = {}
result_set = set()

def get_candidates():
    index = 1
    # [u'\u2261', u'03 Feb 20:23', u'PEN', u'Penumbra Inc', u'PRAY JAMES ROBERT', u'President, International', u'01 Feb 2017', u'Sale', u'900', u'71.42', u'64,278', u'D']
    while (True):
        try: 
            url = 'http://www.dataroma.com/m/ins/ins.php?t=w&fr=' + FROM_DATE + '&to=' + TO_DATE + '&am=0&sym=&o=fd&d=d&L=' + str(index)
            print "getting: " + url
            response = urllib2.urlopen(url)
            html=response.read()
            soup = BeautifulSoup(html, "html.parser")
            table = soup.find("table", {"id": "grid"})
            table_body = table.find('tbody')
            rows = table_body.find_all('tr')
            if (len(rows) <= 0):
                break
            for row in rows:
                cols = row.find_all('td')
                cols = [ele.text.strip() for ele in cols]
                if (len(cols) < 12):
                    continue

                entry = decode_entry(cols)
                if DIRECT in entry.is_direct and PURCHASE in entry.action:
                    if entry.symbol not in direct_buy_map and ('CEO' in entry.title or 'Chief' in entry.title or 'President' in entry.title) and entry.amount >= 500000 and entry.price >= 5: 
                        result_set.add(entry.symbol)
            index = index + 1
        except OSError as e:
            print e
            sys.exit()

def decode_entry(cols):
    date = cols[1]
    symbol = cols[2]
    company = cols[3]
    buyer = cols[4]
    title = cols[5]
    transaction_date = cols[6]
    action = cols[7] # Purchase or Sale
    shares = cols[8]
    price = float(cols[9].replace(",", ""))
    amount = float(cols[10].replace(",", ""))
    is_direct = cols[11]
    entry = DataromaEntry(date, symbol, company, buyer, title, transaction_date, action, shares, price, amount, is_direct) 
    return entry

def main():
    get_candidates()
    # print "Found " + str(len(result_set)) + " candidates"
    result_str = "insider ceo: " + ','.join(result_set)
    # print result_str
    return result_str 

def filtering():
    for symbol in sell_set:
        # delete it as long as there is a sell
        if symbol in direct_buy_map:
            print "delete " + symbol
            del direct_buy_map[symbol]
    for symbol, entries in direct_buy_map.items():
        if symbol.strip() is None:
            del direct_buy_map[symbol]
            continue
        try:
            stock = Share(symbol)
            stock_price = float(stock.get_price())
            if stock_price < LOW_PRICE_STOCK:
                del direct_buy_map[symbol]
                continue
            #if stock.get_price_earnings_ratio() < 0:
            #    del direct_buy_map[symbol]
            #    continue
            #if stock_price < stock.get_200day_moving_avg():
            #    del direct_buy_map[symbol]
            #    continue
            total_amount = 0
            for entry in entries:
                if entry.price / stock_price > 0.8:
                    # lower price option with 10% off the current price is eliminated
                    total_amount += entry.amount 
            if total_amount < AMOUNT_THRESHOLD:
                del direct_buy_map[symbol]
                continue
        except TypeError as e:
            print e
        except Exception as e:
            print e

def print_results():
    result = ""
    for symbol, entries in direct_buy_map.items():
        if symbol.strip() is None:
            continue
        result = result + symbol + " " 
    print result

def output_results():
    file_name = time.strftime("%Y%m%d_insider.txt")
    with open(file_name, "w") as output:
        for symbol, entries in direct_buy_map.items():
            if symbol.strip() is not None:
                output.write(symbol + os.linesep)

# print out details of result
def print_details(direct_buy_maps):
    data = {}
    for symbol, amount in direct_buy_map.items():
        if symbol.strip() is None:
            continue
        if amount > AMOUNT_THRESHOLD:
            print "symbol: " + symbol + ", amount: " + str(amount)
            pg = 1
            all_cols = []
            while (True): 
                url = 'http://www.dataroma.com/m/ins/ins.php?t=y&fr=' + HISTORIC_FROM_DATE + '&to=' + TO_DATE + '&am=0&sym=' + symbol + '&o=fd&d=d&L=' + str(pg)
                response = urllib2.urlopen(url) 
                html=response.read()
                soup = BeautifulSoup(html, "html.parser")
                table = soup.find("table", {"id": "grid"})
                table_body = table.find('tbody')
                rows = table_body.find_all('tr')
                if (len(rows) <= 0):
                    break
                for row in rows:
                    tmp_cols = row.find_all('td')
                    tmp_cols = [ele.text.strip() for ele in tmp_cols]
                    if (len(tmp_cols) < 12):
                        continue
                    all_cols.append(tmp_cols)
                #print json.dumps(all_cols)
                pg = pg + 1
            data[symbol] = all_cols
    print json.dumps(data)

if __name__ == '__main__':
    main()

