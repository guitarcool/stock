import time
import finviz
import insider_ceo
import send_email

date_str = time.strftime("%Y%m%d")

def output_results(result_str):
    file_name = date_str + ".txt"
    with open(file_name, "w") as output:
        output.write(result_str)

if __name__ == '__main__':
    insider_result = insider_ceo.main()
    result_str = insider_result + "\n" + finviz.main()
    print result_str
    output_results(result_str)
    send_email.send_emails(date_str, result_str)
    print "Done!"
