class DataromaEntry:
    def __init__(self, date, symbol, company, buyer, title, transaction_date, action, shares, price, amount, is_direct):
        self.date = date
        self.symbol = symbol
        self.company = company
        self.buyer = buyer
        self.title = title
        self.transaction_date = transaction_date
        self.action = action
        self.shares = shares
        self.price = price
        self.amount = amount
        self.is_direct = is_direct
