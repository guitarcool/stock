import urllib2
import sys
from bs4 import BeautifulSoup
import json
from DataromaEntry import DataromaEntry
import time
import os

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"

SALE = "Sale"
PURCHASE = "Purchase"
DIRECT = "D"
INDIRECT = "I"
LOW_PRICE_STOCK = 3
HIGH_PRICE_STOCK = 50
AMOUNT_THRESHOLD = 100000

gap_up_result_set = set()
huge_volume_result_set = set()
hammer_result_set = set()

GAP_UP_URL = "https://www.finviz.com/screener.ashx?v=111&f=cap_midover,sh_curvol_o200,sh_price_o20,sh_relvol_o2,ta_gap_u3,ta_sma200_pa&ft=4"
HUGE_VOLUME_URL = "https://www.finviz.com/screener.ashx?v=111&f=cap_midover,sh_curvol_o500,sh_price_o20,sh_relvol_o3,ta_sma200_pa,ta_sma50_pa&ft=4"
HAMMER_URL = "https://www.finviz.com/screener.ashx?v=111&f=cap_midover,sh_curvol_o200,sh_price_o20,ta_candlestick_h,ta_sma200_pa&ft=4"

def get_candidates(url, result_set):
    print "scraping: " + url
    req = urllib2.Request(url, data = "abc", headers = {'User-Agent' : USER_AGENT})
    response = urllib2.urlopen(req)
    html = response.read()
    get_stock_symbols(html, result_set)

def get_stock_symbols(html, result_set):
    soup = BeautifulSoup(html, "html.parser")
    div = soup.find("div", {"id": "screener-content"})
    all_a = div.find_all("a", {"class": "screener-link-primary"})
    for a in all_a:
        result_set.add(a.text)

def main():
    get_candidates(GAP_UP_URL, gap_up_result_set)
    get_candidates(HUGE_VOLUME_URL, huge_volume_result_set)
    get_candidates(HAMMER_URL, hammer_result_set)
    gap_up = 'gap up: ' + ','.join(gap_up_result_set)
    huge_volume = 'huge volume: ' + ','.join(huge_volume_result_set)
    hammer = 'hammer: ' + ','.join(hammer_result_set)
    # print gap_up
    # print huge_volume
    # print hammer
    return gap_up + "\n" + huge_volume + "\n" + hammer

if __name__ == '__main__':
    main()

