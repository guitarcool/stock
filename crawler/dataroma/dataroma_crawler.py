from bs4 import BeautifulSoup
import html5lib
from html5lib import sanitizer
from html5lib import treebuilders
import urllib2
import codecs

page = 1
file = codecs.open("dump.txt", "w", "utf-8")
end_of_page = 1
while page <= end_of_page:
    print "processing page: " + str(page)
    url = "http://www.dataroma.com/m/home.php"
    print "sending request to " + url
    fp = urllib2.urlopen(url)
    soup = BeautifulSoup(fp.read(), "html5lib")
    #print(soup.prettify())
    #python len(soup.findAll('table'))
    div = soup.find("div", {"id": "ins"})
    for tr in div.findAll('table')[0].findAll('tr'):
        for td in tr.findAll('td'):
            line = td.text.strip()
            print line
            file.write(line)
            file.write("\n")
            
    #for child_tr in tr.findAll('tbody')[0].findAll('tr'):
    #    file.write(child_tr.text)
    #fp.close()

    page = page + 1

file.close();
