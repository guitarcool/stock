drop database if exists stock;
create database stock;
USE stock;

DROP TABLE IF EXISTS nasdaq_ticker;
DROP TABLE IF EXISTS nyse_ticker;
DROP TABLE IF EXISTS stock_stats;
DROP TABLE IF EXISTS invest_institution;
DROP TABLE IF EXISTS institution_holdings;
DROP TABLE IF EXISTS sector;

DROP FUNCTION IF EXISTS nextval;

DROP TRIGGER IF EXISTS nasdaq_ticker.nasdaq_ticker_insert_version;
DROP TRIGGER IF EXISTS nasdaq_ticker.nasdaq_ticker_update_version;
DROP TRIGGER IF EXISTS nyse_ticker.nyse_ticker_insert_version;
DROP TRIGGER IF EXISTS nyse_ticker.nyse_ticker_update_version;
DROP TRIGGER IF EXISTS stock_stats.stock_stats_insert_version;
DROP TRIGGER IF EXISTS stock_stats.stock_stats_update_version;
DROP TRIGGER IF EXISTS ticker.ticker_insert_version;
DROP TRIGGER IF EXISTS ticker.ticker_update_version;
DROP TRIGGER IF EXISTS invest_institution.invest_institution_insert_version;
DROP TRIGGER IF EXISTS invest_institution.invest_institution_update_version;
DROP TRIGGER IF EXISTS institution_holdings.institution_holdings_insert_version;
DROP TRIGGER IF EXISTS institution_holdings.institution_holdings_update_version;
DROP TRIGGER IF EXISTS sector.sector_insert_version;
DROP TRIGGER IF EXISTS sector.sector_update_version;

CREATE TABLE IF NOT EXISTS sequences(
    name CHAR(30) PRIMARY KEY,
    val INT UNSIGNED
) ENGINE=INNODB;

DELIMITER $$
CREATE FUNCTION nextval (seqname CHAR(30))
RETURNS INT UNSIGNED
BEGIN
    INSERT INTO sequences VALUES (seqname,LAST_INSERT_ID(1))
    ON DUPLICATE KEY UPDATE val=LAST_INSERT_ID(val+1);
    RETURN LAST_INSERT_ID();
END;
$$
DELIMITER ;

CREATE TABLE nasdaq_ticker (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    symbol varchar(16) NOT NULL,
    name varchar(256) NOT NULL,
    last_sale decimal(30,6),
    market_cap decimal(30,6),
    ipo_year varchar(8),
    sector varchar(64),
    industry varchar(256),
    summary_quote varchar(1024),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE ticker (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    symbol varchar(16) NOT NULL,
    name varchar(256) NOT NULL,
    last_sale decimal(30,6),
    market_cap decimal(30,6),
    ipo_year varchar(8),
    sector varchar(64),
    industry varchar(256),
    summary_quote varchar(1024),
    exchange enum('nasdaq','nyse') NOT NULL,
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE sector (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    symbol varchar(16) NOT NULL,
    name varchar(256) NOT NULL,
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE nyse_ticker (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    symbol varchar(16) NOT NULL,
    name varchar(256) NOT NULL,
    last_sale decimal(30,6),
    market_cap decimal(30,6),
    ipo_year varchar(8),
    sector varchar(64),
    industry varchar(256),
    summary_quote varchar(1024),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE stock_stats (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    market_cap decimal(30,6),
    shares_float bigint(20),
    shares_outstanding bigint(20),
    shares_owned bigint(20),
    eps decimal(30,6),
    pe decimal(30,6),
    peg decimal(30,6),
    eps_estimate_current_year decimal(30,6),
    eps_estimate_next_quarter decimal(30,6),
    eps_estimate_next_year decimal(30,6),
    price_book decimal(30,6),
    price_sales decimal(30,6),
    book_value_per_share decimal(30,6),
    revenue decimal(30,6),
    ebitda decimal(30,6),
    one_year_target_price decimal(30,6),
    short_ratio decimal(30,6),
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE invest_institution (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT, 
    name varchar(256) NOT NULL,
    portfolio_value decimal(30,6),
    no_of_stocks int(10),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE institution_holdings (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    institution_id int(10) unsigned NOT NULL,
    ticker varchar(16) NOT NULL,
    stock_name varchar(256),
    percentage  float,
    shares bigint(20),
    recent_actitity enum('Reduce', 'Add', 'Buy') NOT NULL,
    activity_percentage float,
    reported_price float,
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id),
    FOREIGN KEY(institution_id) REFERENCES stock.invest_institution(id)
) ENGINE=INNODB;

DELIMITER $$
CREATE TRIGGER nasdaq_ticker_update_version
BEFORE UPDATE
ON nasdaq_ticker
for each row BEGIN
    set new.version = nextval('nasdaq_ticker');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER nasdaq_ticker_insert_version
BEFORE INSERT
ON nasdaq_ticker
for each row BEGIN
    set new.version = nextval('nasdaq_ticker');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER nyse_ticker_update_version
BEFORE UPDATE
ON nyse_ticker
for each row BEGIN
    set new.version = nextval('nyse_ticker');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER nyse_ticker_insert_version
BEFORE INSERT
ON nyse_ticker
for each row BEGIN
    set new.version = nextval('nyse_ticker');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER stock_stats_update_version
BEFORE UPDATE
ON stock_stats
for each row BEGIN
    set new.version = nextval('stock_stats');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER stock_stats_insert_version
BEFORE INSERT
ON stock_stats
for each row BEGIN
    set new.version = nextval('stock_stats');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER ticker_update_version
BEFORE UPDATE
ON ticker
for each row BEGIN
    set new.version = nextval('ticker');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER ticker_insert_version
BEFORE INSERT
ON ticker
for each row BEGIN
    set new.version = nextval('ticker');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER invest_institution_update_version
BEFORE UPDATE
ON invest_institution
for each row BEGIN
    set new.version = nextval('invest_institution');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER invest_institution_insert_version
BEFORE INSERT
ON invest_institution
for each row BEGIN
    set new.version = nextval('invest_institution');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER institution_holdings_update_version
BEFORE UPDATE
ON institution_holdings
for each row BEGIN
    set new.version = nextval('institution_holdings');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER institution_holdings_insert_version
BEFORE INSERT
ON institution_holdings
for each row BEGIN
    set new.version = nextval('institution_holdings');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;
