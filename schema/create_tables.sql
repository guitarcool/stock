drop database if exists efinancialpal;
create database efinancialpal;
USE efinancialpal;

DROP TABLE IF EXISTS insider_buy_activity;
DROP TABLE IF EXISTS insider_buy_stats;
DROP TABLE IF EXISTS insider_portfolio_manager;
DROP TABLE IF EXISTS daily_transaction_data;
DROP TABLE IF EXISTS weekly_transaction_data;
DROP TABLE IF EXISTS company_profile;
DROP TABLE IF EXISTS company_product;
DROP TABLE IF EXISTS company_foundamentals;
DROP TABLE IF EXISTS executive_summary;
DROP TABLE IF EXISTS report_reference;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS investment_risk;
DROP TABLE IF EXISTS trading_strategy;
DROP TABLE IF EXISTS value_analysis;
DROP TABLE IF EXISTS stock_stats;

DROP FUNCTION IF EXISTS nextval;

DROP TRIGGER IF EXISTS insider_buy_activity.insider_buy_activity_insert_version;
DROP TRIGGER IF EXISTS insider_buy_activity.insider_buy_activity_update_version;
DROP TRIGGER IF EXISTS daily_transaction_data.daily_transaction_data_insert_version;
DROP TRIGGER IF EXISTS daily_transaction_data.daily_transaction_data_update_version;
DROP TRIGGER IF EXISTS company_profile.company_profile_insert_version;
DROP TRIGGER IF EXISTS company_profile.company_profile_update_version;
DROP TRIGGER IF EXISTS company_product.company_product_insert_version;
DROP TRIGGER IF EXISTS company_product.company_product_update_version;
DROP TRIGGER IF EXISTS executive_summary.executive_summary_insert_version;
DROP TRIGGER IF EXISTS executive_summary.executive_summary_update_version;
DROP TRIGGER IF EXISTS report_reference.report_reference_insert_version;
DROP TRIGGER IF EXISTS report_reference.report_reference_update_version;
DROP TRIGGER IF EXISTS category.category_insert_version;
DROP TRIGGER IF EXISTS category.category_update_version;
DROP TRIGGER IF EXISTS investment_risk.investment_risk_insert_version;
DROP TRIGGER IF EXISTS investment_risk.investment_risk_update_version;
DROP TRIGGER IF EXISTS trading_strategy.trading_strategy_insert_version;
DROP TRIGGER IF EXISTS trading_strategy.trading_strategy_update_version;
DROP TRIGGER IF EXISTS value_analysis.value_analysis_insert_version;
DROP TRIGGER IF EXISTS value_analysis.value_analysis_update_version;
DROP TRIGGER IF EXISTS stock_stats.stock_stats_insert_version;
DROP TRIGGER IF EXISTS stock_stats.stock_stats_update_version;

CREATE TABLE IF NOT EXISTS sequences(
    name CHAR(30) PRIMARY KEY,
    val INT UNSIGNED
) ENGINE=INNODB;

DELIMITER $$
CREATE FUNCTION nextval (seqname CHAR(30))
RETURNS INT UNSIGNED
BEGIN
    INSERT INTO sequences VALUES (seqname,LAST_INSERT_ID(1))
    ON DUPLICATE KEY UPDATE val=LAST_INSERT_ID(val+1);
    RETURN LAST_INSERT_ID();
END;
$$
DELIMITER ;

CREATE TABLE stock_stats (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    market_cap decimal(30,6),
    shares_float int(10),
    shares_outstanding int(10),
    shares_owned int(10),
    eps decimal(30,6),
    pe decimal(30,6),
    peg decimal(30,6),
    eps_estimate_current_year decimal(30,6),
    eps_estimate_next_quarter decimal(30,6),
    eps_estimate_next_year decimal(30,6),
    price_book decimal(30,6),
    price_sales decimal(30,6),
    book_value_per_share decimal(30,6),
    revenue decimal(30,6),
    ebitda decimal(30,6),
    one_year_target_price decimal(30,6),
    short_ratio decimal(30,6),
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE daily_transaction_data (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    date datetime(3),
    open decimal(30,6),
    high decimal(30,6),
    low decimal(30,6),
    close decimal(30,6),
    volume int(10),
    adj_close decimal(30,6),
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE executive_summary (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    headline varchar(128) NOT NULL,
    suggestion text NOT NULL,
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE report_reference (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    author varchar(16),
    reference text NOT NULL,
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE category (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    category text NOT NULL,
    category_description text NOT NULL,
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;


CREATE TABLE investment_risk (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    risk_category enum('competitor','opertion_pressure','financial_pressure') NOT NULL,
    risk text NOT NULL,
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE trading_strategy (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    term enum('short_term','mid_term','long_term') NOT NULL,
    strategy text NOT NULL,
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE value_analysis (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    target_time datetime(3) NOT NULL,
    target_price decimal(10,2) NOT NULL,
    analysis text NOT NULL,
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;


CREATE TABLE weekly_transaction_data (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    date datetime(3),
    open decimal(30,6),
    high decimal(30,6),
    low decimal(30,6),
    close decimal(30,6),
    volume int(10),
    adj_close decimal(30,6),
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;


CREATE TABLE insider_buy_stats (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    sector varchar(12) NOT NULL,
    ownership_count int(10) unsigned NOT NULL,
    ownership_rank int(10) unsigned NOT NULL,
    percentage_all_portfolios float(10, 4) NOT NULL,
    hold_price decimal(30,6) NOT NULL,
    num_of_buy_transactions int(10) unsigned NOT NULL,
    total_buy_value int(10) unsigned NOT NULL,
    num_of_sell_transactions int(10) unsigned NOT NULL,
    total_sell_value int(10) unsigned NOT NULL,
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE insider_buy_activity (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    date_filed datetime(3) NOT NULL,
    reporting_name varchar(128),
    relationship varchar(12),
    trans_date datetime(3) NOT NULL,
    shares int(10)  unsigned NOT NULL,
    price decimal(30,6) NOT NULL,
    total_value int(10)  unsigned NOT NULL,
    description varchar(1024),
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE insider_portfolio_manager (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    portfolio_manager varchar(512) NOT NULL,
    percentage_of_portfolio float(7,2) NOT NULL,
    num_of_shares int(10) unsigned NOT NULL,
    recent_activity enum('add','reduce') NOT NULL,
    recent_activity_percentage float(7,2) NULL,
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;


CREATE TABLE company_profile (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    name varchar(512) NOT NULL,
    address varchar(512) NOT NULL,
    city varchar(128) NOT NULL,
    state varchar(2),
    country varchar(128),
    zip_code varchar(6),
    phone varchar(24),
    fax varchar(24),
    website varchar(512),
    index_membership varchar(256),
    sector varchar(256),
    industry varchar(512),
    full_time_employees int(10) unsigned,
    business_summary longtext,
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE company_product (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    name varchar(512) NOT NULL,
    description longtext,
    author varchar(16),
    active boolean NOT NULL DEFAULT true,
    version int(10),
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;


CREATE TABLE company_foundamental (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    ticker varchar(16) NOT NULL,
    market_cap varchar(24),
    enterprise_value varchar(24),
    training_pe float(10,2),
    forward_pe float(10,2),
    peg float(10,2),
    price_sales float(10,2),
    price_book float(10,2),
    enterprise_value_revenuse float(10,2),
    enterprise_value_ebitda float(10,2),
    author varchar(16),
    PRIMARY KEY(id)
) ENGINE=INNODB;

DELIMITER $$
CREATE TRIGGER insider_buy_activity_update_version
BEFORE UPDATE
ON insider_buy_activity
for each row BEGIN
    set new.version = nextval('insider_buy_activity');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER insider_buy_activity_insert_version
BEFORE INSERT
ON insider_buy_activity
for each row BEGIN
    set new.version = nextval('insider_buy_activity');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER daily_transaction_data_update_version
BEFORE UPDATE
ON daily_transaction_data
for each row BEGIN
    set new.version = nextval('daily_transaction_data');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER daily_transaction_data_insert_version
BEFORE INSERT
ON daily_transaction_data
for each row BEGIN
    set new.version = nextval('daily_transaction_data');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER company_profile_update_version
BEFORE UPDATE
ON company_profile
for each row BEGIN
    set new.version = nextval('company_profile');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER company_profile_insert_version
BEFORE INSERT
ON company_profile
for each row BEGIN
    set new.version = nextval('company_profile');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER company_product_update_version
BEFORE UPDATE
ON company_product
for each row BEGIN
    set new.version = nextval('company_product');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER company_product_insert_version
BEFORE INSERT
ON company_product
for each row BEGIN
    set new.version = nextval('company_product');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER executive_summary_update_version
BEFORE UPDATE
ON executive_summary
for each row BEGIN
    set new.version = nextval('executive_summary');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER executive_summary_insert_version
BEFORE INSERT
ON executive_summary
for each row BEGIN
    set new.version = nextval('executive_summary');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER report_reference_update_version
BEFORE UPDATE
ON report_reference
for each row BEGIN
    set new.version = nextval('report_reference');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER report_reference_insert_version
BEFORE INSERT
ON report_reference
for each row BEGIN
    set new.version = nextval('report_reference');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER category_update_version
BEFORE UPDATE
ON category
for each row BEGIN
    set new.version = nextval('category');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER category_insert_version
BEFORE INSERT
ON category
for each row BEGIN
    set new.version = nextval('category');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER investment_risk_update_version
BEFORE UPDATE
ON investment_risk
for each row BEGIN
    set new.version = nextval('investment_risk');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER investment_risk_insert_version
BEFORE INSERT
ON investment_risk
for each row BEGIN
    set new.version = nextval('investment_risk');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER trading_strategy_update_version
BEFORE UPDATE
ON trading_strategy
for each row BEGIN
    set new.version = nextval('trading_strategy');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER trading_strategy_insert_version
BEFORE INSERT
ON trading_strategy
for each row BEGIN
    set new.version = nextval('trading_strategy');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER value_analysis_update_version
BEFORE UPDATE
ON value_analysis
for each row BEGIN
    set new.version = nextval('value_analysis');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER value_analysis_insert_version
BEFORE INSERT
ON value_analysis
for each row BEGIN
    set new.version = nextval('value_analysis');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER stock_stats_update_version
BEFORE UPDATE
ON stock_stats
for each row BEGIN
    set new.version = nextval('stock_stats');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER stock_stats_insert_version
BEFORE INSERT
ON stock_stats
for each row BEGIN
    set new.version = nextval('stock_stats');
    set new.version_updated_at = now(3);
END;
$$
DELIMITER ;


