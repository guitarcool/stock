CREATE TABLE history (
    id int(10)  unsigned NOT NULL AUTO_INCREMENT,
    person_id varchar(256) NOT NULL,
    in_out enum('in','out') NOT NULL,
    timestamp datetime(3),
    active boolean NOT NULL,
    version_updated_at datetime(3),
    PRIMARY KEY(id)
) ENGINE=INNODB;
