#export FROM_DATE="2017-03-01"
#export TO_DATE="2017-03-31"
#python /Users/haiyanluo/workspace/stock/crawler/dataroma/insider.py
REMOTE_DEBUGGER="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=9099"
CLASS_PATH="/Users/haiyanluo/workspace/stock/target/efinancialpal-1.0-SNAPSHOT.jar"
#MAIN_CLASS="com.efinancialpal.swagger.service.StockMain"
MAIN_CLASS="com.efinancialpal.swagger.EFinancialPalApp"
STDOUT="/tmp/efinancialpal.out"
CONFIG="-Dconfig.dir=/Users/haiyanluo/workspace/stock/target/classes/main/resources"
LOG4J_PROPERTY="-Dlog4j.configuration=file:/Users/haiyanluo/workspace/stock/target/classes/main/resources/log4j.properties"
java -cp $CLASS_PATH $LOG4J_PROPERTY $CONFIG $REMOTE_DEBUGGER $MAIN_CLASS >$STDOUT 2>&1
