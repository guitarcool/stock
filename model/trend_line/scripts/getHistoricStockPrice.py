import json,getopt,commands,sys

def main(argv):
   try:
      opts, args = getopt.getopt(argv,"hs:b:e:o:",["symbol=","begin=","end=","ofile="])
   except getopt.GetoptError:
      print 'getHistoricStockPrice.py -s <stock_symbol> -b <begin_date_mm-dd-yyyy> -e <end_date_mm-dd-yyyy> -o <output_file_name>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'getHistoricStockPrice.py -s <stock_symbol> -b <begin_date_mm-dd-yyyy> -e <end_date_mm-dd-yyyy> -o <output_file_name>'
         sys.exit()
      elif opt in ("-s", "--symbol"):
         stock_symbol = arg
      elif opt in ("-b", "--begin"):
         begin_date = arg
	 begin_month,begin_day,begin_year = begin_date.split("_")
 #        begin_month = (string)((int)begin_month - 1)
      elif opt in ("-e", "--end"):
	 end_date = arg
         end_month,end_day,end_year = end_date.split("_")
#         end_month = (int)end_month - 1
      elif opt in ("-o", "--ofile"):
         file_name = arg
         output_file = open(file_name, "w")
   curl_command = "curl \'http://ichart.finance.yahoo.com/table.csv?s=" + stock_symbol + "&a=" + begin_month + "&b=" + begin_day + "&c=" + begin_year + "&d=" + end_month + "&e=" + end_day + "&f=" + end_year + "\'" + " 2>/dev/null"
   try:
#      (status, output) = commands.getstatusoutput("curl http://ichart.finance.yahoo.com/table.csv?s=" + stock_symbol + "&a=" + begin_day + "&b=" + begin_month + "&c=" + begin_year + "&d=" + end_day + "&e=" + end_month + "&f=" + end_year + "2>/dev/null")
      (status, output) = commands.getstatusoutput(curl_command)
      output_file.write(" %s" % output)
   except:
      print 'call stock price failed, please check the input'
 #  output_file.close()

if __name__ == "__main__":
   main(sys.argv[1:])
