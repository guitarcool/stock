1. get the candidate through strategies and model
2. confirm technical details
3. check insider through dataroma.com
4. check finviz.com for more thorough information
5. check seekingalpha.com for more insights on analysis and foundamentals
6. check industry performance and cycles
7. construct and check portfolio and risk management
8. build the entry/exit strategy
