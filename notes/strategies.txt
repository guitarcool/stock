Major strategies:
1. pullback strategy: trending stock that's in pullback and shows signs of continuing the trend (low risk strategy)
2. breakout strategy: break out of support/resistence with increasing volume
3. bottom/top divergence: long-time trend that shows significant signs of divergence. e.g. money flow, mcad, rsi etc
4. gap: increasing volum on gap up/down without filling it up
5. false break out after a long time trending (vice versa) e.g. break up but then lose the gain significantly with huge volume
6. insider heavy purchases with confirmation from technical analysis


